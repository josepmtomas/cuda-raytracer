#ifndef _SCENE_
#define _SCENE_

#include "DeviceScene.cuh"

__host__ bool parseSceneFile(char *filename, HostScene *scene, SceneSettings *settings);
__host__ bool parseCSGTreeFile(int _line, const char *filename, HostCSGTree *tree);

// Scene transfer function HOST -> DEVICE
__host__ void copySceneHostToDevice(HostScene *hostScene, DeviceScene *deviceScene);
__host__ void copyTexturesHostToDevice(HostScene *hostScene, DeviceScene *deviceScene);
__host__ void copyMeshesHostToDevice(HostScene *hostScene, DeviceScene *deviceScene);
__host__ void copyCSGTrees(HostScene *hostScene, DeviceScene *deviceScene);

#endif

bool parseSceneFile(char *filename, HostScene *scene, SceneSettings *settings)
{
	int lineNumber = 0;
	int lineGroup = 0;
	bool valid;
	bool omitLine;
	bool omitGroupLine;
	bool endGroup;
	std::string	line;
	std::string	token;
	std::stringstream lineStream;

	vec3	tempVec3;
	float	tempFloat;

	////////////////////////////////////////// Texture
	
	std::string textureName;
	std::string textureSource;
	std::vector<std::string> textureNames;

	////////////////////////////////////////// Camera

	vec3 pov,poi;
	float fov;

	////////////////////////////////////////// Material

	std::string materialName;
	std::vector<std::string> materialNames;
	Material material;

	////////////////////////////////////////// Primitives

	Box box;
	Cylinder cylinder;
	Sphere sphere;

	////////////////////////////////////////// Primitives

	PointLight pointLight;
	HostAreaLight areaLight;
	vec3 position, direction;
	float rollAngle, width, height;
	u_int2 subdivision;

	////////////////////////////////////////// Open scene file

	std::ifstream inFile(filename);

	if(!inFile.is_open())
	{
		cprintf(CONSOLE_COLOR_RED,"[ERROR] Error opening file: %s\n", filename);
		return false;
	}

	//////////////////////////////////////////

	while(!inFile.eof())
	{
		omitLine = false;
		endGroup = false;
		std::getline(inFile,line);
		lineStream = std::stringstream(line);
		lineNumber++;

		while(lineStream >> token && !omitLine)
		{
			if(token.compare("#") == 0)
			{
				//printf("Comment\n");
				omitLine = true;
			}
			////////////////////////////////////////////////////////////////////////////////////
			// Scene settings parse
			////////////////////////////////////////////////////////////////////////////////////

			else if(token.compare("SceneSettings") == 0)
			{
				//printf("Settings\n");
				lineGroup = lineNumber;
				endGroup = false;
				omitGroupLine = false;

				// Get opener
				std::getline(inFile,line);
				lineNumber++;
				lineStream = std::stringstream(line);
				lineStream >> token;

				if(token.compare("{") != 0)
				{
					cprintf(CONSOLE_COLOR_RED,"[ERROR] Unexpected '%s' Expected '{' to begin SceneSettings declaration.\n", token.c_str());
					return false;
				}

				while(!endGroup)
				{
					omitGroupLine = false;
					std::getline(inFile,line);
					lineNumber++;
					lineStream = std::stringstream(line);

					while(lineStream >> token && !omitGroupLine)
					{
						omitGroupLine = false;

						if(token.compare("BackgroundColor") == 0)
						{
							//printf("\tBackgroundColor\n");
							if(lineStream >> token){ settings->backgroundColor.x = (float)atof(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 3 values to define 'BackgroundColor'.\n", lineNumber); return false; }
							if(lineStream >> token){ settings->backgroundColor.y = (float)atof(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 3 values to define 'BackgroundColor'.\n", lineNumber); return false; }
							if(lineStream >> token){ settings->backgroundColor.z = (float)atof(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 3 values to define 'BackgroundColor'.\n", lineNumber); return false; }
							if(lineStream >> token)
							{
								cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Too many values defining 'BackgroundColor' (expected 3 values)\n", lineNumber);
								return false;
							}
							omitGroupLine = true;
						}

						else if(token.compare("RenderWidth") == 0)
						{
							//printf("\tRenderWidth\n");
							if(lineStream >> token) { settings->renderWidth = atoi(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 1 value to define 'RenderWidth'.\n", lineNumber); return false; }
							if(lineStream >> token)
							{
								cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Too many values defining 'RenderWidth' (expected 1 value)\n", lineNumber);
								return false;
							}
							omitGroupLine = true;
						}

						else if(token.compare("RenderHeight") == 0)
						{
							//printf("\tRenderHeight\n");
							if(lineStream >> token) { settings->renderHeight = atoi(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 1 value to define 'RenderHeight'.\n", lineNumber); return false; }
							if(lineStream >> token)
							{
								cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Too many values defining 'RenderHeight' (expected 1 value)\n", lineNumber);
								return false;
							}
							omitGroupLine = true;
						}

						else if(token.compare("ReflectionQuality") == 0)
						{
							if(lineStream >> token)	{ settings->reflectionQuality = atoi(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 1 value to define 'ReflectionQuality'.\n", lineNumber); return false; }
							if(lineStream >> token)
							{
								cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Too many values defining 'ReflectionQuality' (expected 1 value)\n", lineNumber);
								return false;
							}
							omitGroupLine = true;
						}

						else if(token.compare("ReflectionSteps") == 0)
						{
							if(lineStream >> token)	{ settings->reflectionSteps = atoi(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 1 value to define 'ReflectionSteps'.\n", lineNumber); return false; }
							if(lineStream >> token)
							{
								cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Too many values defining 'ReflectionSteps' (expected 1 value)\n", lineNumber);
								return false;
							}
							omitGroupLine = true;
						}

						else if(token.compare("IndirectQuality") == 0)
						{
							if(lineStream >> token) { settings->indirectQuality = atoi(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 1 value to define 'IndirectQuality'.\n", lineNumber); return false; }
							if(lineStream >> token)
							{
								cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Too many values defining 'IndirectQuality' (expected 1 value)\n", lineNumber);
								return false;
							}
							omitGroupLine = true;
						}

						else if(token.compare("ColorBleeding") == 0)
						{
							if(lineStream >> token) { settings->colorBleeding = (float)atof(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 1 value to define 'ColorBleeding'.\n", lineNumber); return false; }
							if(lineStream >> token)
							{
								cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Too many values defining 'ColorBleeding' (expected 1 value)\n", lineNumber);
								return false;
							}
							omitGroupLine = true;
						}

						else if(token.compare("Antialiasing") == 0)
						{
							if(lineStream >> token)	{ settings->antialiasing = atoi(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 1 value to define 'Antialiasing'.\n", lineNumber); return false; }
							if(lineStream >> token)
							{
								cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Too many values defining 'Antialiasing' (expected 1 values)\n", lineNumber);
								return false;
							}

							if(settings->antialiasing != 4 && settings->antialiasing != 16)
							{
								settings->antialiasing = 1;
							}

							omitGroupLine = true;
						}

						else if(token.compare("}") == 0)
						{
							omitGroupLine = true;
							endGroup = true;
						}

						else
						{
							cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Token '%s' not a field of 'SceneSettings'.\n", lineNumber, token.c_str()); 
							return false;
						}
					}
				}

				cprintf(CONSOLE_COLOR_WHITE, "[");
				cprintf(CONSOLE_COLOR_GREEN, "OK");
				cprintf(CONSOLE_COLOR_WHITE, "] Scene Settings set.\n");
			}

			////////////////////////////////////////////////////////////////////////////////////
			// Camera
			////////////////////////////////////////////////////////////////////////////////////

			else if(token.compare("Camera") == 0)
			{
				//printf("Camera\n");
				lineGroup = lineNumber;
				endGroup = false;
				omitGroupLine = false;

				// Get opener
				std::getline(inFile,line);
				lineNumber++;
				lineStream = std::stringstream(line);
				lineStream >> token;

				if(token.compare("{") != 0)
				{
					cprintf(CONSOLE_COLOR_RED,"[ERROR] Unexpected '%s' Expected '{' to begin Camera declaration.\n", token.c_str());
					return false;
				}

				while(!endGroup)
				{
					omitGroupLine = false;
					std::getline(inFile,line);
					lineNumber++;
					lineStream = std::stringstream(line);

					while(lineStream >> token && !omitGroupLine)
					{
						omitGroupLine = false;

						if(token.compare("POV") == 0)
						{
							if(lineStream >> token) { pov.x = (float)atof(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 3 values to define 'POV'.\n", lineNumber); return false; }
							if(lineStream >> token) { pov.y = (float)atof(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 3 values to define 'POV'.\n", lineNumber); return false; }
							if(lineStream >> token) { pov.z = (float)atof(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 3 values to define 'POV'.\n", lineNumber); return false; }
							if(lineStream >> token)
							{
								cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Too many values defining 'POV' (expected 3 values)\n", lineNumber);
								return false;
							}
							omitGroupLine = true;
						}
						else if(token.compare("POI") == 0)
						{
							if(lineStream >> token) { poi.x = (float)atof(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 3 values to define 'POI'.\n", lineNumber); return false; }
							if(lineStream >> token) { poi.y = (float)atof(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 3 values to define 'POI'.\n", lineNumber); return false; }
							if(lineStream >> token) { poi.z = (float)atof(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 3 values to define 'POI'.\n", lineNumber); return false; }
							if(lineStream >> token)
							{
								cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Too many values defining 'POI' (expected 3 values)\n", lineNumber);
								return false;
							}
							omitGroupLine = true;
						}
						else if(token.compare("FOV") == 0)
						{
							if(lineStream >> token) { fov = (float)atof(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 1 value to define 'FOV'.\n", lineNumber); return false; }
							if(lineStream >> token)
							{
								cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Too many values defining 'FOV' (expected 1 values)\n", lineNumber);
								return false;
							}
							omitGroupLine = true;
						}
						else if(token.compare("}") == 0)
						{
							omitGroupLine = true;
							endGroup = true;
						}

						else
						{
							cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Token '%s' not a field of 'Camera'.\n", lineNumber, token.c_str()); 
							return false;
						}
					}
				}

				PerspectiveCamera camera(pov,poi,fov);
				scene->setCamera(camera);

				cprintf(CONSOLE_COLOR_WHITE, "[");
				cprintf(CONSOLE_COLOR_GREEN, "OK");
				cprintf(CONSOLE_COLOR_WHITE, "] Camera set.\n");
			}

			////////////////////////////////////////////////////////////////////////////////////
			// Texture parse
			////////////////////////////////////////////////////////////////////////////////////
			else if(token.compare("Texture") == 0)
			{
				//printf("Texture\n");
				lineGroup = lineNumber;
				textureName.clear();
				textureSource.clear();
				endGroup = false;
				omitGroupLine = false;

				// Get opener
				std::getline(inFile,line);
				lineNumber++;
				lineStream = std::stringstream(line);
				lineStream >> token;

				if(token.compare("{") != 0)
				{
					cprintf(CONSOLE_COLOR_RED,"[ERROR] Unexpected '%s' Expected '{' to begin Texture declaration.\n", token.c_str());
					return false;
				}

				while(!endGroup)
				{
					omitGroupLine = false;
					std::getline(inFile,line);
					lineNumber++;
					lineStream = std::stringstream(line);

					while(lineStream >> token && !omitGroupLine)
					{
						omitGroupLine = false;

						if(token.compare("Name") == 0)
						{
							if(lineStream >> textureName){}else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 1 value to define 'Name'.\n", lineNumber); return false; }
							//printf("\tTextureName %s\n", textureName.c_str());
							omitGroupLine = true;
						}
						else if(token.compare("Source") == 0)
						{
							std::getline(lineStream,token,'"');
							std::getline(lineStream,textureSource,'"');
							//if(lineStream >> textureSource){}else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 1 value to define 'Source'.\n", lineNumber); return false; }
							//printf("\tTextureSource %s\n", textureSource.c_str());
							omitGroupLine = true;
						}
						else if(token.compare("}") == 0)
						{
							omitGroupLine = true;
							endGroup = true;
						}
						else
						{
							cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Token '%s' is not a field of 'Texture'.\n", lineNumber, token.c_str());
							return false;
						}
					}
				}

				if(textureName.empty())	// No name declarated
				{
					cprintf(CONSOLE_COLOR_RED,"[ERROR] Texture that begins at line (%d): Must specify a name for the texture.\n", lineGroup);
					return false;
				}
				else	// Name aready in use
				{
					for(int i=0; i<textureNames.size(); i++)
					{
						if(textureNames[i].compare(textureName) == 0)
						{
							cprintf(CONSOLE_COLOR_RED,"[ERROR] Texture that begins at line (%d): Texture name already in use.\n", lineGroup);
							return false;
						}
					}
				}
				
				if(textureSource.empty())
				{
					cprintf(CONSOLE_COLOR_RED,"[ERROR] Texture that begins at line (%d): Must specify a source file for the texture.\n", lineGroup);
					return false;
				}

				HostTexture texture;

				if(!texture.loadTexture(textureSource.c_str()))
				{
					cprintf(CONSOLE_COLOR_RED,"[ERROR] Texture that begins at line (%d): failed loading the file\n", lineGroup);
					return false;
				}
				else
				{
					textureNames.push_back(textureName);
					scene->addTexture(texture);
					cprintf(CONSOLE_COLOR_WHITE, "[");
					cprintf(CONSOLE_COLOR_GREEN, "OK");
					cprintf(CONSOLE_COLOR_WHITE, "] Texture '%s' added.\n", textureName.c_str());
				}
			}

			////////////////////////////////////////////////////////////////////////////////////
			// Material parse
			////////////////////////////////////////////////////////////////////////////////////
			else if(token.compare("Material") == 0)
			{
				//cprintf(CONSOLE_COLOR_BLUE, "Material\n");
				materialName.clear();
				lineGroup = lineNumber;
				endGroup = false;
				material = Material();

				// Get opener
				std::getline(inFile,line);
				lineNumber++;
				lineStream = std::stringstream(line);
				lineStream >> token;

				if(token.compare("{") != 0)
				{
					cprintf(CONSOLE_COLOR_RED,"[ERROR] Unexpected '%s' Expected '{' to begin Material declaration.\n", token.c_str());
					return false;
				}

				while(!endGroup)
				{
					omitGroupLine = false;
					lineNumber++;
					std::getline(inFile,line);
					lineStream = std::stringstream(line);

					while(lineStream >> token && !omitGroupLine)
					{
						if(token.compare("}") == 0)
						{
							//printf("}\n");
							omitGroupLine = true;
							endGroup = true;
						}

						else if(token.compare("Name") == 0)
						{
							if(lineStream >> materialName){}else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 1 value to define 'Name'.\n", lineNumber); return false; }

							for(int i=0; i<materialNames.size(); i++)
							{
								if(materialNames[i].compare(materialName) == 0)
								{
									cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Name already in use.\n", lineNumber);
									return false;
								}
							}
						}

						else if(token.compare("DiffuseColor") == 0)
						{
							if(lineStream >> token) { material.diffuseColor.x = (float)atof(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 3 values to define 'DiffuseColor'.\n", lineNumber); return false; }
							if(lineStream >> token)	{ material.diffuseColor.y = (float)atof(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 3 values to define 'DiffuseColor'.\n", lineNumber); return false; }
							if(lineStream >> token)	{ material.diffuseColor.z = (float)atof(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 3 values to define 'DiffuseColor'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Too many values defining 'DiffuseColor' (expected 3 values)\n", lineNumber); return false; }
							omitGroupLine = true;
						}

						else if(token.compare("SpecularColor") == 0)
						{
							if(lineStream >> token) { material.specularColor.x = (float)atof(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 3 values to define 'SpecularColor'.\n", lineNumber); return false; }
							if(lineStream >> token)	{ material.specularColor.y = (float)atof(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 3 values to define 'SpecularColor'.\n", lineNumber); return false; }
							if(lineStream >> token)	{ material.specularColor.z = (float)atof(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 3 values to define 'SpecularColor'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Too many values defining 'SpecularColor' (expected 3 values)\n", lineNumber); return false; }
							omitGroupLine = true;
						}

						else if(token.compare("EmissiveColor") == 0)
						{
							if(lineStream >> token) { material.emissiveColor.x = (float)atof(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 3 values to define 'EmissiveColor'.\n", lineNumber); return false; }
							if(lineStream >> token) { material.emissiveColor.y = (float)atof(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 3 values to define 'EmissiveColor'.\n", lineNumber); return false; }
							if(lineStream >> token) { material.emissiveColor.z = (float)atof(token.c_str()); }else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 3 values to define 'EmissiveColor'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Too many values defining 'EmissiveColor' (expected 3 values)\n", lineNumber); return false; }
							omitGroupLine = true;
						}

						else if(token.compare("DiffuseTexture") == 0)
						{
							if(lineStream >> token){}else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 1 value to define 'DiffuseTexture'.\n", lineNumber); return false; } 
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Too many values defining 'DiffuseTexture' (expected 1 value)\n", lineNumber); return false; }

							valid = false;
							for(int i=0; i<textureNames.size(); i++)
							{
								if(textureNames[i].compare(token) == 0)
								{
									valid = true;
									material.diffuseTexture = i;
									break;
								}
							}

							if(!valid)
							{
								cprintf(CONSOLE_COLOR_RED, "[ERROR] Material that begins at line (%d): DiffuseTexture don't exist.\n", lineGroup);
								return false;
							}
						}

						else if(token.compare("SpecularTexture") == 0)
						{
							if(lineStream >> token){}else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 1 value to define 'SpecularTexture'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Too many values defining 'SpecularTexture' (expected 1 value)\n", lineNumber); return false; }

							valid = false;
							for(int i=0; i<textureNames.size(); i++)
							{
								if(textureNames[i].compare(token) == 0)
								{
									valid = true;
									material.specularTexture = i;
									break;
								}
							}

							if(!valid)
							{
								cprintf(CONSOLE_COLOR_RED, "[ERROR] Material that begins at line (%d): SpecularTexture don't exist.\n", lineGroup);
								return false;
							}
						}

						else if(token.compare("EmissiveTexture") == 0)
						{
							if(lineStream >> token){}else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 1 value to define 'EmissiveTexture'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Too many values defining 'EmissiveTexture' (expected 1 value)\n", lineNumber); return false; }

							valid = false;
							for(int i=0; i<textureNames.size(); i++)
							{
								if(textureNames[i].compare(token) == 0)
								{
									valid = true;
									material.emissiveTexture = i;
									break;
								}
							}

							if(!valid)
							{
								cprintf(CONSOLE_COLOR_RED, "[ERROR] Material that begins at line (%d): EmissiveTexture don't exist.\n", lineGroup);
								return false;
							}
						}

						else if(token.compare("NormalTexture") == 0)
						{
							if(lineStream >> token){}else{ cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 1 value to define 'NormalTexture'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Too many values defining 'NormalTexture' (expected 1 value)\n", lineNumber); return false; }

							valid = false;
							for(int i=0; i<textureNames.size(); i++)
							{
								if(textureNames[i].compare(token) == 0)
								{
									valid = true;
									material.normalTexture = i;
									break;
								}
							}

							if(!valid)
							{
								cprintf(CONSOLE_COLOR_RED, "[ERROR] Material that begins at line (%d): NormalTexture don't exist.\n", lineGroup);
								return false;
							}
						}

						else if(token.compare("Roughness") == 0)
						{
							if(lineStream >> token) { material.roughness = (float)atof(token.c_str()); }else{cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 1 value to define 'Roughness'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Too many values defining 'Roughness' (expected 1 value).\n", lineNumber); return false; }
						}

						else if(token.compare("ReflectionAngle") == 0)
						{
							if(lineStream >> token) { material.reflectionAngle = (float)atof(token.c_str()); }else{cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 1 value to define 'ReflectionAngle'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Too many values defining 'ReflectionAngle' (expected 1 value).\n", lineNumber); return false; }
						}

						else if(token.compare("ReflectionAmount") == 0)
						{
							if(lineStream >> token) { material.reflectionAmount = (float)atof(token.c_str()); }else{cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 1 value to define 'ReflectionAmount'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Too many values defining 'ReflectionAmount' (expected 1 value).\n", lineNumber); return false; }
						}

						else if(token.compare("Opacity") == 0)
						{
							if(lineStream >> token) { material.opacity = (float)atof(token.c_str()); }else{cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 1 value to define 'Opacity'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Too many values defining 'Opacity' (expected 1 value).\n", lineNumber); return false; }
						}

						else if(token.compare("RefractionIndex") == 0)
						{
							if(lineStream >> token) { material.refractionIndex = (float)atof(token.c_str()); }else{cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Need 1 value to define 'RefractionIndex'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Too many values defining 'RefractionIndex' (expected 1 value).\n", lineNumber); return false; }
						}

						else
						{
							cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Token '%s' is not a field of 'Material' .\n", lineNumber, token.c_str());
							return false;
						}
					}
				}

				materialNames.push_back(materialName);
				scene->addMaterial(material);

				cprintf(CONSOLE_COLOR_WHITE, "[");
				cprintf(CONSOLE_COLOR_GREEN, "OK");
				cprintf(CONSOLE_COLOR_WHITE, "] Material '%s' added.\n", materialName.c_str());
			}

			////////////////////////////////////////////////////////////////////////////////////
			// Box parse
			////////////////////////////////////////////////////////////////////////////////////
			else if(token.compare("Box") == 0)
			{
				//cprintf(CONSOLE_COLOR_BLUE, "Material\n");
				lineGroup = lineNumber;
				endGroup = false;
				box = Box();

				// Get opener
				std::getline(inFile,line);
				lineNumber++;
				lineStream = std::stringstream(line);
				lineStream >> token;

				if(token.compare("{") != 0)
				{
					cprintf(CONSOLE_COLOR_RED,"[ERROR] Unexpected '%s' Expected '{' to begin Box declaration.\n", token.c_str());
					return false;
				}

				while(!endGroup)
				{
					omitGroupLine = false;
					lineNumber++;
					std::getline(inFile,line);
					lineStream = std::stringstream(line);

					while(lineStream >> token && !omitGroupLine)
					{
						if(token.compare("}") == 0)
						{
							//printf("}\n");
							omitGroupLine = true;
							endGroup = true;
						}

						else if(token.compare("Material") == 0)
						{
							if(lineStream >> token)
							{
								valid = false;
								for(int i=0; i<materialNames.size(); i++)
								{
									if(materialNames[i].compare(token) == 0)
									{
										box.setMaterial(i);
										valid = true;
										break;
									}
								}

								if(!valid)
								{
									cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Material '%s' don't exist.\n", lineNumber, token.c_str());
									return false;
								}
							}
							else
							{
								cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 1 value to define 'Material'.\n", lineNumber);
								return false;
							}
							if(lineStream >> token){ cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Material' (expected 1 value).\n", lineNumber); return false; }
						}

						else if(token.compare("Translation") == 0)
						{
							if(lineStream >> token) { tempVec3.x = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Translation'.\n", lineNumber); return false; }
							if(lineStream >> token) { tempVec3.y = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Translation'.\n", lineNumber); return false; }
							if(lineStream >> token) { tempVec3.z = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Translation'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Translation' (expected 3 values).\n", lineNumber); return false; }

							box.translation(tempVec3);
						}

						else if(token.compare("Rotation") == 0)
						{
							if(lineStream >> token) { tempVec3.x = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 4 values to define 'Rotation'.\n", lineNumber); return false; }
							if(lineStream >> token) { tempVec3.y = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 4 values to define 'Rotation'.\n", lineNumber); return false; }
							if(lineStream >> token) { tempVec3.z = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 4 values to define 'Rotation'.\n", lineNumber); return false; }
							if(lineStream >> token) {  tempFloat = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 4 values to define 'Rotation'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Translation' (expected 4 values).\n", lineNumber); return false; }

							box.rotation(tempVec3, tempFloat);
						}

						else if(token.compare("Scale") == 0)
						{
							if(lineStream >> token) { tempVec3.x = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Scale'.\n", lineNumber); return false; }
							if(lineStream >> token) { tempVec3.y = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Scale'.\n", lineNumber); return false; }
							if(lineStream >> token) { tempVec3.z = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Scale'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Scale' (expected 3 values).\n", lineNumber); return false; }

							if(tempVec3.x == 0 || tempVec3.y == 0 || tempVec3.z == 0)
							{
								cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Scaling values of zero not allowed.\n", lineNumber);
								return false;
							}

							box.scale(tempVec3);
						}

						else
						{
							cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Token '%s' is not a field of 'Box' .\n", lineNumber, token.c_str());
							return false;
						}
					}
				}

				scene->addBox(box);

				cprintf(CONSOLE_COLOR_WHITE, "[");
				cprintf(CONSOLE_COLOR_GREEN, "OK");
				cprintf(CONSOLE_COLOR_WHITE, "] Box added.\n");
			}

			////////////////////////////////////////////////////////////////////////////////////
			// Sphere parse
			////////////////////////////////////////////////////////////////////////////////////
			else if(token.compare("Sphere") == 0)
			{
				//cprintf(CONSOLE_COLOR_BLUE, "Material\n");
				lineGroup = lineNumber;
				endGroup = false;
				sphere = Sphere();

				// Get opener
				std::getline(inFile,line);
				lineNumber++;
				lineStream = std::stringstream(line);
				lineStream >> token;

				if(token.compare("{") != 0)
				{
					cprintf(CONSOLE_COLOR_RED,"[ERROR] Unexpected '%s' Expected '{' to begin Sphere declaration.\n", token.c_str());
					return false;
				}

				while(!endGroup)
				{
					omitGroupLine = false;
					lineNumber++;
					std::getline(inFile,line);
					lineStream = std::stringstream(line);

					while(lineStream >> token && !omitGroupLine)
					{
						if(token.compare("}") == 0)
						{
							//printf("}\n");
							omitGroupLine = true;
							endGroup = true;
						
						}

						else if(token.compare("Material") == 0)
						{
							if(lineStream >> token)
							{
								valid = false;
								for(int i=0; i<materialNames.size(); i++)
								{
									if(materialNames[i].compare(token) == 0)
									{
										sphere.setMaterial(i);
										valid = true;
										break;
									}
								}

								if(!valid)
								{
									cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Material '%s' don't exist.\n", lineNumber, token.c_str());
									return false;
								}
							}
							else
							{
								cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 1 value to define 'Material'.\n", lineNumber);
								return false;
							}
							if(lineStream >> token){ cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Material' (expected 1 value).\n", lineNumber); return false; }
						}

						else if(token.compare("Center") == 0)
						{
							if(lineStream >> token) { tempVec3.x = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Center'.\n", lineNumber); return false; }
							if(lineStream >> token) { tempVec3.y = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Center'.\n", lineNumber); return false; }
							if(lineStream >> token) { tempVec3.z = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Center'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Center' (expected 3 values).\n", lineNumber); return false; }

							sphere.setCenter(tempVec3);
						}

						else if(token.compare("Radius") == 0)
						{
							if(lineStream >> token) { tempFloat = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 1 value to define 'Radius'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Radius' (expected 1 value).\n", lineNumber); return false; }

							sphere.setRadius(tempFloat);
						}

						else
						{
							cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Token '%s' is not a field of 'Sphere' .\n", lineNumber, token.c_str());
							return false;
						}
					}
				}

				//sphere = Sphere(tempVec3, tempFloat);
				scene->addSphere(sphere);

				cprintf(CONSOLE_COLOR_WHITE, "[");
				cprintf(CONSOLE_COLOR_GREEN, "OK");
				cprintf(CONSOLE_COLOR_WHITE, "] Sphere added.\n");
			}

			////////////////////////////////////////////////////////////////////////////////////
			// Cylinder parse
			////////////////////////////////////////////////////////////////////////////////////
			else if(token.compare("Cylinder") == 0)
			{
				//cprintf(CONSOLE_COLOR_BLUE, "Material\n");
				lineGroup = lineNumber;
				endGroup = false;
				cylinder = Cylinder();

				// Get opener
				std::getline(inFile,line);
				lineNumber++;
				lineStream = std::stringstream(line);
				lineStream >> token;

				if(token.compare("{") != 0)
				{
					cprintf(CONSOLE_COLOR_RED,"[ERROR] Unexpected '%s' Expected '{' to begin Cylinder declaration.\n", token.c_str());
					return false;
				}

				while(!endGroup)
				{
					omitGroupLine = false;
					lineNumber++;
					std::getline(inFile,line);
					lineStream = std::stringstream(line);

					while(lineStream >> token && !omitGroupLine)
					{
						if(token.compare("}") == 0)
						{
							//printf("}\n");
							omitGroupLine = true;
							endGroup = true;
						
						}

						else if(token.compare("Material") == 0)
						{
							if(lineStream >> token)
							{
								valid = false;
								for(int i=0; i<materialNames.size(); i++)
								{
									if(materialNames[i].compare(token) == 0)
									{
										cylinder.setMaterial(i);
										valid = true;
										break;
									}
								}

								if(!valid)
								{
									cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Material '%s' don't exist.\n", lineNumber, token.c_str());
									return false;
								}
							}
							else
							{
								cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 1 value to define 'Material'.\n", lineNumber);
								return false;
							}
							if(lineStream >> token){ cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Material' (expected 1 value).\n", lineNumber); return false; }
						}

						else if(token.compare("Base") == 0)
						{
							if(lineStream >> token) { tempVec3.x = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Base'.\n", lineNumber); return false; }
							if(lineStream >> token) { tempVec3.y = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Base'.\n", lineNumber); return false; }
							if(lineStream >> token) { tempVec3.z = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Base'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Base' (expected 3 values).\n", lineNumber); return false; }

							cylinder.base = tempVec3;
						}

						else if(token.compare("Axis") == 0)
						{
							if(lineStream >> token) { tempVec3.x = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Axis'.\n", lineNumber); return false; }
							if(lineStream >> token) { tempVec3.y = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Axis'.\n", lineNumber); return false; }
							if(lineStream >> token) { tempVec3.z = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Axis'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Axis' (expected 3 values).\n", lineNumber); return false; }

							cylinder.axis = normalize(tempVec3);
						}

						else if(token.compare("Height") == 0)
						{
							if(lineStream >> token) { tempFloat = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 1 value to define 'Height'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Height' (expected 1 value).\n", lineNumber); return false; }

							cylinder.height = tempFloat;
						}

						else if(token.compare("Radius") == 0)
						{
							if(lineStream >> token) { tempFloat = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 1 value to define 'Radius'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Radius' (expected 1 value).\n", lineNumber); return false; }

							cylinder.radius = tempFloat;
						}

						else
						{
							cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Token '%s' is not a field of 'Cylinder' .\n", lineNumber, token.c_str());
							return false;
						}
					}
				}

				cylinder.buildPlanes();

				scene->addCylinder(cylinder);

				cprintf(CONSOLE_COLOR_WHITE, "[");
				cprintf(CONSOLE_COLOR_GREEN, "OK");
				cprintf(CONSOLE_COLOR_WHITE, "] Cylinder added.\n");
			}

			////////////////////////////////////////////////////////////////////////////////////
			// PointLight parse
			////////////////////////////////////////////////////////////////////////////////////
			else if(token.compare("PointLight") == 0)
			{
				//cprintf(CONSOLE_COLOR_BLUE, "Material\n");
				lineGroup = lineNumber;
				endGroup = false;
				pointLight = PointLight();

				// Get opener
				std::getline(inFile,line);
				lineNumber++;
				lineStream = std::stringstream(line);
				lineStream >> token;

				if(token.compare("{") != 0)
				{
					cprintf(CONSOLE_COLOR_RED,"[ERROR] Unexpected '%s' Expected '{' to begin PointLight declaration.\n", token.c_str());
					return false;
				}

				while(!endGroup)
				{
					omitGroupLine = false;
					lineNumber++;
					std::getline(inFile,line);
					lineStream = std::stringstream(line);

					while(lineStream >> token && !omitGroupLine)
					{
						if(token.compare("}") == 0)
						{
							//printf("}\n");
							omitGroupLine = true;
							endGroup = true;
						}

						else if(token.compare("Position") == 0)
						{
							if(lineStream >> token) { pointLight.position.x = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Position'.\n", lineNumber); return false; }
							if(lineStream >> token) { pointLight.position.y = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Position'.\n", lineNumber); return false; }
							if(lineStream >> token) { pointLight.position.z = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Position'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Position' (expected 3 values).\n", lineNumber); return false; }
						}

						else if(token.compare("Color") == 0)
						{
							if(lineStream >> token) { pointLight.color.x = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Color'.\n", lineNumber); return false; }
							if(lineStream >> token) { pointLight.color.y = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Color'.\n", lineNumber); return false; }
							if(lineStream >> token) { pointLight.color.z = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Color'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Color' (expected 3 values).\n", lineNumber); return false; }
						}

						else
						{
							cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Token '%s' is not a field of 'PointLight' .\n", lineNumber, token.c_str());
							return false;
						}
					}
				}

				scene->addPointLight(pointLight);

				cprintf(CONSOLE_COLOR_WHITE, "[");
				cprintf(CONSOLE_COLOR_GREEN, "OK");
				cprintf(CONSOLE_COLOR_WHITE, "] PointLight added.\n");
			}

			////////////////////////////////////////////////////////////////////////////////////
			// AreaLight parse
			////////////////////////////////////////////////////////////////////////////////////
			else if(token.compare("AreaLight") == 0)
			{
				//cprintf(CONSOLE_COLOR_BLUE, "Material\n");
				lineGroup = lineNumber;
				endGroup = false;
				areaLight = HostAreaLight();

				position = vec3(0,0,0);
				direction = vec3(0,-1,0);
				rollAngle = 0;
				width = 1;
				height = 1;
				subdivision = u_int2(5,5);

				// Get opener
				std::getline(inFile,line);
				lineNumber++;
				lineStream = std::stringstream(line);
				lineStream >> token;

				if(token.compare("{") != 0)
				{
					cprintf(CONSOLE_COLOR_RED,"[ERROR] Unexpected '%s' Expected '{' to begin AreaLight declaration.\n", token.c_str());
					return false;
				}

				while(!endGroup)
				{
					omitGroupLine = false;
					lineNumber++;
					std::getline(inFile,line);
					lineStream = std::stringstream(line);

					while(lineStream >> token && !omitGroupLine)
					{
						if(token.compare("}") == 0)
						{
							//printf("}\n");
							omitGroupLine = true;
							endGroup = true;
						}

						else if(token.compare("Color") == 0)
						{
							if(lineStream >> token) { areaLight.color.x = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Color'.\n", lineNumber); return false; }
							if(lineStream >> token) { areaLight.color.y = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Color'.\n", lineNumber); return false; }
							if(lineStream >> token) { areaLight.color.z = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Color'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Color' (expected 3 values).\n", lineNumber); return false; }
						}

						else if(token.compare("Position") == 0)
						{
							if(lineStream >> token) { position.x = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Position'.\n", lineNumber); return false; }
							if(lineStream >> token) { position.y = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Position'.\n", lineNumber); return false; }
							if(lineStream >> token) { position.z = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Position'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Position' (expected 3 values).\n", lineNumber); return false; }
						}

						else if(token.compare("Direction") == 0)
						{
							if(lineStream >> token) { direction.x = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Direction'.\n", lineNumber); return false; }
							if(lineStream >> token) { direction.y = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Direction'.\n", lineNumber); return false; }
							if(lineStream >> token) { direction.z = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Direction'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Direction' (expected 3 values).\n", lineNumber); return false; }
						}

						else if(token.compare("RollAngle") == 0)
						{
							if(lineStream >> token) { rollAngle = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 1 value to define 'RollAngle'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Direction' (expected 1 value).\n", lineNumber); return false; }
						}

						else if(token.compare("Width") == 0)
						{
							if(lineStream >> token) { width = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 1 value to define 'Width'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Width' (expected 1 value).\n", lineNumber); return false; }

							if(width <= 0)
							{
								cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: 'Width' value of zero or negateive is not allowed.\n", lineNumber); return false;
							}
						}

						else if(token.compare("Height") == 0)
						{
							if(lineStream >> token) { height = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 1 value to define 'Height'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Height' (expected 1 value).\n", lineNumber); return false; }

							if(height <= 0)
							{
								cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: 'Height' value of zero or negative is not allowed.\n", lineNumber); return false;
							}
						}

						else if(token.compare("Subdivision") == 0)
						{
							if(lineStream >> token) { subdivision.x = atoi(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 2 values to define 'Subdivision'.\n", lineNumber); return false; }
							if(lineStream >> token) { subdivision.y = atoi(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 2 values to define 'Subdivision'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Subdivision' (expected 2 values).\n", lineNumber); return false; }

							if(subdivision.x <= 0 || subdivision.y <= 0)
							{
								cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: 'Subdivision' values of zero or negative are not allowed.\n", lineNumber); return false;
							}
						}

						else
						{
							cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Token '%s' is not a field of 'AreaLight' .\n", lineNumber, token.c_str());
							return false;
						}
					}
				}

				areaLight.buildPlaneAreaLight(position,direction,rollAngle,width,height,subdivision);
				scene->addAreaLight(areaLight);

				cprintf(CONSOLE_COLOR_WHITE, "[");
				cprintf(CONSOLE_COLOR_GREEN, "OK");
				cprintf(CONSOLE_COLOR_WHITE, "] AreaLight added.\n");
			}

			////////////////////////////////////////////////////////////////////////////////////
			// Mesh parse
			////////////////////////////////////////////////////////////////////////////////////
			else if(token.compare("Mesh") == 0)
			{
				//cprintf(CONSOLE_COLOR_BLUE, "Material\n");
				lineGroup = lineNumber;
				endGroup = false;
				HostMesh mesh = HostMesh();

				// Get opener
				std::getline(inFile,line);
				lineNumber++;
				lineStream = std::stringstream(line);
				lineStream >> token;

				if(token.compare("{") != 0)
				{
					cprintf(CONSOLE_COLOR_RED,"[ERROR] Unexpected '%s' Expected '{' to begin Mesh declaration.\n", token.c_str());
					return false;
				}

				while(!endGroup)
				{
					omitGroupLine = false;
					lineNumber++;
					std::getline(inFile,line);
					lineStream = std::stringstream(line);

					while(lineStream >> token && !omitGroupLine)
					{
						if(token.compare("}") == 0)
						{
							//printf("}\n");
							omitGroupLine = true;
							endGroup = true;
						}

						else if(token.compare("Material") == 0)
						{
							if(lineStream >> token)
							{
								valid = false;
								for(int i=0; i<materialNames.size(); i++)
								{
									if(materialNames[i].compare(token) == 0)
									{
										mesh.setMaterial(i);
										valid = true;
										break;
									}
								}

								if(!valid)
								{
									cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Material '%s' don't exist.\n", lineNumber, token.c_str());
									return false;
								}
							}
							else
							{
								cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 1 value to define 'Material'.\n", lineNumber);
								return false;
							}
							if(lineStream >> token){ cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Material' (expected 1 value).\n", lineNumber); return false; }
						}

						else if(token.compare("Source") == 0)
						{
							std::getline(lineStream,token,'"');
							std::getline(lineStream,token,'"');
							if(!mesh.loadOBJ(token.c_str())) return false;
							//cprintf(CONSOLE_COLOR_YELLOW, "Loading mesh: %s\n", token.c_str());
							/*if(lineStream >> token)
							{
								cprintf(CONSOLE_COLOR_YELLOW, "Loading mesh: %s\n", token.c_str());
								if(!mesh.loadOBJ(token.c_str())) return false;
							}
							else
							{
								cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 1 value to define 'Source.\n", lineNumber);
								return false;
							}*/
						}

						else if(token.compare("Translation") == 0)
						{
							if(lineStream >> token) { tempVec3.x = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Translation'.\n", lineNumber); return false; }
							if(lineStream >> token) { tempVec3.y = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Translation'.\n", lineNumber); return false; }
							if(lineStream >> token) { tempVec3.z = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Translation'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Translation' (expected 3 values).\n", lineNumber); return false; }

							mesh.translation(tempVec3);
						}

						else if(token.compare("Rotation") == 0)
						{
							if(lineStream >> token) { tempVec3.x = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 4 values to define 'Rotation'.\n", lineNumber); return false; }
							if(lineStream >> token) { tempVec3.y = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 4 values to define 'Rotation'.\n", lineNumber); return false; }
							if(lineStream >> token) { tempVec3.z = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 4 values to define 'Rotation'.\n", lineNumber); return false; }
							if(lineStream >> token) {  tempFloat = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 4 values to define 'Rotation'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Translation' (expected 4 values).\n", lineNumber); return false; }
							if(tempVec3 == vec3(0.0,0.0,0.0)){cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Rotation axis cannot be (0,0,0).\n", lineNumber); return false;}
							mesh.rotation(normalize(tempVec3), tempFloat);
						}

						else if(token.compare("Scale") == 0)
						{
							if(lineStream >> token) { tempVec3.x = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Scale'.\n", lineNumber); return false; }
							if(lineStream >> token) { tempVec3.y = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Scale'.\n", lineNumber); return false; }
							if(lineStream >> token) { tempVec3.z = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 3 values to define 'Scale'.\n", lineNumber); return false; }
							if(lineStream >> token) { cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Scale' (expected 3 values).\n", lineNumber); return false; }

							if(tempVec3.x == 0 || tempVec3.y == 0 || tempVec3.z == 0)
							{
								cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Scaling values of zero not allowed.\n", lineNumber);
								return false;
							}

							mesh.scale(tempVec3);
						}
					}
				}

				mesh.buildBounds(u_int3(2,2,2));
				scene->addMesh(mesh);

				cprintf(CONSOLE_COLOR_WHITE, "[");
				cprintf(CONSOLE_COLOR_GREEN, "OK");
				cprintf(CONSOLE_COLOR_WHITE, "] Mesh added.\n");
			}

			////////////////////////////////////////////////////////////////////////////////////
			// CSG parse
			////////////////////////////////////////////////////////////////////////////////////
			else if(token.compare("CSG") == 0)
			{
				//cprintf(CONSOLE_COLOR_BLUE, "Material\n");
				lineGroup = lineNumber;
				endGroup = false;
				HostCSGTree tree = HostCSGTree();

				// Get opener
				std::getline(inFile,line);
				lineNumber++;
				lineStream = std::stringstream(line);
				lineStream >> token;

				if(token.compare("{") != 0)
				{
					cprintf(CONSOLE_COLOR_RED,"[ERROR] Unexpected '%s' Expected '{' to begin CSG declaration.\n", token.c_str());
					return false;
				}

				while(!endGroup)
				{
					omitGroupLine = false;
					lineNumber++;
					std::getline(inFile,line);
					lineStream = std::stringstream(line);

					while(lineStream >> token && !omitGroupLine)
					{
						if(token.compare("}") == 0)
						{
							//printf("}\n");
							omitGroupLine = true;
							endGroup = true;
						}
						else if(token.compare("Material") == 0)
						{
							if(lineStream >> token)
							{
								valid = false;
								for(int i=0; i<materialNames.size(); i++)
								{
									if(materialNames[i].compare(token) == 0)
									{
										tree.material = i;
										valid = true;
										break;
									}
								}

								if(!valid)
								{
									cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Material '%s' don't exist.\n", lineNumber, token.c_str());
									return false;
								}
							}
							else
							{
								cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Need 1 value to define 'Material'.\n", lineNumber);
								return false;
							}
							if(lineStream >> token){ cprintf(CONSOLE_COLOR_RED, "[ERROR] Line %d: Too many values defining 'Material' (expected 1 value).\n", lineNumber); return false; }
						}

						else if(token.compare("Source") == 0)
						{
							std::getline(lineStream,token,'"');
							std::getline(lineStream,token,'"');
							if(!parseCSGTreeFile(lineNumber, token.c_str(), &tree))
							{
								cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Failed to import the CSG file.\n", lineNumber);
								return false;
							}
						}
					}
				}

				tree.printContent(CONSOLE_COLOR_YELLOW);

				scene->addCSGTree(tree);

				cprintf(CONSOLE_COLOR_WHITE, "[");
				cprintf(CONSOLE_COLOR_GREEN, "OK");
				cprintf(CONSOLE_COLOR_WHITE, "] CSG added.\n");
			}

			else
			{
				cprintf(CONSOLE_COLOR_RED,"[ERROR] Line %d: Unexpected token: %s\n", lineNumber, token.c_str());
				return false;
				//omitLine = true;
			}
		}
	}

	inFile.close();

	return true;
}


bool parseCSGTreeFile(int _line, const char *filename, HostCSGTree *tree)
{
	int lineNumber = 0;
	bool omitLine;
	bool omitGroupLine;
	bool endGroup;
	std::string	line;
	std::string	token;
	std::stringstream lineStream;

	int		tempInt;
	vec3	tempVec3;
	float	tempFloat;

	//////////////////////////////////////////////////////////////

	CSG_Node		node;
	CSG_Box			box;
	CSG_Cylinder	cylinder;
	CSG_Sphere		sphere;

	//////////////////////////////////////////////////////////////

	std::ifstream inFile(filename);

	if(!inFile.is_open())
	{
		cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Error opening file '%s'\n", _line, filename);
		return false;
	}
	else
	{
		while(!inFile.eof())
		{
			lineNumber++;
			omitLine = false;
			std::getline(inFile,line);
			lineStream = std::stringstream(line);

			while(lineStream >> token && !omitLine)
			{
				//////////////////////////////////////////////////////////////
				// CSG_Box parse
				//////////////////////////////////////////////////////////////

				if(token.compare("Box") == 0)
				{
					box = CSG_Box();
					endGroup = false;
					omitGroupLine = false;

					std::getline(inFile,line);
					lineStream = std::stringstream(line);
					lineStream >> token;
					lineNumber++;

					if(token.compare("{") != 0)
					{
						cprintf(CONSOLE_COLOR_RED,"[CSG at scene line %d][ERROR] Line %d: Unexpected token '%s'\n", _line, lineNumber, token.c_str());
						return false;
					}
					
					while(!endGroup)
					{
						omitGroupLine = false;
						std::getline(inFile,line);
						lineNumber++;
						lineStream = std::stringstream(line);

						while(lineStream >> token && !omitGroupLine)
						{
							omitGroupLine = false;

							if(token.compare("Size") == 0)
							{
								if(lineStream >> token) { tempVec3.x = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'Size'.\n", _line, lineNumber); return false; }
								if(lineStream >> token) { tempVec3.y = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'Size'.\n", _line, lineNumber); return false; }
								if(lineStream >> token) { tempVec3.z = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'Size'.\n", _line, lineNumber); return false; }
								box.scale(tempVec3);
							}

							else if(token.compare("Translation") == 0)
							{
								if(lineStream >> token) { tempVec3.x = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'Translation'.\n", _line, lineNumber); return false; }
								if(lineStream >> token) { tempVec3.y = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'Translation'.\n", _line, lineNumber); return false; }
								if(lineStream >> token) { tempVec3.z = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'Translation'.\n", _line, lineNumber); return false; }
								box.translation(tempVec3);
							}

							else if(token.compare("Rotation") == 0)
							{
								if(lineStream >> token) { tempVec3.x = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 4 values to define 'Rotation'.\n", _line, lineNumber); return false; }
								if(lineStream >> token) { tempVec3.y = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 4 values to define 'Rotation'.\n", _line, lineNumber); return false; }
								if(lineStream >> token) { tempVec3.z = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 4 values to define 'Rotation'.\n", _line, lineNumber); return false; }
								if(lineStream >> token) { tempFloat  = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 4 values to define 'Rotation'.\n", _line, lineNumber); return false; }
								box.rotation(tempVec3, tempFloat);
							}

							else if(token.compare("Scale") == 0)
							{
								if(lineStream >> token) { tempVec3.x = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'Scale'.\n", _line, lineNumber); return false; }
								if(lineStream >> token) { tempVec3.y = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'Scale'.\n", _line, lineNumber); return false; }
								if(lineStream >> token) { tempVec3.z = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'Scale'.\n", _line, lineNumber); return false; }
								box.scale(tempVec3);
							}

							else if(token.compare("}") == 0)
							{
								omitGroupLine = true;
								endGroup = true;
							}
						}
					}

					tree->boxes.push_back(box);
				}
				// End CSG_Box parse

				//////////////////////////////////////////////////////////////
				// CSG_Cylinder parse
				//////////////////////////////////////////////////////////////

				else if(token.compare("Cylinder") == 0)
				{
					cylinder = CSG_Cylinder();
					endGroup = false;
					omitGroupLine = false;

					std::getline(inFile,line);
					lineStream = std::stringstream(line);
					lineStream >> token;
					lineNumber++;

					if(token.compare("{") != 0)
					{
						cprintf(CONSOLE_COLOR_RED,"[CSG at scene line %d][ERROR] Line %d: Unexpected token '%s'\n", _line, lineNumber, token.c_str());
						return false;
					}
					
					while(!endGroup)
					{
						omitGroupLine = false;
						std::getline(inFile,line);
						lineNumber++;
						lineStream = std::stringstream(line);

						while(lineStream >> token && !omitGroupLine)
						{
							omitGroupLine = false;

							if(token.compare("Base") == 0)
							{
								if(lineStream >> token) { tempVec3.x = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'Base'.\n", _line, lineNumber); return false; }
								if(lineStream >> token) { tempVec3.y = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'Base'.\n", _line, lineNumber); return false; }
								if(lineStream >> token) { tempVec3.z = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'Base'.\n", _line, lineNumber); return false; }
								cylinder.base = tempVec3;
							}

							else if(token.compare("Axis") == 0)
							{
								if(lineStream >> token) { tempVec3.x = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'Axis'.\n", _line, lineNumber); return false; }
								if(lineStream >> token) { tempVec3.y = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'Axis'.\n", _line, lineNumber); return false; }
								if(lineStream >> token) { tempVec3.z = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'Axis'.\n", _line, lineNumber); return false; }
								cylinder.axis = tempVec3;
							}

							else if(token.compare("Radius") == 0)
							{
								if(lineStream >> token) { tempFloat = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 1 value to define 'Radius'.\n", _line, lineNumber); return false; }
								cylinder.radius = tempFloat;
							}

							else if(token.compare("Height") == 0)
							{
								if(lineStream >> token) { tempFloat = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 1 value to define 'Height'.\n", _line, lineNumber); return false; }
								cylinder.height = tempFloat;
							}

							else if(token.compare("}") == 0)
							{
								omitGroupLine = true;
								endGroup = true;
							}

						}
					}
					cylinder.buildPlanes();

					tree->cylinders.push_back(cylinder);
				}
				// End CSG_Cylinder parse

				//////////////////////////////////////////////////////////////
				// CSG_Sphere parse
				//////////////////////////////////////////////////////////////

				else if(token.compare("Sphere") == 0)
				{
					sphere = CSG_Sphere();
					endGroup = false;
					omitGroupLine = false;

					std::getline(inFile,line);
					lineStream = std::stringstream(line);
					lineStream >> token;
					lineNumber++;

					if(token.compare("{") != 0)
					{
						cprintf(CONSOLE_COLOR_RED,"[CSG at scene line %d][ERROR] Line %d: Unexpected token '%s'\n", _line, lineNumber, token.c_str());
						return false;
					}
					
					while(!endGroup)
					{
						omitGroupLine = false;
						std::getline(inFile,line);
						lineNumber++;
						lineStream = std::stringstream(line);

						while(lineStream >> token && !omitGroupLine)
						{
							omitGroupLine = false;

							if(token.compare("Center") == 0)
							{
								if(lineStream >> token) { tempVec3.x = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'Center'.\n", _line, lineNumber); return false; }
								if(lineStream >> token) { tempVec3.y = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'Center'.\n", _line, lineNumber); return false; }
								if(lineStream >> token) { tempVec3.z = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'Center'.\n", _line, lineNumber); return false; }
								sphere.center = tempVec3;
							}

							if(token.compare("Radius") == 0)
							{
								if(lineStream >> token) { tempFloat = (float)atof(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 1 value to define 'radius'.\n", _line, lineNumber); return false; }
								sphere.radius = tempFloat;
							}

							else if(token.compare("}") == 0)
							{
								omitGroupLine = true;
								endGroup = true;
							}
						}
					}

					tree->spheres.push_back(sphere);
				}
				// End CSG_Sphere parse

				//////////////////////////////////////////////////////////////
				// Union parse
				//////////////////////////////////////////////////////////////

				else if(token.compare("Union") == 0)
				{
					node = CSG_Node();
					if(tree->operations.size() == 0) { node.type = OBJECT_TYPE_CSG_ROOT; } else { node.type = OBJECT_TYPE_CSG_NODE; }
					node.operation = CSG_OPERATION_UNION;

					endGroup = false;
					omitGroupLine = false;

					std::getline(inFile,line);
					lineStream = std::stringstream(line);
					lineStream >> token;
					lineNumber++;

					if(token.compare("{") != 0)
					{
						cprintf(CONSOLE_COLOR_RED,"[CSG at scene line %d][ERROR] Line %d: Unexpected token '%s'\n", _line, lineNumber, token.c_str());
						return false;
					}
					
					while(!endGroup)
					{
						omitGroupLine = false;
						std::getline(inFile,line);
						lineNumber++;
						lineStream = std::stringstream(line);

						while(lineStream >> token && !omitGroupLine)
						{
							omitGroupLine = false;

							if(token.compare("LeftType") == 0)
							{
								if(lineStream >> token) { tempInt = atoi(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'LeftChild'.\n", _line, lineNumber); return false; }
								node.type_a = tempInt;
							}

							else if(token.compare("LeftIndex") == 0)
							{
								if(lineStream >> token) { tempInt = atoi(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'LeftIndex'.\n", _line, lineNumber); return false; }
								node.index_a = tempInt;
							}

							if(token.compare("RightType") == 0)
							{
								if(lineStream >> token) { tempInt = atoi(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'RightChild'.\n", _line, lineNumber); return false; }
								node.type_b = tempInt;
							}

							else if(token.compare("RightIndex") == 0)
							{
								if(lineStream >> token) { tempInt = atoi(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'RightIndex'.\n", _line, lineNumber); return false; }
								node.index_b = tempInt;
							}

							else if(token.compare("}") == 0)
							{
								omitGroupLine = true;
								endGroup = true;
							}
						}
					}

					tree->operations.push_back(node);
				}
				// End Union parse

				//////////////////////////////////////////////////////////////
				// Difference parse
				//////////////////////////////////////////////////////////////

				else if(token.compare("Difference") == 0)
				{
					node = CSG_Node();
					if(tree->operations.size() == 0) { node.type = OBJECT_TYPE_CSG_ROOT; } else { node.type = OBJECT_TYPE_CSG_NODE; }
					node.operation = CSG_OPERATION_DIFFERENCE;

					endGroup = false;
					omitGroupLine = false;

					std::getline(inFile,line);
					lineStream = std::stringstream(line);
					lineStream >> token;
					lineNumber++;

					if(token.compare("{") != 0)
					{
						cprintf(CONSOLE_COLOR_RED,"[CSG at scene line %d][ERROR] Line %d: Unexpected token '%s'\n", _line, lineNumber, token.c_str());
						return false;
					}
					
					while(!endGroup)
					{
						omitGroupLine = false;
						std::getline(inFile,line);
						lineNumber++;
						lineStream = std::stringstream(line);

						while(lineStream >> token && !omitGroupLine)
						{
							omitGroupLine = false;

							if(token.compare("LeftType") == 0)
							{
								if(lineStream >> token) { tempInt = atoi(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'LeftChild'.\n", _line, lineNumber); return false; }
								node.type_a = tempInt;
							}

							else if(token.compare("LeftIndex") == 0)
							{
								if(lineStream >> token) { tempInt = atoi(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'LeftIndex'.\n", _line, lineNumber); return false; }
								node.index_a = tempInt;
							}

							if(token.compare("RightType") == 0)
							{
								if(lineStream >> token) { tempInt = atoi(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'RightChild'.\n", _line, lineNumber); return false; }
								node.type_b = tempInt;
							}

							else if(token.compare("RightIndex") == 0)
							{
								if(lineStream >> token) { tempInt = atoi(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'RightIndex'.\n", _line, lineNumber); return false; }
								node.index_b = tempInt;
							}

							else if(token.compare("}") == 0)
							{
								omitGroupLine = true;
								endGroup = true;
							}
						}
					}

					tree->operations.push_back(node);
				}
				// End Difference parse

				//////////////////////////////////////////////////////////////
				// Intersection parse
				//////////////////////////////////////////////////////////////

				else if(token.compare("Intersection") == 0)
				{
					node = CSG_Node();
					if(tree->operations.size() == 0) { node.type = OBJECT_TYPE_CSG_ROOT; } else { node.type = OBJECT_TYPE_CSG_NODE; }
					node.operation = CSG_OPERATION_INTERSECTION;

					endGroup = false;
					omitGroupLine = false;

					std::getline(inFile,line);
					lineStream = std::stringstream(line);
					lineStream >> token;
					lineNumber++;

					if(token.compare("{") != 0)
					{
						cprintf(CONSOLE_COLOR_RED,"[CSG at scene line %d][ERROR] Line %d: Unexpected token '%s'\n", _line, lineNumber, token.c_str());
						return false;
					}
					
					while(!endGroup)
					{
						omitGroupLine = false;
						std::getline(inFile,line);
						lineNumber++;
						lineStream = std::stringstream(line);

						while(lineStream >> token && !omitGroupLine)
						{
							omitGroupLine = false;

							if(token.compare("LeftType") == 0)
							{
								if(lineStream >> token) { tempInt = atoi(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'LeftChild'.\n", _line, lineNumber); return false; }
								node.type_a = tempInt;
							}

							else if(token.compare("LeftIndex") == 0)
							{
								if(lineStream >> token) { tempInt = atoi(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'LeftIndex'.\n", _line, lineNumber); return false; }
								node.index_a = tempInt;
							}

							if(token.compare("RightType") == 0)
							{
								if(lineStream >> token) { tempInt = atoi(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'RightChild'.\n", _line, lineNumber); return false; }
								node.type_b = tempInt;
							}

							else if(token.compare("RightIndex") == 0)
							{
								if(lineStream >> token) { tempInt = atoi(token.c_str()); } else { cprintf(CONSOLE_COLOR_RED, "[CSG at scene line %d][ERROR] Line %d: Need 3 values to define 'RightIndex'.\n", _line, lineNumber); return false; }
								node.index_b = tempInt;
							}

							else if(token.compare("}") == 0)
							{
								omitGroupLine = true;
								endGroup = true;
							}
						}
					}

					tree->operations.push_back(node);
				}
				// End Intersection parse

				else
				{
					cprintf(CONSOLE_COLOR_RED,"[CSG at scene line %d][ERROR] Line %d: Unexpected token: %s\n", _line, lineNumber, token.c_str());
					return false;
					//omitLine = true;
				}
			}
		}
	}

	inFile.close();
	return true;
}


void copySceneHostToDevice(HostScene *hostScene, DeviceScene *deviceScene)
{
	Material	**d_materials;	// Array of Material pointers
	Material	*d_material;	// device Material
	Box			**d_boxes;		// Array of Box pointers
	Box			*d_box;			// device Box
	Cone		**d_cones;		// Array of Cone pointers
	Cone		*d_cone;		// device Cone
	Cylinder	**d_cylinders;	// Array of Cylinder pointers
	Cylinder	*d_cylinder;	// device Cylinder
	Sphere		**d_spheres;	// Array of Sphere pointers
	Sphere		*d_sphere;		// device Sphere
	PerspectiveCamera *d_camera;

	PointLight			**d_pointLights;
	PointLight			*d_pointLight;
	DeviceAreaLight		**d_areaLights;
	DeviceAreaLight		*d_areaLight, h_areaLight;
	vec3				*d_areaLightPoints;
	//vec3				*d_areaLightNormals;
	vec3				*d_areaLightTangents;
	vec3				*d_areaLightBinormals;
	Plane				*d_areaLightPlane;

	//cudaError_t __err;


	printf("\tCopying Camera ... ");
	cudaMalloc((void**)&d_camera, sizeof(PerspectiveCamera));
	cudaMemcpy(d_camera, &hostScene->camera, sizeof(PerspectiveCamera), cudaMemcpyHostToDevice);
	deviceScene->setCamera(d_camera);
	cprintf(CONSOLE_COLOR_DARKGREEN,"%s\n",cudaGetErrorString(cudaGetLastError()));


	copyTexturesHostToDevice(hostScene,deviceScene);

	
	printf("\tCopying %d materials ... ", hostScene->materials.size());
	if(hostScene->materials.size() > 0)
	{
		cudaMalloc((void**)&d_materials, sizeof(Material*)*hostScene->materials.size());

		for(int i=0; i<hostScene->materials.size(); i++)
		{
			cudaMalloc((void**)&d_material, sizeof(Material));
			cudaMemcpy(d_material, &hostScene->materials[i], sizeof(Material), cudaMemcpyHostToDevice);
			cudaMemcpy(&d_materials[i], &d_material, sizeof(Material*), cudaMemcpyHostToDevice);
		}
		deviceScene->setMaterials(d_materials,hostScene->materials.size());
	}
	cprintf(CONSOLE_COLOR_DARKGREEN,"%s\n",cudaGetErrorString(cudaGetLastError()));


	printf("\tCopying %d boxes ... ", hostScene->boxes.size());
	if(hostScene->boxes.size() > 0)
	{
		cudaMalloc((void**)&d_boxes, sizeof(Box*)*hostScene->boxes.size());							// Allocate space for n Box pointers in GPU

		for(int i=0; i<hostScene->boxes.size(); i++)
		{
			hostScene->boxes[i].setIndex(i);
			cudaMalloc((void**)&d_box, sizeof(Box));												// Allocate space for one Box in GPU
			cudaMemcpy(d_box, &hostScene->boxes[i], sizeof(Box), cudaMemcpyHostToDevice);			// Copy the Box in the host scene to the allocated space
			cudaMemcpy(&d_boxes[i], &d_box, sizeof(Box*), cudaMemcpyHostToDevice);					// Copy the reference to the list of Box pointers
		}
		deviceScene->setBoxes(d_boxes,hostScene->boxes.size());										// Copy the array and the size to the field in deviceScene
	}
	else
	{
		deviceScene->numBoxes = 0;
	}
	cprintf(CONSOLE_COLOR_DARKGREEN,"%s\n",cudaGetErrorString(cudaGetLastError()));


	printf("\tCopying %d cones ... ", hostScene->cones.size());
	if(hostScene->cones.size() > 0)
	{
		cudaMalloc((void**)&d_cones, sizeof(Cone*)*hostScene->cones.size());						// Allocate space for n Cone pointers in GPU

		for(int i=0; i<hostScene->cones.size(); i++)
		{
			hostScene->cones[i].setIndex(i);
			cudaMalloc((void**)&d_cone, sizeof(Cone));												// Allocate space for one Cone in GPU
			cudaMemcpy(d_cone, &hostScene->cones[i], sizeof(Cone), cudaMemcpyHostToDevice);			// Copy the Cone in the host scene to the allocated space
			cudaMemcpy(&d_cones[i], &d_cone, sizeof(Cone*), cudaMemcpyHostToDevice);				// Copy the reference to the list of Cone pointers
		}
		deviceScene->setCones(d_cones,hostScene->cones.size());										// Copy the array and the size to the field in deviceScene
	}
	else
	{
		deviceScene->numCones = 0;
	}
	cprintf(CONSOLE_COLOR_DARKGREEN,"%s\n",cudaGetErrorString(cudaGetLastError()));


	printf("\tCopying %d cylinders ... ", hostScene->cylinders.size());
	if(hostScene->cylinders.size() > 0)
	{
		cudaMalloc((void**)&d_cylinders, sizeof(Cylinder*)*hostScene->cylinders.size());			// Allocate space for n Cylinder pointers in GPU

		for(int i=0; i<hostScene->cylinders.size(); i++)
		{
			hostScene->cylinders[i].setIndex(i);
			cudaMalloc((void**)&d_cylinder, sizeof(Cylinder));										// Allocate space for one Cylinder in GPU
			cudaMemcpy(d_cylinder, &hostScene->cylinders[i], sizeof(Cylinder), cudaMemcpyHostToDevice);
			cudaMemcpy(&d_cylinders[i], &d_cylinder, sizeof(Cylinder*), cudaMemcpyHostToDevice);
		}
		deviceScene->setCylinders(d_cylinders,hostScene->cylinders.size());							// Copy the array and the size to the field in deviceScene
	}
	else
	{
		deviceScene->numCylinders = 0;
	}
	cprintf(CONSOLE_COLOR_DARKGREEN,"%s\n",cudaGetErrorString(cudaGetLastError()));


	printf("\tCopying %d spheres ... ", hostScene->spheres.size());
	if(hostScene->spheres.size() > 0)
	{
		cudaMalloc((void**)&d_spheres, sizeof(Sphere*)*hostScene->spheres.size());					// Allocate space for n Sphere pointers in GPU

		for(int i=0; i<hostScene->spheres.size(); i++)
		{
			hostScene->spheres[i].setIndex(i);
			cudaMalloc((void**)&d_sphere, sizeof(Sphere));											// Allocate space for one Sphere in GPU
			cudaMemcpy(d_sphere, &hostScene->spheres[i], sizeof(Sphere), cudaMemcpyHostToDevice);	// Copy the Sphere in the host scene to the allocated space
			cudaMemcpy(&d_spheres[i], &d_sphere, sizeof(Sphere*), cudaMemcpyHostToDevice);			// Copy the reference to the list of Sphere pointers
		}
		deviceScene->setSpheres(d_spheres,hostScene->spheres.size());								// Copy the array and the size to the field in deviceScene
	}
	else
	{
		deviceScene->numSpheres = 0;
	}
	cprintf(CONSOLE_COLOR_DARKGREEN,"%s\n",cudaGetErrorString(cudaGetLastError()));

	// Meshes

	printf("\tCopying %d meshes ...", hostScene->meshes.size());
	if(hostScene->meshes.size() > 0)
	{
		copyMeshesHostToDevice(hostScene,deviceScene);
	}
	else
	{
		deviceScene->numMeshes = 0;
	}
	cprintf(CONSOLE_COLOR_DARKGREEN,"%s\n",cudaGetErrorString(cudaGetLastError()));

	// CSG

	//printf("\tCopying %d CSG trees ... ", hostScene->csgTrees.size());
	if(hostScene->csgTrees.size() > 0)
	{
		cprintf(CONSOLE_COLOR_YELLOW, "Starting to copy CSG\n");
		copyCSGTrees(hostScene, deviceScene);
	}
	else
	{
		deviceScene->numCSGTrees = 0;
	}
	cprintf(CONSOLE_COLOR_DARKGREEN,"%s\n",cudaGetErrorString(cudaGetLastError()));

	// Point Lights

	printf("\tCopying %d point lights ... ", hostScene->pointLights.size());
	if(hostScene->pointLights.size() > 0)
	{
		cudaMalloc((void**)&d_pointLights, sizeof(PointLight*)*hostScene->pointLights.size());

		for(int i=0; i<hostScene->pointLights.size(); i++)
		{
			cudaMalloc((void**)&d_pointLight, sizeof(PointLight));
			cudaMemcpy(d_pointLight, &hostScene->pointLights[i], sizeof(PointLight), cudaMemcpyHostToDevice);
			cudaMemcpy(&d_pointLights[i], &d_pointLight, sizeof(PointLight*), cudaMemcpyHostToDevice);
		}
		deviceScene->setPointLights(d_pointLights, hostScene->pointLights.size());
	}
	else
	{
		deviceScene->numPointLights = 0;
	}
	cprintf(CONSOLE_COLOR_DARKGREEN,"%s\n",cudaGetErrorString(cudaGetLastError()));

	// Area Lights

	printf("\tCopying %d area lights ... ", hostScene->areaLights.size());
	if(hostScene->areaLights.size() > 0)
	{
		cudaMalloc((void**)&d_areaLights, sizeof(DeviceAreaLight*)*hostScene->areaLights.size());

		for(int i=0; i<hostScene->areaLights.size(); i++)
		{
			h_areaLight = DeviceAreaLight(hostScene->areaLights[i].color);

			switch(hostScene->areaLights[i].type)
			{
			case AREA_LIGHT_PLANE:
				// Allocate space for the points of this light
				cudaMalloc((void**)&d_areaLightPoints, sizeof(vec3)*hostScene->areaLights[i].points.size());
				cudaMalloc((void**)&d_areaLightTangents, sizeof(vec3)*hostScene->areaLights[i].tangents.size());
				cudaMalloc((void**)&d_areaLightBinormals, sizeof(vec3)*hostScene->areaLights[i].binormals.size());
				
				// Copy the points
				for(int j=0; j<hostScene->areaLights[i].points.size(); j++)
				{
					cudaMemcpy(&d_areaLightPoints[j], &hostScene->areaLights[i].points[j], sizeof(vec3), cudaMemcpyHostToDevice);
					cudaMemcpy(&d_areaLightTangents[j], &hostScene->areaLights[i].tangents[j], sizeof(vec3), cudaMemcpyHostToDevice);
					cudaMemcpy(&d_areaLightBinormals[j], &hostScene->areaLights[i].binormals[j], sizeof(vec3), cudaMemcpyHostToDevice);
				}

				// Assign this vector pointer on the areaLight object
				//h_areaLight.setPoints(d_areaLightPoints, hostScene->areaLights[i].points.size(), AREA_LIGHT_PLANE);
				h_areaLight.setPoints(d_areaLightPoints, d_areaLightTangents, d_areaLightBinormals, hostScene->areaLights[i].points.size(), AREA_LIGHT_PLANE);

				// Copy the plane
				cudaMalloc((void**)&d_areaLightPlane, sizeof(Plane));
				cudaMemcpy(d_areaLightPlane, &hostScene->areaLights[i].plane, sizeof(Plane), cudaMemcpyHostToDevice);
				h_areaLight.setPlane(d_areaLightPlane);

				h_areaLight.setSpacing(hostScene->areaLights[i].spacing);

				cudaMalloc((void**)&d_areaLight, sizeof(DeviceAreaLight));
				cudaMemcpy(d_areaLight, &h_areaLight, sizeof(DeviceAreaLight), cudaMemcpyHostToDevice);
				cudaMemcpy(&d_areaLights[i], &d_areaLight, sizeof(DeviceAreaLight*), cudaMemcpyHostToDevice);		
				break;

			case AREA_LIGHT_SPHERE:
				break;
			default:
				break;
			}
		}
		deviceScene->setAreaLights(d_areaLights, hostScene->areaLights.size());
	}
	else
	{
		deviceScene->numAreaLights = 0;
	}
	cprintf(CONSOLE_COLOR_DARKGREEN,"%s\n",cudaGetErrorString(cudaGetLastError()));
}

/*
printf("\tCopying %d materials ...", hostScene->materials.size());
if(hostScene->materials.size() > 0)
{
	cudaMalloc((void**)&d_materials, sizeof(Material*)*hostScene->materials.size());

	for(int i=0; i<hostScene->materials.size(); i++)
	{
		cudaMalloc((void**)&d_material, sizeof(Material));
		cudaMemcpy(d_material, &hostScene->materials[i], sizeof(Material), cudaMemcpyHostToDevice);
		cudaMemcpy(&d_materials[i], &d_material, sizeof(Material*), cudaMemcpyHostToDevice);
	}
	deviceScene->setMaterials(d_materials,hostScene->materials.size());
}
cprintf(CONSOLE_COLOR_DARKGREEN,"%s\n",cudaGetErrorString(cudaGetLastError()));
*/

void copyTexturesHostToDevice(HostScene *hostScene, DeviceScene *deviceScene)
{
	DeviceTexture	**d_textures;
	DeviceTexture	*d_texture;
	DeviceTexture	h_texture;
	vec3	*data;

	printf("\tCopying %d textures ... ", hostScene->textures.size());
	if(hostScene->textures.size() > 0)
	{
		cudaMalloc((void**)&d_textures, sizeof(DeviceTexture*)*hostScene->textures.size());

		for(int i=0; i<hostScene->textures.size(); i++)
		{
			// Copy the data (pixels)
			cudaMalloc((void**)&data, sizeof(vec3)*hostScene->textures[i].data.size());
			cudaMemcpy(data, hostScene->textures[i].data.data(), sizeof(vec3)*hostScene->textures[i].data.size(), cudaMemcpyHostToDevice);

			h_texture = DeviceTexture();
			h_texture.setTexture(data, hostScene->textures[i].width, hostScene->textures[i].height);

			cudaMalloc((void**)&d_texture, sizeof(DeviceTexture));
			cudaMemcpy(d_texture, &h_texture, sizeof(DeviceTexture), cudaMemcpyHostToDevice);
			cudaMemcpy(&d_textures[i], &d_texture, sizeof(DeviceTexture*), cudaMemcpyHostToDevice);
		}

		deviceScene->setTextures(d_textures, hostScene->textures.size());
	}
	else
	{
		deviceScene->numTextures = 0;
	}
	cprintf(CONSOLE_COLOR_DARKGREEN,"%s\n",cudaGetErrorString(cudaGetLastError()));
}


void copyMeshesHostToDevice(HostScene *hostScene, DeviceScene *deviceScene)
{
	int numBoundingBoxes = 0;
	std::vector<HostBoundingBox> boundingBoxes;

	// Mesh
	DeviceMesh	**d_meshes;
	DeviceMesh	*d_mesh;
	DeviceMesh	h_mesh;
	int			*d_elements;
	vec3		*d_positions;
	vec2		*d_uvs;
	vec3		*d_vnormals;
	vec3		*d_fnormals;
	vec4		*d_tangents;

	// Bounding boxes
	DeviceBoundingBox	*d_boundingBoxes;
	//std::vector<DeviceBoundingBox> boundingBoxes;
	int  *d_bb_polygons;
	int  *d_bb_children;
	int  *d_bb_elements;
	vec3 *d_bb_positions;
	vec3 *d_bb_fnormals;
	//DeviceBoundingBox	boundingBox;

	////////////////////////////////////////////////////////////////////////////
	// Device Meshes
	////////////////////////////////////////////////////////////////////////////

	cudaMalloc((void**)&d_meshes, sizeof(DeviceMesh*) * hostScene->meshes.size());

	for(int i=0; i<hostScene->meshes.size(); i++)
	{
		////printf("Transfering Mesh (%d)\n", i);
		////////////////////////////////////////////////////////////////////////////
		// Device Mesh
		////////////////////////////////////////////////////////////////////////////

			cudaMalloc((void**)&d_mesh, sizeof(DeviceMesh));

		////////////////////////////////////////////////////////////////////////////
		// Mesh Geometry info
		////////////////////////////////////////////////////////////////////////////

		// Mesh elements a.k.a. polygons indices
			cudaMalloc((void**)&d_elements, sizeof(int) * hostScene->meshes[i].elements.size());
			/*for(int j=0; j<hostScene->meshes[i].elements.size(); j++)
			{
				cudaMemcpy(&d_elements[j], &hostScene->meshes[i].elements[j], sizeof(int), cudaMemcpyHostToDevice);
			}*/
			cudaMemcpy(d_elements, hostScene->meshes[i].elements.data(), sizeof(int) * hostScene->meshes[i].elements.size(), cudaMemcpyHostToDevice);
			h_mesh.setElements(d_elements, hostScene->meshes[i].elements.size());

		// Mesh vertex positions
			cudaMalloc((void**)&d_positions, sizeof(vec3) * hostScene->meshes[i].positions.size());
			/*for(int j=0; j<hostScene->meshes[i].positions.size(); j++)
			{
				cudaMemcpy(&d_positions[j], &hostScene->meshes[i].positions[j], sizeof(vec3), cudaMemcpyHostToDevice);
			}*/
			cudaMemcpy(d_positions, hostScene->meshes[i].positions.data(), sizeof(vec3) * hostScene->meshes[i].positions.size(), cudaMemcpyHostToDevice);
			h_mesh.setPositions(d_positions, hostScene->meshes[i].positions.size());

		// Mesh vertex texture coordinates
			cudaMalloc((void**)&d_uvs, sizeof(vec2) * hostScene->meshes[i].uvs.size());
			/*for(int j=0; j<hostScene->meshes[i].uvs.size(); j++)
			{
				cudaMemcpy(&d_uvs[j], &hostScene->meshes[i].uvs[j], sizeof(vec2), cudaMemcpyHostToDevice);
			}*/
			cudaMemcpy(d_uvs, hostScene->meshes[i].uvs.data(), sizeof(vec2) * hostScene->meshes[i].uvs.size(), cudaMemcpyHostToDevice);
			h_mesh.setUVs(d_uvs, hostScene->meshes[i].uvs.size());

		// Mesh vertex normals
			cudaMalloc((void**)&d_vnormals, sizeof(vec3) * hostScene->meshes[i].vnormals.size());
			/*for(int j=0; j<hostScene->meshes[i].vnormals.size(); j++)
			{
				cudaMemcpy(&d_vnormals[j], &hostScene->meshes[i].vnormals[j], sizeof(vec3), cudaMemcpyHostToDevice);
			}*/
			cudaMemcpy(d_vnormals, hostScene->meshes[i].vnormals.data(), sizeof(vec3) * hostScene->meshes[i].vnormals.size(), cudaMemcpyHostToDevice);
			h_mesh.setVertexNormals(d_vnormals, hostScene->meshes[i].vnormals.size());

		// Mesh face normals
			cudaMalloc((void**)&d_fnormals, sizeof(vec3) * hostScene->meshes[i].fnormals.size());
			/*for(int j=0; j<hostScene->meshes[i].fnormals.size(); j++)
			{
				cudaMemcpy(&d_fnormals[j], &hostScene->meshes[i].fnormals[j], sizeof(vec3), cudaMemcpyHostToDevice);
			}*/
			cudaMemcpy(d_fnormals, hostScene->meshes[i].fnormals.data(), sizeof(vec3) * hostScene->meshes[i].fnormals.size(), cudaMemcpyHostToDevice);
			h_mesh.setFaceNormals(d_fnormals, hostScene->meshes[i].fnormals.size());

		// Mesh vertex tangents
			cudaMalloc((void**)&d_tangents, sizeof(vec4) * hostScene->meshes[i].tangents.size());
			/*for(int j=0; j<hostScene->meshes[i].tangents.size(); j++)
			{
				cudaMemcpy(&d_tangents[j], &hostScene->meshes[i].tangents[j], sizeof(vec4), cudaMemcpyHostToDevice);
			}*/
			cudaMemcpy(d_tangents, hostScene->meshes[i].tangents.data(), sizeof(vec4) * hostScene->meshes[i].tangents.size(), cudaMemcpyHostToDevice);
			h_mesh.setTangents(d_tangents, hostScene->meshes[i].tangents.size());

		////////////////////////////////////////////////////////////////////////////
		// Bounding Boxes
		////////////////////////////////////////////////////////////////////////////

		// Obtain an array of bounding boxes for this mesh
			hostScene->meshes[i].bounds.recursiveMark(&numBoundingBoxes);
			boundingBoxes.push_back(hostScene->meshes[i].bounds);
			hostScene->meshes[i].bounds.toArray(&boundingBoxes);
			numBoundingBoxes++;

			////printf("\tTransfering (%d) Bounding Boxes:\n", numBoundingBoxes);

		// Allocate space for this array on GPU
			cudaMalloc((void**)&d_boundingBoxes, sizeof(DeviceBoundingBox) * numBoundingBoxes);
			
		// Convert all the HostBoundingBoxes to DeviceBoundingBoxes
			for(int j=0; j<numBoundingBoxes; j++)
			{
				/////printf("\t\tTransfering Bounding Box (%d)\n", j);

				DeviceBoundingBox boundingBox;

				// Associated polygons & mesh info
				cudaMalloc((void**)&d_bb_polygons, sizeof(int) * boundingBoxes[j].meshPolygons.size());
				////printf("\t\t\tTransfering (%d) associated polygons\n", boundingBoxes[j].meshPolygons.size());
				/*for(int k=0; k<boundingBoxes[j].meshPolygons.size(); k++)
				{
					cudaMemcpy(&d_bb_polygons[k], &boundingBoxes[j].meshPolygons[k], sizeof(int), cudaMemcpyHostToDevice);
				}*/
				cudaMemcpy(d_bb_polygons, boundingBoxes[j].meshPolygons.data(), sizeof(int) * boundingBoxes[j].meshPolygons.size(), cudaMemcpyHostToDevice);
				boundingBox.setMeshInformation(d_bb_polygons, boundingBoxes[j].meshPolygons.size(), d_positions, d_uvs, d_vnormals, d_fnormals, d_tangents);

				// Children
				cudaMalloc((void**)&d_bb_children, sizeof(int) * boundingBoxes[j].children.size());
				////printf("\t\t\tTransfering (%d) children\n", boundingBoxes[j].children.size());
				/*for(int k=0; k<boundingBoxes[j].children.size(); k++)
				{
					cudaMemcpy(&d_bb_children[k], &boundingBoxes[j].childrenIndices[k], sizeof(int), cudaMemcpyHostToDevice);
				}*/
				cudaMemcpy(d_bb_children, boundingBoxes[j].childrenIndices.data(), sizeof(int) * boundingBoxes[j].children.size(), cudaMemcpyHostToDevice);
				boundingBox.setChildren(d_bb_children, boundingBoxes[j].children.size());

				// BoundingBox array
				////printf("\t\t\tTransfering array reference\n");
				boundingBox.setBoundingBoxArray(d_boundingBoxes);

				// BoundingBox definition: elements
				////printf("\t\t\tTransfering box definition\n");
				cudaMalloc((void**)&d_bb_elements, sizeof(int)*24);
				/*for(int k=0; k<24; k++)
				{
					cudaMemcpy(&d_bb_elements[k], &boundingBoxes[j].elements[k], sizeof(int), cudaMemcpyHostToDevice);
				}*/
				cudaMemcpy(d_bb_elements, boundingBoxes[j].elements.data(), sizeof(int) * boundingBoxes[j].elements.size(), cudaMemcpyHostToDevice);
				// BoundingBox definition: positions
				cudaMalloc((void**)&d_bb_positions, sizeof(vec3)*8);
				/*for(int k=0; k<8; k++)
				{
					cudaMemcpy(&d_bb_positions[k], &boundingBoxes[j].position[k], sizeof(vec3), cudaMemcpyHostToDevice);
				}*/
				cudaMemcpy(d_bb_positions, boundingBoxes[j].position.data(), sizeof(vec3) * boundingBoxes[j].position.size(), cudaMemcpyHostToDevice);
				// BoundingBox definition: face normals
				cudaMalloc((void**)&d_bb_fnormals, sizeof(vec3)*6);
				/*for(int k=0; k<6; k++)
				{
					cudaMemcpy(&d_bb_fnormals[k], &boundingBoxes[j].fnormals[k], sizeof(vec3), cudaMemcpyHostToDevice);
				}*/
				cudaMemcpy(d_bb_fnormals, boundingBoxes[j].fnormals.data(), sizeof(vec3) * boundingBoxes[j].fnormals.size(), cudaMemcpyHostToDevice);
				boundingBox.setBoundingBoxDefinition(d_bb_elements, d_bb_positions, d_bb_fnormals, boundingBoxes[j].boundsMin, boundingBoxes[j].boundsMax);

				cudaMemcpy(&d_boundingBoxes[j], &boundingBox, sizeof(DeviceBoundingBox), cudaMemcpyHostToDevice);

				////printf("\t\tDONE\n");
			}

			h_mesh.setBoundingBoxes(d_boundingBoxes);

			h_mesh.setMaterial(hostScene->meshes[i].material);

		boundingBoxes.clear();
		//__err = cudaGetLastError();
		//printf("Mesh transfer PRE: %s\n", cudaGetErrorString(__err));

		cudaMemcpy(d_mesh, &h_mesh, sizeof(DeviceMesh), cudaMemcpyHostToDevice);
		cudaMemcpy(&d_meshes[i], &d_mesh, sizeof(DeviceMesh*), cudaMemcpyHostToDevice);
	}

	deviceScene->setMeshes(d_meshes, hostScene->meshes.size());
}

void copyCSGTrees(HostScene *hostScene, DeviceScene *deviceScene)
{
	CSG_Box	*d_box, **d_boxes;
	CSG_Cylinder *d_cylinder, **d_cylinders;
	CSG_Sphere *d_sphere, **d_spheres;
	CSG_Node *d_node, **d_nodes;
	CSG_Node **d_trees;

	int numBoxes;
	int numCylinders;
	int numSpheres;
	int numOperations;

	// Allocate space for pointer array
	cudaMalloc((void**)&d_trees, sizeof(CSG_Node*)*hostScene->csgTrees.size());

	for(int i=0; i<hostScene->csgTrees.size(); i++)
	{

		// Boxes
		numBoxes = hostScene->csgTrees[i].boxes.size();
		if(numBoxes > 0)
		{
			d_boxes = new CSG_Box*[numBoxes];
			//cudaMalloc((void**)&d_boxes, sizeof(CSG_Box*)*hostScene->csgTrees[i].boxes.size());
			//cudaMemcpy(d_boxes, hostScene->csgTrees[i].boxes.data(), sizeof(CSG_Box)*hostScene->csgTrees[i].boxes.size(), cudaMemcpyHostToDevice);
			for(int j=0; j<numBoxes; j++)
			{
				cudaMalloc((void**)&d_box, sizeof(CSG_Box));
				cudaMemcpy(d_box, &hostScene->csgTrees[i].boxes[j], sizeof(CSG_Box), cudaMemcpyHostToDevice);
				d_boxes[j] = d_box;
				//cudaMemcpy(d_boxes[j], &d_box, sizeof(CSG_Box*), cudaMemcpyHostToDevice);
			}
		}

		// Cylinders
		numCylinders = hostScene->csgTrees[i].cylinders.size();
		if(numCylinders > 0)
		{
			d_cylinders = new CSG_Cylinder*[numCylinders];
			//cudaMalloc((void**)&d_cylinders, sizeof(CSG_Cylinder)*hostScene->csgTrees[i].cylinders.size());
			//cudaMemcpy(d_cylinders, hostScene->csgTrees[i].cylinders.data(), sizeof(CSG_Cylinder)*hostScene->csgTrees[i].cylinders.size(), cudaMemcpyHostToDevice);
			for(int j=0; j<numCylinders; j++)
			{
				cudaMalloc((void**)&d_cylinder, sizeof(CSG_Cylinder));
				cudaMemcpy(d_cylinder, &hostScene->csgTrees[i].cylinders[j], sizeof(CSG_Cylinder), cudaMemcpyHostToDevice);
				d_cylinders[j] = d_cylinder;
			}
		}

		// Spheres
		numSpheres = hostScene->csgTrees[i].spheres.size();
		if(numSpheres > 0)
		{
			d_spheres = new CSG_Sphere*[numSpheres];
			//cudaMalloc((void**)&d_spheres, sizeof(CSG_Sphere)*hostScene->csgTrees[i].spheres.size());
			//cudaMemcpy(d_spheres, hostScene->csgTrees[i].spheres.data(), sizeof(CSG_Sphere)*hostScene->csgTrees[i].spheres.size(), cudaMemcpyHostToDevice);
			for(int j=0; j<numSpheres; j++)
			{
				cudaMalloc((void**)&d_sphere, sizeof(CSG_Sphere));
				cudaMemcpy(d_sphere, &hostScene->csgTrees[i].spheres[j], sizeof(CSG_Sphere), cudaMemcpyHostToDevice);
				d_spheres[j] = d_sphere;
			}
		}

		// Operation nodes
		//cudaMalloc((void**)&d_nodes, sizeof(CSG_Node)*hostScene->csgTrees[i].operations.size());
		//cudaMemcpy(d_nodes, hostScene->csgTrees[i].operations.data(), sizeof(CSG_Node)*hostScene->csgTrees[i].operations.size(), cudaMemcpyHostToDevice);

		numOperations = hostScene->csgTrees[i].operations.size();
		d_nodes = new CSG_Node*[numOperations];
		for(int j=0; j<numOperations; j++)
		{
			cudaMalloc((void**)&d_node, sizeof(CSG_Node));
			cudaMemcpy(d_node, &hostScene->csgTrees[i].operations[j], sizeof(CSG_Node), cudaMemcpyHostToDevice);
			d_nodes[j] = d_node;
		}

		hostScene->csgTrees[i].operations[0].n_primitives = numBoxes + numCylinders + numSpheres;

		for(int j=0; j<hostScene->csgTrees[i].operations.size(); j++)
		{
			//cprintf(CONSOLE_COLOR_YELLOW,"\n");
			//cprintf(CONSOLE_COLOR_YELLOW,"NODE %d -> OPERATION = %d\n", j, hostScene->csgTrees[i].operations[j].operation);
			//cprintf(CONSOLE_COLOR_YELLOW,"\tLEFT -> TYPE = %d | INDEX = %d\n",hostScene->csgTrees[i].operations[j].type_a, hostScene->csgTrees[i].operations[j].index_a);
			//cprintf(CONSOLE_COLOR_YELLOW,"\tRIGHT -> TYPE = %d | INDEX = %d\n",hostScene->csgTrees[i].operations[j].type_b, hostScene->csgTrees[i].operations[j].index_b);

			//printf("\n%d\n",hostScene->csgTrees[i].material);
			hostScene->csgTrees[i].operations[j].material = hostScene->csgTrees[i].material;

			switch(hostScene->csgTrees[i].operations[j].type_a)
			{
				case OBJECT_TYPE_CSG_BOX:
					hostScene->csgTrees[i].operations[j].addChild(CSG_CHILD_LEFT, d_boxes[hostScene->csgTrees[i].operations[j].index_a]);
					break;
				case OBJECT_TYPE_CSG_CYLINDER:
					hostScene->csgTrees[i].operations[j].addChild(CSG_CHILD_LEFT, d_cylinders[hostScene->csgTrees[i].operations[j].index_a]);
					break;
				case OBJECT_TYPE_CSG_SPHERE:
					hostScene->csgTrees[i].operations[j].addChild(CSG_CHILD_LEFT, d_spheres[hostScene->csgTrees[i].operations[j].index_a]);
					break;	
				case OBJECT_TYPE_CSG_NODE:
					hostScene->csgTrees[i].operations[j].addChild(CSG_CHILD_LEFT, d_nodes[hostScene->csgTrees[i].operations[j].index_a]);
					break;
				default:
					break;
			}

			switch(hostScene->csgTrees[i].operations[j].type_b)
			{
				case OBJECT_TYPE_CSG_BOX:
					hostScene->csgTrees[i].operations[j].addChild(CSG_CHILD_RIGHT, d_boxes[hostScene->csgTrees[i].operations[j].index_b]);
					break;
				case OBJECT_TYPE_CSG_CYLINDER:
					hostScene->csgTrees[i].operations[j].addChild(CSG_CHILD_RIGHT, d_cylinders[hostScene->csgTrees[i].operations[j].index_b]);
					break;
				case OBJECT_TYPE_CSG_SPHERE:
					hostScene->csgTrees[i].operations[j].addChild(CSG_CHILD_RIGHT, d_spheres[hostScene->csgTrees[i].operations[j].index_b]);
					break;	
				case OBJECT_TYPE_CSG_NODE:
					hostScene->csgTrees[i].operations[j].addChild(CSG_CHILD_RIGHT, d_nodes[hostScene->csgTrees[i].operations[j].index_b]);
					break;
				default:
					break;
			}
		}
		
		//cudaMemcpy(d_nodes, hostScene->csgTrees[i].operations.data(), sizeof(CSG_Node)*hostScene->csgTrees[i].operations.size(), cudaMemcpyHostToDevice);
		for(int j=0; j<hostScene->csgTrees[i].operations.size(); j++)
		{
			cudaMemcpy(d_nodes[j], &hostScene->csgTrees[i].operations[j], sizeof(CSG_Node), cudaMemcpyHostToDevice);
		}

		cudaMemcpy(&d_trees[i], &d_nodes[0], sizeof(CSG_Node*), cudaMemcpyHostToDevice);
	}
	deviceScene->setCSGTrees(d_trees, hostScene->csgTrees.size());
}