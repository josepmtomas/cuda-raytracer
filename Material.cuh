#ifndef _MATERIAL_
#define _MATERIAL_

#include "Algebra.cuh"
#include "HostTexture.cuh"
#include "DeviceTexture.cuh"

class Material
{
public:

	vec3	diffuseColor;
	vec3	specularColor;
	vec3	emissiveColor;

	int		diffuseTexture;
	int		specularTexture;
	int		emissiveTexture;
	int		normalTexture;

	float	roughness;
	float	opacity;
	float	refractionIndex;
	float	reflectionAngle;
	float	reflectionAmount;

	__device__ __host__ Material();

	//__device__ __host__ vec3 getDiffuse(vec2 uv);
	//__device__ __host__ vec3 getSpecular(vec2 uv);

};



//////////////////////////////////////////////////////////////////////////////////////////////////////
// Material implementation
//////////////////////////////////////////////////////////////////////////////////////////////////////

inline Material::Material()
{
	diffuseColor = vec3(0.9f,0.9f,0.9f);
	specularColor = vec3(0.8f,0.8f,0.8f);
	emissiveColor = vec3(0.0f,0.0f,0.0f);
	roughness = 0.5f;
	opacity = 1.0f;
	refractionIndex = 1.0f;
	reflectionAngle = 0.0f;
	reflectionAmount = 0.0f;

	diffuseTexture = -1;
	specularTexture = -1;
	emissiveTexture = -1;
	normalTexture = -1;
}


/*inline vec3 Material::getDiffuse(vec2 uv)
{
	return diffuseColor;
}*/


/*inline vec3 Material::getSpecular(vec2 uv)
{
	return specularColor;
}*/

#endif // _MATERIAL_