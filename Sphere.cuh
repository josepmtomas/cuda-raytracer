#ifndef _SPHERE_PRIMITIVE_
#define _SPHERE_PRIMITIVE_

#include "Intersection.cuh"

class Sphere
{
protected:

	int material;
	
	vec3	center;
	float	radius;

public:

	int index;
	
	__host__ __device__ Sphere();
	__host__ __device__ Sphere(vec3 _center, float _radius);

	__host__ __device__ void setCenter(vec3 _center);
	__host__ __device__ void setRadius(float _radius);
	__host__ __device__ void setMaterial(int index);
	__host__ __device__ void setIndex(int _index);
	__host__ __device__ bool rayIntersection(vec3 origin, vec3 ray, Intersection &intersection);
	__host__ __device__ bool rayIntersectionOut(vec3 origin, vec3 ray, Intersection &intersection);
};

#endif	// _SPHERE_PRIMITIVE_


//////////////////////////////////////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////////////////////////////////////


Sphere::Sphere()
{
	index = 0;
	center = vec3(0,0,0);
	radius = 1.0f;
}


Sphere::Sphere(vec3 _center, float _radius)
{
	index = 0;
	center = _center;
	radius = _radius;
}


void Sphere::setCenter(vec3 _center)
{
	center = _center;
}


void Sphere::setRadius(float _radius)
{
	radius = _radius;
}


void Sphere::setMaterial(int index)
{
	material = index;
}


void Sphere::setIndex(int _index)
{
	index = _index;
}


bool Sphere::rayIntersection(vec3 origin, vec3 ray, Intersection &intersection)
{
	float alpha, beta, gamma;
	float root;
	float t1,t2;
	vec3 pcenter = origin - center;

	alpha = dot(ray,ray);
	beta = 2 * dot(pcenter,ray);
	gamma = dot(pcenter,pcenter) - (radius*radius);

	root = beta*beta - 4*alpha*gamma;

	if(root < 0.0f)
	{
		intersection.type = INTERSECTION_NONE;
		return false;
	}

	root = sqrtf(root);
	//t2 = (-beta+root)/(2*alpha);
	//t1 = (-beta-root)/(2*alpha);

	float q;
	if(beta < 0)
		q = (-beta -root) /2.0;
	else
		q = (-beta +root) /2.0;

	t1 = q / alpha;
	t2 = gamma / alpha;

	if(t1 < 0 || t2 < 0)
	{
		intersection.type = INTERSECTION_NONE;
		return false;
	}

	if(t1 > t2)
	{
		float temp = t1;
		t1 = t2;
		t2 = temp;
	}

	intersection.type			= INTERSECTION_IN;
	intersection.objectType		= OBJECT_TYPE_SPHERE;
	intersection.objectIndex	= index;
	intersection.distance		= t1;
	intersection.point			= origin + ray*t1;
	intersection.normal			= normalize(intersection.point - center);
	intersection.material		= material;

	if(intersection.normal == vec3(0,1,0))
	{
		intersection.tangent	= vec3(1,0,0);
	}
	else
	{
		intersection.tangent	= normalize(cross(vec3(0,1,0),intersection.normal));
	}

	return true;
}

bool Sphere::rayIntersectionOut(vec3 origin, vec3 ray, Intersection &intersection)
{
	float alpha, beta, gamma;
	float root;
	float t1, t2;
	vec3 pcenter = origin - center;

	alpha = dot(ray,ray);
	beta = 2 * dot(pcenter,ray);
	gamma = dot(pcenter,pcenter) - (radius*radius);

	root = beta*beta - 4*alpha*gamma;

	if(root <= 0.0f)
	{
		intersection.type = INTERSECTION_NONE;
		return false;
	}

	root = sqrtf(root);
	t2 = (-beta+(2*root))/(2);
	t1 = (-beta-root)/(2);

	if(t1 > t2)
	{
		float temp = t1;
		t1 = t2;
		t2 = temp;
	}

	if(t2 < 0)
	{
		intersection.type = INTERSECTION_NONE;
		return false;
	}

	intersection.type			= INTERSECTION_OUT;
	intersection.objectType		= OBJECT_TYPE_SPHERE;
	intersection.objectIndex	= index;
	intersection.distance		= t2;
	intersection.point			= origin + ray*t2;
	intersection.normal			= normalize(intersection.point - center);
	intersection.material		= material;

	if(intersection.normal == vec3(0,1,0))
	{
		intersection.tangent	= vec3(1,0,0);
	}
	else
	{
		intersection.tangent	= normalize(cross(vec3(0,1,0),intersection.normal));
	}

	return true;
}

//#endif	// _SPHERE_PRIMITIVE_


