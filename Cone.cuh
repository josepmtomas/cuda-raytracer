#ifndef _CONE_PRIMITIVE_
#define _CONE_PRIMITIVE_

#include "Intersection.cuh"

class Cone
{
protected:

	vec4 bottom;			// Equation that defines the bottom end-cap plane
	int  index;

	__device__ __host__ void buildPlane();

public:

	typedef enum ConeSurface { SIDE, BOTTOM };

	vec3	base;			// The base location of the cone.
	vec3	axis;			// The axis of symetry for the cone. (unitary)
	float	radius;			// The radius of the cone.
	float	height;			// The height of the cone.
	float	angle;			// The angle between axis and surface
	int		material;		// Material of the cone.

	__device__ __host__ Cone();
	__device__ __host__ Cone(vec3 _base, vec3 _axis, float _radius, float _height);

	__device__ __host__ void setIndex(int _index);
	__device__ __host__ void setMaterial(int _material);
	__device__ __host__ bool rayIntersection(vec3 origin, vec3 ray, Intersection &intersection);
	__device__ __host__ bool rayIntersectionOut(vec3 origin, vec3 ray, Intersection &intersection);
};

#endif	// _CONE_PRIMITIVE_

Cone::Cone()
{
	base	= vec3(0.0,0.0,0.0);
	axis	= vec3(0.0,1.0,0.0);
	radius	= 1.0;
	height	= 1.0;
	angle	= atanf(radius/height);

	buildPlane();
}

Cone::Cone(vec3 _base, vec3 _axis, float _radius, float _height)
{
	base	= _base;
	axis	= normalize(_axis);
	radius	= _radius;
	height	= _height;
	angle	= atanf(radius/height);

	buildPlane();
}

void Cone::setIndex(int _index)
{
	index = _index;
}

void Cone::setMaterial(int _material)
{
	material = _material;
}

void Cone::buildPlane()
{
	// Bottom plane

	vec3	point = negate(base);
	vec3	normal = negate(axis);
	float	d;

	d = normal.x*point.x + normal.y*point.y + normal.z*point.z;
	bottom = vec4(normal.x, normal.y, normal.z, d);
}


bool Cone::rayIntersection(vec3 origin, vec3 ray, Intersection &intersection)
{
	int		mQuantity;
	vec3	mPoint[2];

	vec3	coneAxis = negate(axis);
	vec3	coneVertex = base + axis*height;
	float	AdD = dot(coneAxis, ray);
	float	cosSqr = cosf(angle)*cosf(angle);
	vec3	E = origin - coneVertex;

	float	AdE = dot(coneAxis,E);
	float	DdE = dot(ray,E);
	float	EdE = dot(E,E);

	float	c2 = AdD*AdD - cosSqr;
	float	c1 = AdD*AdE - cosSqr*DdE;
	float	c0 = AdE*AdE - cosSqr*EdE;
	float	dotf;
	
	float	in,out;
	ConeSurface surfin;

	in = out = INF;

	// Solve the quadratic. Keep only those X for which Dot(A,X-V) >= 0
	if(fabs(c2) >= 0.0)
	{
		// c2 != 0
		float discr = c1*c1 - c0*c2;
		if(discr < 0.0)
		{
			// Q(t) = 0 has no real-valued roots. The line does not intersect
			// the double sided cone
			return false;
		}
		else if( discr > 0.0)
		{
			// Q(t) = 0 has two distinct real-valued roots. However, one or
			// both of them might intersect the portion of the double-sided
			// cone "behind" the vertex. We are interested only in those
			// intersections "in front" of the vertex.
			float root = sqrtf(discr);
			float invC2 = 1.0f/c2;
			mQuantity = 0;

			float t = (-c1 - root)*invC2;
			mPoint[mQuantity] = origin + t*ray;
			E = mPoint[mQuantity] - coneVertex;
			dotf = dot(E,coneAxis);
			if(dotf > 0.0)
			{
				mQuantity++;
				in = t;
				out = t;
			}

			t = (-c1 + root)*invC2;
			mPoint[mQuantity] = origin + t*ray;
			E = mPoint[mQuantity] - coneVertex;
			dotf = dot(E,coneAxis);
			if(dotf > 0.0)
			{
				mQuantity++;
				if(t < in)
				{
					in = t;
					surfin = ConeSurface::SIDE;
				}
				if(t > out)
				{
					out = t;
					//surfout = ConeSurface::SIDE;
				}
			}

			if(mQuantity == 2)
			{
				// The line intersects the single-sided cone in front of the
				// vertex twice.

				// Intersect the ray with the bottom cap plane
				float dc = bottom.x*ray.x + bottom.y*ray.y + bottom.z*ray.z;
				float dw = bottom.x*origin.x + bottom.y*origin.y + bottom.z*origin.z + bottom.w;

				// If parallel to bottom plane
				if(dc == 0)
				{
					if(dw >= 0.0f) return false;
				}
				else
				{
					t = -dw/dc;

					if(dc >= 0.0)
					{
						if(t > in && t < out)
						{
							out = t;
							//surfout = ConeSurface::BOTTOM;
						}
						if(t < in) return false;
					}
					else
					{
						if(t > in && t < out)
						{
							in = t;
							surfin = ConeSurface::BOTTOM;
						}
						if(t > out) return false;
					}
				}

				if(in < out && in > 0.0 && out > 0.0)
				{
					intersection.distance = in;
					intersection.point = origin + in*ray;

					if(surfin == ConeSurface::SIDE)
					{
						intersection.tangent = normalize(cross(normalize(coneVertex - intersection.point), axis));
						intersection.normal = normalize(cross(intersection.tangent, normalize(coneVertex - intersection.point)));
					}
					else // if (surfin == ConeSurface::BOTTOM)
					{
						intersection.normal = coneAxis;
						intersection.tangent = normalize(cross(coneAxis,vec3(0,1,0)));
					}
					/*intersection.distance = in;
					intersection.point = origin + in*ray;
					intersection.tangent = normalize(cross(normalize(coneVertex - intersection.point), axis));
					intersection.normal = normalize(cross(intersection.tangent, normalize(coneVertex - intersection.point)));*/
					intersection.material = material;
					return true;
				}
				else
				{
					return false;
				}
			}
			else if(mQuantity == 1)
			{
				// The line intersects the single-sided cone in front of the
				// vertex once. The other intersection is with the 
				// single-sided cone behind the vertex
				intersection.distance = in;
				intersection.point = mPoint[0];
				intersection.material = material;
				intersection.tangent = normalize(cross(normalize(coneVertex - intersection.point), axis));
				intersection.normal = normalize(cross(intersection.tangent, normalize(coneVertex - intersection.point)));
				return false;
			}
			else
			{
				// The line intersects the single-sided cone behind the vertex
				// twice.
				return false;
			}
		}
		else
		{
			// One repeated real root (line is tangent to the cone).
			mPoint[0] = origin + (c1/c2)*ray;
			E = mPoint[0] - coneVertex;
			if(dot(E,coneAxis) > 0.0)
			{
				intersection.distance = (c1/c2);
				intersection.point = mPoint[0];
				intersection.material = material;
				intersection.tangent = normalize(cross(normalize(coneVertex - intersection.point), axis));
				intersection.normal = normalize(cross(intersection.tangent, normalize(coneVertex - intersection.point)));
				return false;
			}
			else
			{
				return false;
			}
		}
	}
	else if(fabs(c1) >= 0.0)
	{
		// c2 = 0, c1 != 0 (D is a direction vector on the cone boundary)
		mPoint[0] = origin - ((0.5f)*c0/c1)*ray;
		E = mPoint[0] - coneVertex;
		if(dot(E,coneAxis) > 0.0)
		{
			intersection.distance = ((0.5f)*c0/c1);
			intersection.point = mPoint[0];
			intersection.material = material;
			intersection.tangent = normalize(cross(normalize(coneVertex - intersection.point), axis));
			intersection.normal = normalize(cross(intersection.tangent, normalize(coneVertex - intersection.point)));
			return false;;
		}
		else
		{
			return false;
		}
	}
	else if(fabs(c0) >= 0.0)
	{
		// c2 = c1 = 0 , c0 != 0
		return false;
	}
	else
	{
		// c2 = c1 = c0 = 0, cone contains ray V+t*D where V is cone vertex
		// and D is the line direction
		intersection.distance = distance(coneVertex,origin);
		intersection.point = coneVertex;
		intersection.material = material;
		intersection.tangent = normalize(cross(normalize(coneVertex - intersection.point), axis));
		intersection.normal = normalize(cross(intersection.tangent, normalize(coneVertex - intersection.point)));
		return false;
	}

}


bool Cone::rayIntersectionOut(vec3 origin, vec3 ray, Intersection &intersection)
{
	int		mQuantity;
	vec3	mPoint[2];

	vec3	coneAxis = negate(axis);
	vec3	coneVertex = base + axis*height;
	float	AdD = dot(coneAxis, ray);
	float	cosSqr = cosf(angle)*cosf(angle);
	vec3	E = origin - coneVertex;

	float	AdE = dot(coneAxis,E);
	float	DdE = dot(ray,E);
	float	EdE = dot(E,E);

	float	c2 = AdD*AdD - cosSqr;
	float	c1 = AdD*AdE - cosSqr*DdE;
	float	c0 = AdE*AdE - cosSqr*EdE;
	float	dotf;// mint;
	
	float	in,out;
	ConeSurface surfout;

	in = out = INF;

	// Solve the quadratic. Keep only those X for which Dot(A,X-V) >= 0
	if(fabs(c2) >= 0.0)
	{
		// c2 != 0
		float discr = c1*c1 - c0*c2;
		if(discr < 0.0)
		{
			// Q(t) = 0 has no real-valued roots. The line does not intersect
			// the double sided cone
			return false;
		}
		else if( discr > 0.0)
		{
			// Q(t) = 0 has two distinct real-valued roots. However, one or
			// both of them might intersect the portion of the double-sided
			// cone "behind" the vertex. We are interested only in those
			// intersections "in front" of the vertex.
			float root = sqrtf(discr);
			float invC2 = 1.0f/c2;
			mQuantity = 0;

			float t = (-c1 - root)*invC2;
			mPoint[mQuantity] = origin + t*ray;
			E = mPoint[mQuantity] - coneVertex;
			dotf = dot(E,coneAxis);
			if(dotf > 0.0)
			{
				mQuantity++;
				in = t;
				out = t;
			}

			t = (-c1 + root)*invC2;
			mPoint[mQuantity] = origin + t*ray;
			E = mPoint[mQuantity] - coneVertex;
			dotf = dot(E,coneAxis);
			if(dotf > 0.0)
			{
				mQuantity++;
				if(t < in)
				{
					in = t;
					//surfin = ConeSurface::SIDE;
				}
				if(t > out)
				{
					out = t;
					surfout = ConeSurface::SIDE;
				}
			}

			if(mQuantity == 2)
			{
				// The line intersects the single-sided cone in front of the
				// vertex twice.

				// Intersect the ray with the bottom cap plane
				float dc = bottom.x*ray.x + bottom.y*ray.y + bottom.z*ray.z;
				float dw = bottom.x*origin.x + bottom.y*origin.y + bottom.z*origin.z + bottom.w;

				// If parallel to bottom plane
				if(dc == 0)
				{
					if(dw >= 0.0f) return false;
				}
				else
				{
					t = -dw/dc;

					if(dc >= 0.0)
					{
						if(t > in && t < out)
						{
							out = t;
							surfout = ConeSurface::BOTTOM;
						}
						if(t < in) return false;
					}
					else
					{
						if(t > in && t < out)
						{
							in = t;
							//surfin = ConeSurface::BOTTOM;
						}
						if(t > out) return false;
					}
				}

				if(in < out)
				{
					intersection.distance = out;
					intersection.point = origin + out*ray;

					if(surfout == ConeSurface::BOTTOM)
					{
						intersection.tangent = normalize(cross(normalize(coneVertex - intersection.point), axis));
						intersection.normal = normalize(cross(intersection.tangent, normalize(coneVertex - intersection.point)));
					}
					else // if (surfin == ConeSurface::BOTTOM)
					{
						intersection.normal = coneAxis;
						intersection.tangent = normalize(cross(coneAxis,vec3(0,1,0)));
					}
					/*intersection.distance = in;
					intersection.point = origin + in*ray;
					intersection.tangent = normalize(cross(normalize(coneVertex - intersection.point), axis));
					intersection.normal = normalize(cross(intersection.tangent, normalize(coneVertex - intersection.point)));*/
					intersection.objectType = OBJECT_TYPE_CONE;
					intersection.objectIndex = index;
					intersection.material = material;
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				// The line intersects the single-sided cone behind the vertex
				// twice.
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}

}