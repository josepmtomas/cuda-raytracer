#ifndef _BOX_PRIMITIVE_
#define _BOX_PRIMITIVE_

#include "Intersection.cuh"

class Box
{
protected:

	int material;
	int index;

	vec3	vertices[8];
	vec3	fnormals[6];
	vec3	tangents[6];
	uint4	faces[6];

public:

	__device__ __host__ Box();
	__device__ __host__ Box(vec3 offset, vec3 scale);

	__device__ __host__ void setIndex(int _index);
	__device__ __host__ void setMaterial(int index);

	__device__ __host__ void translation(vec3 t);
	__device__ __host__ void rotation(vec3 axis, float rads);
	__device__ __host__ void scale(vec3 s);

	__device__ __host__ void recalculateNormalsAndTangents();

	__device__ __host__ bool rayIntersection(vec3 origin, vec3 ray, Intersection& intersection);
	__device__ __host__ bool rayIntersectionOut(vec3 origin, vec3 ray, Intersection& intersection);

};

#endif // _BOX_PRIMITIVE_



//////////////////////////////////////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////////////////////////////////////


inline Box::Box()
{
	index = 0;

	vertices[0] = vec3(-0.5, -0.5,  0.5);
	vertices[1] = vec3( 0.5, -0.5,  0.5);
	vertices[2] = vec3( 0.5, -0.5, -0.5);
	vertices[3] = vec3(-0.5, -0.5, -0.5);
	vertices[4] = vec3(-0.5,  0.5,  0.5);
	vertices[5] = vec3( 0.5,  0.5,  0.5);
	vertices[6] = vec3( 0.5,  0.5, -0.5);
	vertices[7] = vec3(-0.5,  0.5, -0.5);

	faces[0] = make_uint4(4,0,1,5);
	faces[1] = make_uint4(5,1,2,6);
	faces[2] = make_uint4(6,2,3,7);
	faces[3] = make_uint4(7,3,0,4);
	faces[4] = make_uint4(7,4,5,6);
	faces[5] = make_uint4(0,3,2,1);

	fnormals[0] = normalize(cross(vertices[faces[0].y] - vertices[faces[0].x], vertices[faces[0].z] - vertices[faces[0].y]));
	fnormals[1] = normalize(cross(vertices[faces[1].y] - vertices[faces[1].x], vertices[faces[1].z] - vertices[faces[1].y]));
	fnormals[2] = normalize(cross(vertices[faces[2].y] - vertices[faces[2].x], vertices[faces[2].z] - vertices[faces[2].y]));
	fnormals[3] = normalize(cross(vertices[faces[3].y] - vertices[faces[3].x], vertices[faces[3].z] - vertices[faces[3].y]));
	fnormals[4] = normalize(cross(vertices[faces[4].y] - vertices[faces[4].x], vertices[faces[4].z] - vertices[faces[4].y]));
	fnormals[5] = normalize(cross(vertices[faces[5].y] - vertices[faces[5].x], vertices[faces[5].z] - vertices[faces[5].y]));

	tangents[0] = vec3(0,1,0);
	tangents[1] = vec3(0,1,0);
	tangents[2] = vec3(0,1,0);
	tangents[3] = vec3(0,1,0);
	tangents[4] = vec3(1,0,0);
	tangents[5] = vec3(1,0,0);
}


inline Box::Box(vec3 offset, vec3 scale)
{
	index = 0;

	vertices[0] = vec3(-0.5, -0.5,  0.5)*scale + offset;
	vertices[1] = vec3( 0.5, -0.5,  0.5)*scale + offset;
	vertices[2] = vec3( 0.5, -0.5, -0.5)*scale + offset;
	vertices[3] = vec3(-0.5, -0.5, -0.5)*scale + offset;
	vertices[4] = vec3(-0.5,  0.5,  0.5)*scale + offset;
	vertices[5] = vec3( 0.5,  0.5,  0.5)*scale + offset;
	vertices[6] = vec3( 0.5,  0.5, -0.5)*scale + offset;
	vertices[7] = vec3(-0.5,  0.5, -0.5)*scale + offset;

	faces[0] = make_uint4(4,0,1,5);
	faces[1] = make_uint4(5,1,2,6);
	faces[2] = make_uint4(6,2,3,7);
	faces[3] = make_uint4(7,3,0,4);
	faces[4] = make_uint4(7,4,5,6);
	faces[5] = make_uint4(0,3,2,1);

	fnormals[0] = normalize(cross(vertices[faces[0].y] - vertices[faces[0].x], vertices[faces[0].z] - vertices[faces[0].y]));
	fnormals[1] = normalize(cross(vertices[faces[1].y] - vertices[faces[1].x], vertices[faces[1].z] - vertices[faces[1].y]));
	fnormals[2] = normalize(cross(vertices[faces[2].y] - vertices[faces[2].x], vertices[faces[2].z] - vertices[faces[2].y]));
	fnormals[3] = normalize(cross(vertices[faces[3].y] - vertices[faces[3].x], vertices[faces[3].z] - vertices[faces[3].y]));
	fnormals[4] = normalize(cross(vertices[faces[4].y] - vertices[faces[4].x], vertices[faces[4].z] - vertices[faces[4].y]));
	fnormals[5] = normalize(cross(vertices[faces[5].y] - vertices[faces[5].x], vertices[faces[5].z] - vertices[faces[5].y]));

	tangents[0] = vec3(0.0,1,0);
	tangents[1] = vec3(0.0,1,0);
	tangents[2] = vec3(0.0,1,0);
	tangents[3] = vec3(0.0,1,0);
	tangents[4] = vec3(1.0,0,0);
	tangents[5] = vec3(1.0,0,0);
}


inline void Box::setIndex(int _index)
{
	index = index;
}


inline void Box::setMaterial(int index)
{
	material = index;
}


void Box::translation(vec3 t)
{
	for(int i=0; i<8; i++)
	{
		vertices[i] += t;
	}
}


void Box::rotation(vec3 axis, float rads)
{
	for(int i=0; i<8; i++)
	{
		vertices[i] = rotate(vertices[i], axis, rads);
	}

	recalculateNormalsAndTangents();
}


void Box::scale(vec3 s)
{
	for(int i=0; i<8; i++)
	{
		vertices[i] = vertices[i] * s;
	}

	recalculateNormalsAndTangents();
}


void Box::recalculateNormalsAndTangents()
{
	for(int i=0; i<6; i++)
	{
		fnormals[i] = normalize(cross(vertices[faces[i].y] - vertices[faces[i].x], vertices[faces[i].z] - vertices[faces[i].y]));
	}

	for(int i=0; i<6; i++)
	{
		if(fnormals[i] == vec3(0.0,1.0,0.0))
		{
			tangents[i] = vec3(1.0,0.0,0.0);
		}
		else if(fnormals[i] == vec3(0.0,-1.0,0.0))
		{
			tangents[i] = vec3(1.0,0.0,0.0);
		}
		else
		{
			tangents[i] = normalize(cross(fnormals[i], vec3(0,1,0)));
		}
	}

	/*cprintf(CONSOLE_COLOR_YELLOW, "Resulting Normals and Tangents.\n");
	for(int i=0; i<6; i++)
	{
		cprintf(CONSOLE_COLOR_YELLOW, "\tNormal(%.2f, %.2f, %.2f) - Tangent(%.2f, %.2f, %.2f)\n",
		fnormals[i].x, fnormals[i].y, fnormals[i].z, tangents[i].x, tangents[i].y, tangents[i].z);
	}*/
}


inline bool Box::rayIntersection(vec3 origin, vec3 ray, Intersection& intersection)
{
	float te = 0.0f;
	float ts = INF;
	float t = 0.0f;
	float RdotN;

	vec3 normal;
	vec3 tangent;
	vec3 N;

	for(int i=0; i<6; i++)
	{
		//N = normalize(cross(vertices[faces[i].y] - vertices[faces[i].x], vertices[faces[i].z] - vertices[faces[i].y]));
		N = fnormals[i];
		RdotN = dot(N,ray);

		t = dot(N, vertices[faces[i].x] - origin) / RdotN;

		if(RdotN == 0)
		{
			if(t < 0.0f)
			{
				return false;
			}
		}
		else
		{
			if(RdotN < 0)	// IN
			{
				te = max(te,t);

				if(te == t)
				{
					normal = N;
					tangent = tangents[i];
				}
			}
			if(RdotN > 0)
			{
				ts = min(ts,t);
			}
		}
	}
	if(te <= ts)
	{
		intersection.distance		= te;
		intersection.type			= INTERSECTION_IN;
		intersection.point			= origin + ray*te;
		intersection.normal			= normal;
		intersection.tangent		= tangent;
		intersection.objectType		= OBJECT_TYPE_BOX;
		intersection.objectIndex	= index;
		intersection.material		= material;
		return true;
	}
	else
	{
		return false;
	}
}


inline bool Box::rayIntersectionOut(vec3 origin, vec3 ray, Intersection& intersection)
{
	float te = 0.0f;
	float ts = INF;
	float t = 0.0f;
	float RdotN;

	vec3 normal;
	vec3 tangent;
	vec3 N;

	for(int i=0; i<6; i++)
	{
		//N = normalize(cross(vertices[faces[i].y] - vertices[faces[i].x], vertices[faces[i].z] - vertices[faces[i].y]));
		N = fnormals[i];
		RdotN = dot(N,ray);

		t = dot(N, vertices[faces[i].x] - origin) / RdotN;

		if(RdotN == 0)
		{
			if(t < 0.0f)
			{
				return false;
			}
		}
		else
		{
			if(RdotN < 0)	// IN
			{
				te = max(te,t);

				if(te == t)
				{
					//normal = N;
					//tangent = tangents[i];
				}
			}
			if(RdotN > 0)
			{
				ts = min(ts,t);

				if(ts == t)
				{
					normal = N;
					tangent = tangents[i];
				}
			}
		}
	}
	if(te <= ts && te > 0)
	{
		intersection.distance		= ts;
		intersection.type			= INTERSECTION_OUT;
		intersection.point			= origin + ray*ts;
		intersection.normal			= normal;
		intersection.tangent		= tangent;
		intersection.objectType		= OBJECT_TYPE_BOX;
		intersection.objectIndex	= index;
		intersection.material		= material;
		return true;
	}
	else
	{
		return false;
	}
}