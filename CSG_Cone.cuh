#ifndef _CSG_CONE_
#define _CSG_CONE_

#include "Intersection.cuh"

class CSG_Cone
{
protected:

	vec4 bottom;		// Equation that defines the bottom end-cap plane

	__device__ __host__ void buildPlane();

public:

	typedef enum ConeSurface { SIDE, BOTTOM };

	vec3	base;		// The base location of the cone.
	vec3	axis;		// The axis of symetry for the cone. (unitary)
	float	radius;		// The bottom radius of the cone.
	float	height;		// The height of the cone;
	float	angle;		// The angle between axis and surface.
	int		material;	// Naterial of the cone.

	__device__ __host__ CSG_Cone();
	__device__ __host__ CSG_Cone(vec3 _base, vec3 _axis, float _radius, float _height);

	__device__ __host__ void setMaterial(int _material);
	__device__ __host__ bool rayIntersection(vec3 origin, vec3 ray, Intersection &inIntersection, Intersection &outIntersection);
};

#endif // _CSG_CONE_

CSG_Cone::CSG_Cone()
{
	base	= vec3(0.0,0.0,0.0);
	axis	= vec3(0.0,1.0,0.0);
	radius	= 1.0;
	height	= 1.0;
	angle	= atanf(radius/height);

	buildPlane();
}

CSG_Cone::CSG_Cone(vec3 _base, vec3 _axis, float _radius, float _height)
{
	base	= _base;
	axis	= _axis;
	radius	= _radius;
	height	= _height;
	angle	= atanf(radius/height);

	buildPlane();
}

void CSG_Cone::setMaterial(int _material)
{
	material = _material;
}

void CSG_Cone::buildPlane()
{
	// Bottom plane

	vec3	point = base;
	vec3	normal = negate(axis);
	float	d;

	d = normal.x*point.x + normal.y*point.y + normal.z*point.z;
	bottom = vec4(normal.x, normal.y, normal.z, d);
}


bool CSG_Cone::rayIntersection(vec3 origin, vec3 ray, Intersection &inIntersection, Intersection &outIntersection)
{
	int		mQuantity;
	vec3	mPoint[2];

	vec3	coneAxis = negate(axis);
	vec3	coneVertex = base + axis*height;
	float	AdD = dot(coneAxis, ray);
	float	cosSqr = cosf(angle)*cosf(angle);
	vec3	E = origin - coneVertex;

	float	AdE = dot(coneAxis,E);
	float	DdE = dot(ray,E);
	float	EdE = dot(E,E);

	float	c2 = AdD*AdD - cosSqr;
	float	c1 = AdD*AdE - cosSqr*DdE;
	float	c0 = AdE*AdE - cosSqr*EdE;
	float	dotf, mint;
	
	float	in,out;
	ConeSurface surfin, surfout;

	in = out = INF;

	// Solve the quadratic. Keep only those X for which Dot(A,X-V) >= 0
	if(fabs(c2) >= 0.0)
	{
		// c2 != 0
		float discr = c1*c1 - c0*c2;
		if(discr < 0.0)
		{
			// Q(t) = 0 has no real-valued roots. The line does not intersect
			// the double sided cone
			return false;
		}
		else if( discr > 0.0)
		{
			// Q(t) = 0 has two distinct real-valued roots. However, one or
			// both of them might intersect the portion of the double-sided
			// cone "behind" the vertex. We are interested only in those
			// intersections "in front" of the vertex.
			float root = sqrtf(discr);
			float invC2 = 1.0/c2;
			mQuantity = 0;

			float t = (-c1 - root)*invC2;
			mPoint[mQuantity] = origin + t*ray;
			E = mPoint[mQuantity] - coneVertex;
			dotf = dot(E,coneAxis);
			if(dotf > 0.0)
			{
				mQuantity++;
				in = t;
				out = t;
			}

			t = (-c1 + root)*invC2;
			mPoint[mQuantity] = origin + t*ray;
			E = mPoint[mQuantity] - coneVertex;
			dotf = dot(E,coneAxis);
			if(dotf > 0.0)
			{
				mQuantity++;
				if(t < in)
				{
					in = t;
					surfin = ConeSurface::SIDE;
				}
				if(t > out)
				{
					out = t;
					surfout = ConeSurface::SIDE;
				}
			}

			if(mQuantity == 2)
			{
				// The line intersects the single-sided cone in front of the
				// vertex twice.

				// Intersect the ray with the bottom cap plane
				float dc = bottom.x*ray.x + bottom.y*ray.y + bottom.z*ray.z;
				float dw = bottom.x*origin.x + bottom.y*origin.y + bottom.z*origin.z + bottom.w;

				// If parallel to bottom plane
				if(dc == 0)
				{
					if(dw >= 0.0f) return false;
				}
				else
				{
					t = -dw/dc;

					if(dc >= 0.0)
					{
						if(t > in && t < out)
						{
							out = t;
							surfout = ConeSurface::BOTTOM;
						}
						if(t < in) return false;
					}
					else
					{
						if(t > in && t < out)
						{
							in = t;
							surfin = ConeSurface::BOTTOM;
						}
						if(t > out) return false;
					}
				}

				inIntersection.distance = in;
				inIntersection.point = origin + in*ray;
				outIntersection.distance = out;
				outIntersection.point = origin + out*ray;

				if(surfin == ConeSurface::SIDE)
				{
					inIntersection.tangent = normalize(cross(normalize(coneVertex - inIntersection.point), axis));
					inIntersection.normal = normalize(cross(inIntersection.tangent, normalize(coneVertex - inIntersection.point)));
				}
				else // if (surfin == ConeSurface::BOTTOM)
				{
					inIntersection.normal = coneAxis;
					inIntersection.tangent = normalize(cross(coneAxis,vec3(0,1,0)));
				}

				if(surfout == ConeSurface::SIDE)
				{
					outIntersection.tangent = normalize(cross(normalize(coneVertex - outIntersection.point), axis));
					outIntersection.normal = normalize(cross(outIntersection.tangent, normalize(coneVertex - outIntersection.point)));
				}
				else // if (surfout == ConeSurface::BOTTOM)
				{
					outIntersection.normal = coneAxis;
					outIntersection.tangent = normalize(cross(coneAxis,vec3(0,1,0)));
				}

				inIntersection.material = material;
				outIntersection.material = material;
				return true;
			}
			else if(mQuantity == 1)
			{
				// The line intersects the single-sided cone in front of the
				// vertex once. The other intersection is with the 
				// single-sided cone behind the vertex
				inIntersection.distance = in;
				inIntersection.point = mPoint[0];
				inIntersection.material = material;
				return true;
			}
			else
			{
				// The line intersects the single-sided cone behind the vertex
				// twice.
				return false;
			}
		}
		else
		{
			// One repeated real root (line is tangent to the cone).
			mPoint[0] = origin + (c1/c2)*ray;
			E = mPoint[0] - coneVertex;
			if(dot(E,coneAxis) > 0.0)
			{
				inIntersection.distance = (c1/c2);
				inIntersection.point = mPoint[0];
				inIntersection.material = material;
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	else if(fabs(c1) >= 0.0)
	{
		// c2 = 0, c1 != 0 (D is a direction vector on the cone boundary)
		mPoint[0] = origin - ((0.5)*c0/c1)*ray;
		E = mPoint[0] - coneVertex;
		if(dot(E,coneAxis) > 0.0)
		{
			inIntersection.distance = ((0.5)*c0/c1);
			inIntersection.point = mPoint[0];
			inIntersection.material = material;
			return true;
		}
		else
		{
			return false;
		}
	}
	else if(fabs(c0) >= 0.0)
	{
		// c2 = c1 = 0 , c0 != 0
		return false;
	}
	else
	{
		// c2 = c1 = c0 = 0, cone contains ray V+t*D where V is cone vertex
		// and D is the line direction
		inIntersection.distance = distance(coneVertex,origin);
		inIntersection.point = coneVertex;
		inIntersection.material = material;
		return true;
	}

}