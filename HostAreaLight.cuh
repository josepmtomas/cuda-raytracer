#ifndef _HOST_AREA_LIGHT_
#define _HOST_AREA_LIGHT_

#include <vector>
#include "Algebra.cuh"
#include "Plane.cuh"

class HostAreaLight
{
public:

	std::vector<vec3> points;
	std::vector<vec3> normals;
	std::vector<vec3> tangents;
	std::vector<vec3> binormals;
	Plane	plane;

	vec3 color;
	vec2 spacing;
	int  type;
	

	HostAreaLight();
	HostAreaLight(vec3 _color);

	__host__ void buildPlaneAreaLight(vec3 point, vec3 direction, float rollAngle, float planeWidth, float planeHeight, u_int2 division);
};

//

HostAreaLight::HostAreaLight()
{
	type = AREA_LIGHT_NONE;
	color = vec3(1.0,1.0,1.0);
}


HostAreaLight::HostAreaLight(vec3 _color)
{
	type = AREA_LIGHT_NONE;
	color = _color;
}


void HostAreaLight::buildPlaneAreaLight(vec3 point, vec3 direction, float rollAngle, float planeWidth, float planeHeight, u_int2 division)
{
	vec3 min = vec3(-0.5*planeWidth,0.0,-0.5*planeHeight);
	vec3 normalPoint = vec3(0,0.1,0);
	vec3 lightDirection = normalize(direction);
	float incx = planeWidth / (division.x - 1);
	float incz = planeHeight / (division.y - 1);

	spacing = vec2(incx,incz);

	// Divide the base plane and obtain the points
	for(int i=0; i<division.x; i++)
	{
		for(int j=0; j<division.y; j++)
		{
			points.push_back(vec3(min.x+(float)i*incx, 0.0, min.z+(float)j*incz));
			//printf("[%d,%d] : [%.2f, %.2f]\n", i,j, min.x+(float)i*incx, min.z+(float)j*incz);
		}
	}

	// Generate the tangents and binormals
	for(int i=0; i<points.size(); i++)
	{
		tangents.push_back(vec3(1.0,0.0,0.0));
		binormals.push_back(vec3(0.0,0.0,1.0));
	}

	// Rotate the points about the default axis (Roll Angle)
	for(int i=0; i<points.size(); i++)
	{
		points[i] = rotate(points[i], vec3(0,-1,0), rollAngle);
		tangents[i] = rotate(tangents[i], vec3(0,-1,0), rollAngle);
		binormals[i] = rotate(binormals[i], vec3(0,-1,0), rollAngle);
	}

	// Calculate the rotation axis and angle to match the given direction
	vec3 crossp = cross(lightDirection,vec3(0,-1,0));
	vec3 rotationAxis;
	float rotationAngle;

	if(direction != vec3(0,-1,0))
	{
		if(l1Norm(crossp) == 0.0)
		{
			rotationAxis = vec3(1,0,0);
			rotationAngle = PI;
		}
		else
		{
			rotationAxis = normalize(crossp);
			rotationAngle = acosf(dot(vec3(0,-1,0),lightDirection));
		}

		// Rotate the points
		for(int i=0; i<points.size(); i++)
		{
			points[i] = rotate(points[i], rotationAxis, rotationAngle);
			tangents[i] = rotate(tangents[i], rotationAxis, rotationAngle);
			binormals[i] = rotate(binormals[i], rotationAxis, rotationAngle);
		}
		normalPoint = rotate(normalPoint, rotationAxis, rotationAngle);
	}

	// Translate the points
	printf("\nFinalPoints\n");
	for(int i=0; i<points.size(); i++)
	{
		points[i] = points[i] + point;
		//printf("[%d] : [%.2f, %.2f, %.2f]\n", i, points[i].x, points[i].y, points[i].z);
	}
	normalPoint = normalPoint + point;

	// Generate normals
	/*for(int i=0; i<points.size(); i++)
	{
		normals.push_back(normalize(points[i] - normalPoint));
	}*/

	type = AREA_LIGHT_PLANE;

	// Build the plane
	//plane = Plane(points[0],points[division.y * division.y], points[division.y-1], points[points.size()-1], direction);
	plane = Plane(points[0],points[division.y * (division.x-1)], points[points.size()-1], points[division.y-1], direction);
}

#endif // _HOST_AREA_LIGHT_