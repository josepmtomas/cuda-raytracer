#ifndef _HOST_MESH_
#define _HOST_MESH_

#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "Algebra.cuh"
#include "HostBoundingBox.cuh"

class HostMesh
{
public:

	std::vector<int>	elements;		// Array of vertex indexes
	std::vector<vec3>	positions;		// Array of vertex positions
	std::vector<vec2>	uvs;			// Array of vertex texture coordinates
	std::vector<vec3>	vnormals;		// Array of vertex normals
	std::vector<vec3>	fnormals;		// Array of face normals
	std::vector<vec4>	tangents;		// Array of vertex tangents

	int material;

	HostBoundingBox		bounds;

	__host__ HostMesh();

	__host__ bool loadOBJ(const char *filename);
	__host__ void buildBounds(u_int3 subdivision);
	__host__ void setMaterial(int _material);
	__host__ void clear();

	// Transformations
	__host__ void translation(vec3 factor);
	__host__ void scale(vec3 factor);
	__host__ void rotation(vec3 axis, float rads);
};

//

HostMesh::HostMesh()
{

}


bool HostMesh::loadOBJ(const char *filename)
{
	/*std::ofstream logFile("objLog.txt");

	if(!logFile.is_open())
	{
		fprintf(stderr,"Error opening file: %s\n", logFile);
	}*/

	/////////////////////////

	int tri;
	char format[4];
	bool omitLine;
	std::string line;
	std::string token;
	std::stringstream lineStream;
	std::stringstream tokenStream;

	vec3	tempPosition;
	vec3	tempNormal;
	vec2	tempUV;
	face3	tempFace;

	std::vector<vec3>	tempPositions;
	std::vector<vec3>	tempVNormals;
	std::vector<vec2>	tempUVs;
	std::vector<face3>	tempFaces;


	/////////////////////////

	std::ifstream inFile(filename);

	if(!inFile.is_open())
	{
		fprintf(stderr,"Error opening file: %s\n", filename);
	}

	/////////////////////////

	format[0] = toupper(filename[strlen(filename)-3]);
	format[1] = toupper(filename[strlen(filename)-2]);
	format[2] = toupper(filename[strlen(filename)-1]);
	format[3] = '\0';

	if(strcmp(format, "OBJ") > 0)
	{
		fprintf(stderr,"[ERROR] El formato .%s no esta soportado.\n", format);
		return false;
	}

	/////////////////////////
	
	while(!inFile.eof())
	{
		omitLine = false;
		std::getline(inFile,line);
		lineStream = std::stringstream(line);

		while(lineStream >> token && !omitLine)
		{
			if(token.compare("#") == 0)			// Comment
			{
				omitLine = true;
			}
			else if(token.compare("v") == 0)	// Vertex positiom
			{
				lineStream >> token;
				tempPosition.x = atof(token.c_str());

				lineStream >> token;
				tempPosition.y = atof(token.c_str());

				lineStream >> token;
				tempPosition.z = atof(token.c_str());

				tempPositions.push_back(tempPosition);
			}
			else if(token.compare("vn") == 0)
			{
				lineStream >> token;
				tempNormal.x = atof(token.c_str());

				lineStream >> token;
				tempNormal.y = atof(token.c_str());

				lineStream >> token;
				tempNormal.z = atof(token.c_str());

				tempVNormals.push_back(tempNormal);
			}
			else if(token.compare("vt") == 0)
			{
				lineStream >> token;
				tempUV.x = atof(token.c_str());

				lineStream >> token;
				tempUV.y = atof(token.c_str());

				lineStream >> token;

				tempUVs.push_back(tempUV);
			}
			else if(token.compare("g") == 0)
			{
				//logFile << "Group" << std::endl;
				omitLine = true;
			}
			else if(token.compare("f") == 0)
			{
				//logFile << "Face" << std::endl;
				tri = 0;
				while(lineStream >> token && tri < 3)
				{
					tokenStream = std::stringstream(token);

					std::getline(tokenStream,token,'/');
					tempFace.position[tri] = atoi(token.c_str());
					tempFace.position[tri] -= 1;

					std::getline(tokenStream,token,'/');
					tempFace.uv[tri] = atoi(token.c_str());
					tempFace.uv[tri] -= 1;

					std::getline(tokenStream,token,'/');
					tempFace.vnormal[tri] = atoi(token.c_str());
					tempFace.vnormal[tri] -= 1;

					tri++;
				}
				tempFaces.push_back(tempFace);
			}
			else if(token.compare("s") == 0)
			{
				omitLine = true;
			}
			else
			{
				//logFile << "Others" << std::endl;
				fprintf(stderr,"[ERROR] Unexpected token, mesh not as expected\n");
				return false;
			}
		}
		// New line
	}

	inFile.close();

	//////////////////////////////////////////////////////////////////////////
	// 2 - Build the buffers with unique vertexes
	//////////////////////////////////////////////////////////////////////////

	vec3 pos1, pos2, pos3;
	vec2 tc1, tc2, tc3;
	vec3 vn1, vn2, vn3;
	int k, index;
	bool next;

	/////////////////////////

	for(int i=0; i<tempFaces.size(); i++)
	{
		tempFace = tempFaces[i];

		// Vertex positions of the current face
		pos1 = tempPositions[tempFace.position[0]];
		pos2 = tempPositions[tempFace.position[1]];
		pos3 = tempPositions[tempFace.position[2]];

		// Texture coordinates of the current face
		tc1 = tempUVs[tempFace.uv[0]];
		tc2 = tempUVs[tempFace.uv[1]];
		tc3 = tempUVs[tempFace.uv[2]];

		// Vertex normals of the current face
		vn1 = tempVNormals[tempFace.vnormal[0]];
		vn2 = tempVNormals[tempFace.vnormal[1]];
		vn3 = tempVNormals[tempFace.vnormal[2]];

		if(positions.empty())	// First iteration
		{
			positions.push_back(pos1);
			positions.push_back(pos2);
			positions.push_back(pos3);
			uvs.push_back(tc1);
			uvs.push_back(tc2);
			uvs.push_back(tc3);
			vnormals.push_back(vn1);
			vnormals.push_back(vn2);
			vnormals.push_back(vn3);
			elements.push_back(0);
			elements.push_back(1);
			elements.push_back(2);
		}
		else	// Search if exists a vertex with the same position, uv and normal
		{
			// Check if the first vertex exists in the vector
			k = 0;
			index = -1;
			next = true;

			while(k < positions.size() && next == true)
			{
				if(pos1 == positions[k] && tc1 == uvs[k] && vn1 == vnormals[k])
				{
					index = k;
					next = false;
				}
				k++;
			}

			if(index == -1)
			{
				positions.push_back(pos1);
				uvs.push_back(tc1);
				vnormals.push_back(vn1);
				elements.push_back(positions.size()-1);
			}
			else
			{
				elements.push_back(index);
			}

			// Check if the second vertex exists in the vector
			k = 0;
			index = -1;
			next = true;

			while(k < positions.size() && next == true)
			{
				if(pos2 == positions[k] && tc2 == uvs[k] && vn2 == vnormals[k])
				{
					index = k;
					next = false;
				}
				k++;
			}

			if(index == -1)
			{
				positions.push_back(pos2);
				uvs.push_back(tc2);
				vnormals.push_back(vn2);
				elements.push_back(positions.size()-1);
			}
			else
			{
				elements.push_back(index);
			}

			// Check if the third vertex exists in the vector
			k = 0;
			index = -1;
			next = true;

			while(k < positions.size() && next == true)
			{
				if(pos3 == positions[k] && tc3 == uvs[k] && vn3 == vnormals[k])
				{
					index = k;
					next = false;
				}
				k++;
			}

			if(index == -1)
			{
				positions.push_back(pos3);
				uvs.push_back(tc3);
				vnormals.push_back(vn3);
				elements.push_back(positions.size()-1);
			}
			else
			{
				elements.push_back(index);
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////
	// 3 - Calculate face normals
	//////////////////////////////////////////////////////////////////////////

	for(unsigned int i=0; i<elements.size(); i+=3)
	{
		pos1 = positions[elements[i]];
		pos2 = positions[elements[i+1]];
		pos3 = positions[elements[i+2]];

		fnormals.push_back(normalize(cross((pos2-pos1),(pos3-pos2))));
	}

	//////////////////////////////////////////////////////////////////////////
	// 4 - Generate Tangents
	//////////////////////////////////////////////////////////////////////////

	std::vector<vec3> tan1Accum;
	std::vector<vec3> tan2Accum;

	int vsize = positions.size();
	int fsize = elements.size();

	vec3  p1, p2, tan1, tan2;
	float s1, s2, t1, t2, r;

	for(int i=0; i<vsize; i++)
	{
		tan1Accum.push_back(vec3(0.0));
		tan2Accum.push_back(vec3(0.0));
		tangents.push_back(vec4(0.0));
	}

	// Compute the tangent vector
	for(int i=0; i<fsize; i+=3)
	{
		pos1 = positions[elements[i]];
		pos2 = positions[elements[i+1]];
		pos3 = positions[elements[i+2]];

		tc1 = uvs[elements[i]];
		tc2 = uvs[elements[i+1]];
		tc3 = uvs[elements[i+2]];

		p1 = pos2 - pos1;
		p2 = pos3 - pos1;

		s1 = tc2.x - tc1.x;
		s2 = tc3.x - tc1.x;
		t1 = tc2.y - tc1.y;
		t2 = tc3.y - tc1.y;
		r = 1.0 / (s1 * t2 - s2 * t1);

		tan1 = vec3((t2*p1.x - t1*p2.x) * r,
					(t2*p1.y - t1*p2.y) * r,
					(t2*p1.z - t1*p2.z) * r);
		tan2 = vec3((s1*p2.x - s2*p1.x) * r,
					(s1*p2.y - s2*p1.y) * r,
					(s1*p2.z - s2*p1.z) * r);

		tan1Accum[elements[i]] += tan1;
		tan1Accum[elements[i+1]] += tan1;
		tan1Accum[elements[i+2]] += tan1;
		tan2Accum[elements[i]] += tan2;
		tan2Accum[elements[i+1]] += tan2;
		tan2Accum[elements[i+2]] += tan2;
	}

	for(int i=0; i<vsize; i++)
	{
		tempNormal = vnormals[i];
		tan1 = tan1Accum[i];
		tan2 = tan2Accum[i];

		// Gram-Schmidt orthogonalize
		tangents[i] = vec4(normalize(tan1 - (dot(tempNormal, tan1) * tempNormal)), 0.0f);

		// Store handedness in w
		tangents[i].w = (dot(cross(tempNormal, tan1), tan2)) < 0.0f ? -1.0f : 1.0f;
	}

	tan1Accum.clear();
	tan2Accum.clear();

	/////////////////////////

	//logFile.close();
	

	return true;
}


void HostMesh::buildBounds(u_int3 subdivision)
{
	vec3 min(INF);
	vec3 max(-INF);
	std::vector<int> elementList;

	// Calculate the bounding box limits
	for(int i=0; i<positions.size(); i++)
	{
		if(positions[i].x < min.x) min.x = positions[i].x;
		if(positions[i].y < min.y) min.y = positions[i].y;
		if(positions[i].z < min.z) min.z = positions[i].z;
		if(positions[i].x > max.x) max.x = positions[i].x;
		if(positions[i].y > max.y) max.y = positions[i].y;
		if(positions[i].z > max.z) max.z = positions[i].z;
	}

	//printf("MESH min ( x: %.2f, y: %.2f, z: %.2f )\n", min.x, min.y, min.z);
	//printf("MESH max ( x: %.2f, y: %.2f, z: %.2f )\n", max.x, max.y, max.z);

	bounds = HostBoundingBox( min, max, subdivision, &elements, &positions, &uvs, &vnormals, &fnormals, &tangents);

	int index = 0;
	bounds.recursiveMark(&index);
	//printf("At the end index = %d\n", index);
}


void HostMesh::setMaterial(int _material)
{
	material = _material;
}


void HostMesh::clear()
{
	elements.clear();
	positions.clear();
	uvs.clear();
	vnormals.clear();
	fnormals.clear();
	tangents.clear();
}


void HostMesh::translation(vec3 factor)
{
	for(int i=0; i<positions.size(); i++)
	{
		positions[i] = positions[i] + factor;
	}
}


void HostMesh::scale(vec3 factor)
{
	//vec3 position;
	//printf("Scaling mesh:\n");
	for(int i=0; i<positions.size(); i++)
	{
		positions[i] = positions[i] * factor;
	}
	//printf("Done scaling\n");
}


void HostMesh::rotation(vec3 axis, float rads)
{
	vec3 tempTangent;

	for(int i=0; i<positions.size(); i++)
	{
		positions[i] = rotate(positions[i],axis,rads);
		vnormals[i] = rotate(vnormals[i],axis,rads);

		tempTangent = vec3(tangents[i].x, tangents[i].y, tangents[i].z);
		tempTangent = rotate(tempTangent,axis,rads);
		tangents[i] = vec4(tempTangent, tangents[i].w);
	}
}

#endif //_HOST_MESH_