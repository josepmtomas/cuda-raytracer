#ifndef _DEVICE_SCENE_
#define _DEVICE_SCENE_

#include "HostScene.cuh"
#include "DeviceAreaLight.cuh"
#include "DeviceMesh.cuh"
#include "Shading.cuh"
/*#include "Material.cuh"
#include "Sphere.cuh"
#include "Box.cuh"
#include "Cylinder.cuh"
#include "PerspectiveCamera.cuh"*/

class DeviceScene
{
public:

	Material			**materials;
	DeviceTexture		**textures;

	Box					**boxes;
	Cone				**cones;
	Cylinder			**cylinders;
	Sphere				**spheres;
	DeviceMesh			**meshes;
	CSG_Node			**csgTrees;
	PointLight			**pointLights;
	DeviceAreaLight		**areaLights;
	PerspectiveCamera	*camera;

	int	numBoxes;
	int	numCones;
	int	numCylinders;
	int	numSpheres;
	int numMeshes;
	int numCSGTrees;
	int	numMaterials;
	int numTextures;
	int	numPointLights;
	int numAreaLights;

	__host__ __device__ DeviceScene();

	__host__ __device__ void setMaterials(Material **_materials, int _numMaterials);
	__host__ __device__ void setTextures(DeviceTexture **_textures, int _numTextures);
	__host__ __device__ void setBoxes(Box **_boxes, int _numBoxes);
	__host__ __device__ void setCones(Cone **_cones, int _numCones);
	__host__ __device__ void setCylinders(Cylinder **_cylinders, int _numCylinders);
	__host__ __device__ void setSpheres(Sphere **_spheres, int _numSpheres);
	__host__ __device__ void setMeshes(DeviceMesh **_meshes, int _numMeshes);
	__host__ __device__ void setCSGTrees(CSG_Node **_trees, int _numTrees);
	__host__ __device__ void setPointLights(PointLight **_pointLights, int _numPointLights);
	__host__ __device__ void setAreaLights(DeviceAreaLight **_areaLights, int _numAreaLights);
	__host__ __device__ void setCamera(PerspectiveCamera *_camera);

	__host__ __device__ vec3 getDiffuseAlbedo(Intersection &intersection);
	__host__ __device__ vec3 getSpecularAlbedo(Intersection &intersection);
	__host__ __device__ vec3 getEmissiveAlbedo(Intersection &intersection);
	__host__ __device__ vec3 getNormalAt(Intersection &intersection);

	__host__ __device__ bool getNearestIntersection(vec3 origin, vec3 ray, Intersection &in);
	__host__ __device__ bool getNearestIntersectionAll(vec3 origin, vec3 ray, Intersection &in);
	__host__ __device__ bool getOutIntersection(vec3 ray, Intersection &in, Intersection &out, vec3 &direction);
	__host__ __device__ vec3 getTransmission(vec3 ray, Intersection &in, Intersection &out);
	__host__ __device__ int  getTransmission(vec3 transmissionRay, Intersection &in, Intersection &out, vec3 &rayOut);

			 __device__ vec3 getDirectLighting(Intersection &intersection, vec3 eye, curandState *state);
	__host__ __device__ vec3 getPointLightIlluminationAt(int index, Intersection &intersection, vec3 eye, bool diffuse, bool specular);
	__host__ __device__ vec3 getAreaLightIlluminationAt(int index, Intersection &intersection, vec3 eye);
	__host__ __device__ vec3 getAreaLightIlluminationAt(int index, Intersection &intersection, vec3 eye, vec2 offset, bool diffuse, bool specular);

};





DeviceScene::DeviceScene()
{
	numMaterials = 0;
	numBoxes = 0;
	numCylinders = 0;
	numSpheres = 0;
}

void DeviceScene::setMaterials(Material **_materials, int _numMaterials)
{
	materials = _materials;
	numMaterials = _numMaterials;
}


void DeviceScene::setTextures(DeviceTexture **_textures, int _numTextures)
{
	textures = _textures;
	numTextures = _numTextures;
}


void DeviceScene::setBoxes(Box **_boxes, int _numBoxes)
{
	boxes = _boxes;
	numBoxes = _numBoxes;
}


void DeviceScene::setCones(Cone **_cones, int _numCones)
{
	cones = _cones;
	numCones = _numCones;
}


void DeviceScene::setCylinders(Cylinder **_cylinders, int _numCylinders)
{
	cylinders = _cylinders;
	numCylinders = _numCylinders;
}


void DeviceScene::setSpheres(Sphere **_spheres, int _numSpheres)
{
	spheres = _spheres;
	numSpheres = _numSpheres;
}


void DeviceScene::setMeshes(DeviceMesh **_meshes, int _numMeshes)
{
	meshes = _meshes;
	numMeshes = _numMeshes;
}


void DeviceScene::setCSGTrees(CSG_Node **_trees, int _numTrees)
{
	csgTrees = _trees;
	numCSGTrees = _numTrees;
}


void DeviceScene::setPointLights(PointLight **_pointLights, int _numPointLights)
{
	pointLights = _pointLights;
	numPointLights = _numPointLights;
}


void DeviceScene::setAreaLights(DeviceAreaLight **_areaLights, int _numAreaLights)
{
	areaLights = _areaLights;
	numAreaLights = _numAreaLights;
}


void DeviceScene::setCamera(PerspectiveCamera *_camera)
{
	camera = _camera;
}


vec3 DeviceScene::getDiffuseAlbedo(Intersection &intersection)
{
	int material = intersection.material;
	int texture = materials[material]->diffuseTexture;

	if(texture >= 0)
	{
		return textures[texture]->at(intersection.uv) * materials[material]->diffuseColor;
	}
	else
	{
		return materials[material]->diffuseColor;
	}
}

vec3 DeviceScene::getSpecularAlbedo(Intersection &intersection)
{
	int material = intersection.material;
	int texture = materials[material]->specularTexture;

	if(texture >= 0)
	{
		return textures[texture]->at(intersection.uv) * materials[material]->specularColor;
	}
	else
	{
		return materials[material]->specularColor;
	}
}


vec3 DeviceScene::getEmissiveAlbedo(Intersection &intersection)
{
	int material = intersection.material;
	int texture = materials[material]->emissiveTexture;

	if(texture >= 0)
	{
		return textures[texture]->at(intersection.uv) * materials[material]->emissiveColor;
	}
	else
	{
		return materials[material]->emissiveColor;
	}
}


vec3 DeviceScene::getNormalAt(Intersection &intersection)
{
	int material = intersection.material;
	int texture = materials[material]->normalTexture;
	vec3 binormal;
	vec3 normal;

	if(texture >= 0 && intersection.objectType == OBJECT_TYPE_MESH)
	{
		normal = textures[texture]->at(intersection.uv);
		normal = normal * 2.0 - 1.0;
		normal = normalize(normal);
		binormal = normalize(cross(intersection.normal, intersection.tangent));
		normal = intersection.tangent*normal.x + binormal*normal.y + intersection.normal*normal.z;
		return normal;
	}
	else
	{
		return intersection.normal;
	}
}


bool DeviceScene::getNearestIntersection(vec3 origin, vec3 ray, Intersection &in)
{
	Intersection	minimum = Intersection();
	Intersection	current;

	for(int i=0; i<numBoxes; i++)
	{
		current = Intersection();
		if(boxes[i]->rayIntersection(origin, ray, current))
		{
			if(current < minimum)
				minimum = current;
		}
	}

	for(int i=0; i<numCones; i++)
	{
		current = Intersection();
		if(cones[i]->rayIntersection(origin, ray, current))
		{
			if(current < minimum)
				minimum = current;
		}
	}

	for(int i=0; i<numCylinders; i++)
	{
		current = Intersection();
		if(cylinders[i]->rayIntersection(origin, ray, current))
		{
			if(current < minimum)
				minimum = current;
		}
	}

	for(int i=0; i<numSpheres; i++)
	{
		current = Intersection();
		if(spheres[i]->rayIntersection(origin, ray, current))
		{
			if(current < minimum)
				minimum = current;
		}
	}

	for(int i=0; i<numMeshes; i++)
	{
		current = Intersection();
		if(meshes[i]->rayIntersection(origin, ray, current))
		{
			if(current < minimum)
				minimum = current;
		}
	}

	for(int i=0; i<numCSGTrees; i++)
	{
		current = Intersection();
		if(csgTrees[i]->rayIntersection(origin, ray, current))
		{
			if(current < minimum)
				minimum = current;
		}
	}

	if(minimum.distance == INF)
	{
		return false;
	}
	else
	{
		in = minimum;
		return true;
	}
}


bool DeviceScene::getNearestIntersectionAll(vec3 origin, vec3 ray, Intersection &in)
{
	Intersection	minimum = Intersection();
	Intersection	current;

	for(int i=0; i<numBoxes; i++)
	{
		current = Intersection();
		if(boxes[i]->rayIntersection(origin, ray, current))
		{
			if(current < minimum)
				minimum = current;
		}
	}

	for(int i=0; i<numCones; i++)
	{
		current = Intersection();
		if(cones[i]->rayIntersection(origin, ray, current))
		{
			if(current < minimum)
				minimum = current;
		}
	}

	for(int i=0; i<numCylinders; i++)
	{
		current = Intersection();
		if(cylinders[i]->rayIntersection(origin, ray, current))
		{
			if(current < minimum)
				minimum = current;
		}
	}

	for(int i=0; i<numSpheres; i++)
	{
		current = Intersection();
		if(spheres[i]->rayIntersection(origin, ray, current))
		{
			if(current < minimum)
				minimum = current;
		}
	}

	for(int i=0; i<numAreaLights; i++)
	{
		current = Intersection();
		if(areaLights[i]->rayIntersect(origin, ray, current))
		{
			if(current < minimum)
				minimum = current;
		}
	}

	for(int i=0; i<numMeshes; i++)
	{
		current = Intersection();
		if(meshes[i]->rayIntersection(origin, ray, current))
		{
			if(current < minimum)
				minimum = current;
		}
	}

	for(int i=0; i<numCSGTrees; i++)
	{
		current = Intersection();
		if(csgTrees[i]->rayIntersection(origin, ray, current))
		{
			if(current < minimum)
				minimum = current;
		}
	}

	if(minimum.distance == INF)
	{
		return false;
	}
	else
	{
		in = minimum;
		return true;
	}
}


int  DeviceScene::getTransmission(vec3 transmissionRay, Intersection &in, Intersection &out, vec3 &rayOut)
{
	Intersection current = Intersection();
	Intersection scene   = Intersection();

	vec3	origin = in.point + in.normal*EPSILON;
	vec3	directionOut;

	// Search for the exiting intersection for this object
	switch(in.objectType)
	{
	case OBJECT_TYPE_BOX:
		boxes[in.objectIndex]->rayIntersectionOut(origin,transmissionRay,current);
		break;

	case OBJECT_TYPE_CONE:
		cones[in.objectIndex]->rayIntersectionOut(origin,transmissionRay,current);
		break;

	case OBJECT_TYPE_CYLINDER:
		cylinders[in.objectIndex]->rayIntersectionOut(origin,transmissionRay,current);
		break;

	case OBJECT_TYPE_SPHERE:
		spheres[in.objectIndex]->rayIntersectionOut(origin,transmissionRay,current);
		break;

	default:
		return TRANSMISSION_FAIL;
	}

	// If the transmission vector for exiting this object exists ( a.k.a. not internal reflection )
	if(getTransmissionVector(materials[in.material]->refractionIndex, 1.0, negate(current.normal), negate(transmissionRay), directionOut))
	{
		if(getNearestIntersection(in.point - in.normal*EPSILON, transmissionRay, scene))
		{
			if(in.objectType != scene.objectType || in.objectIndex != scene.objectIndex)
			{
				// Compare which is more near, object or itself

				if(current > scene)
				{
					out = scene;
					return TRANSMISSION_IN_OBJECT;
				}
				else
				{
					out = current;
					rayOut = directionOut;
					return TRANSMISSION_IN_OUT;
				}
			}
			else
			{
				out = current;
				rayOut = directionOut;
				return TRANSMISSION_IN_OUT;
			}
		}
		else
		{
			out = current;
			rayOut = directionOut;
			return TRANSMISSION_IN_OUT;
		}
	}
	else
	{
		return TRANSMISSION_FAIL;
	}

}


vec3 DeviceScene::getTransmission(vec3 ray, Intersection &in, Intersection &out)
{
	Intersection inIntersection = Intersection();
	Intersection outIntersection = Intersection();
	Intersection sceneIntersection = Intersection();
	vec3	origin;
	float	indexi = materials[in.material]->refractionIndex;
	bool	valid = false;
	vec3	directionIn, directionOut;
	int		count = 0;
	
	directionIn = ray;
	origin = in.point+in.normal*EPSILON;
	
	getTransmissionVector(1.0,indexi,in.normal,negate(ray), directionIn);

	do
	{
		switch(in.objectType)
		{
		case OBJECT_TYPE_BOX:
			boxes[in.objectIndex]->rayIntersectionOut(origin,directionIn,out);
			break;

		case OBJECT_TYPE_CONE:
			cones[in.objectIndex]->rayIntersectionOut(origin,directionIn,out);
			break;

		case OBJECT_TYPE_CYLINDER:
			cylinders[in.objectIndex]->rayIntersectionOut(origin,directionIn,out);
			break;

		case OBJECT_TYPE_SPHERE:
			spheres[in.objectIndex]->rayIntersectionOut(origin,directionIn,out);
			break;

		default:
			return false;
		}

		//return direction;
		if(getTransmissionVector(indexi,1.0,negate(out.normal),negate(directionIn),directionOut))
		{
			valid = true;
			count = 2;
		}
		else
		{
			directionIn = directionOut;
			origin = out.point+out.normal*EPSILON;
			valid = false;
			count++;
		}
	}
	while(valid==false && count < 2);

	return directionOut;
}


bool DeviceScene::getOutIntersection(vec3 ray, Intersection &in, Intersection &out, vec3 &direction)
{
	vec3 origin = in.point+in.normal;
	float  indexi = materials[in.material]->refractionIndex;

	vec3 directionIn, directionOut;
	
	getTransmissionVector(1.0,indexi,in.normal,negate(ray), directionIn);
	//return direction;

	switch(in.objectType)
	{
	case OBJECT_TYPE_BOX:
		boxes[in.objectIndex]->rayIntersectionOut(origin,directionIn,out);
		break;

	case OBJECT_TYPE_CONE:
		cones[in.objectIndex]->rayIntersectionOut(origin,directionIn,out);
		break;

	case OBJECT_TYPE_CYLINDER:
		cylinders[in.objectIndex]->rayIntersectionOut(origin,directionIn,out);
		break;

	case OBJECT_TYPE_SPHERE:
		spheres[in.objectIndex]->rayIntersectionOut(origin,directionIn,out);
		break;

	default:
		return false;
	}

	//return direction;
	if(getTransmissionVector(indexi,1.0,negate(out.normal),negate(directionIn),directionOut))
	{
		direction = directionOut;
		return true;
	}
	else
	{
		direction = directionOut;
		return false;
	}
}


vec3 DeviceScene::getDirectLighting(Intersection &intersection, vec3 eye, curandState *state)
{
	vec2 areaLightOffset;
	vec3 output = vec3(0,0);

	areaLightOffset.x = curand_uniform(state);
	areaLightOffset.y = curand_uniform(state);

	for(int i=0; i<numAreaLights; i++)
	{
		output = output + getAreaLightIlluminationAt(i, intersection, eye, areaLightOffset, true, true);
	}

	for(int i=0; i<numPointLights; i++)
	{
		output = output + getPointLightIlluminationAt(i, intersection, eye, true, true);
	}

	output = output + materials[intersection.material]->emissiveColor;

	return output;
}


vec3 DeviceScene::getPointLightIlluminationAt(int index, Intersection &intersection, vec3 eye, bool _diffuse, bool _specular)
{
	bool isIlluminated;
	vec3 vLight = normalize(pointLights[index]->position - intersection.point);
	vec3 diffuse = vec3(0.0);
	vec3 specular = vec3(0.0);
	Intersection lightIntersection = Intersection();

	if(getNearestIntersection(intersection.point, vLight, lightIntersection))
	{
		if(lightIntersection.distance < distance(pointLights[index]->position, intersection.point))
		{
			isIlluminated = false;
		}
		else
		{
			isIlluminated = true;
		}
	}
	else
	{
		isIlluminated = true;
	}

	if(isIlluminated)
	{
		if(_diffuse) diffuse = getOrenNayar(vLight, eye, intersection.normal, pointLights[index]->color, getDiffuseAlbedo(intersection), materials[intersection.material]->roughness);
		if(_specular) specular = getCookTorrance(vLight, eye, intersection.normal, pointLights[index]->color, getSpecularAlbedo(intersection), materials[intersection.material]->roughness);
		return diffuse + specular;
	}
	else
	{
		return vec3(0.0);
	}
}

vec3 DeviceScene::getAreaLightIlluminationAt(int index, Intersection &intersection, vec3 eye)
{
	vec3 vLight;
	vec3 output = vec3(0.0,0.0,0.0);
	vec3 diffuse = vec3(0.0,0.0,0.0);
	vec3 specular = vec3(0.0,0.0,0.0);
	bool isIlluminated;
	float pointLightDistance;
	Intersection lightIntersection;

	if(areaLights[index]->type == AREA_LIGHT_PLANE)
	{
		for(int j=0; j<areaLights[index]->numPoints; j++)
		{
			//return vec3(dot(eye,normal));
			isIlluminated = false;
			vLight = normalize(areaLights[index]->points[j] - intersection.point);
			pointLightDistance = distance(areaLights[index]->points[j], intersection.point);
			lightIntersection = Intersection();

			// Check if there's an object between the point and the light
			if(getNearestIntersection(intersection.point, vLight, lightIntersection))
			{
				if(lightIntersection.distance < pointLightDistance)
				{
					isIlluminated = false;
				}
				else
				{
					isIlluminated = true;
				}
			}
			else
			{
				isIlluminated = true;
			}

			// If the point is being illuminated directly from the light, calculate shading at the point
			if(isIlluminated)
			{
				//output = output + vec3(1.0);
				//output = output + getOrenNayar(vLight, eye, normal, areaLights[index]->color, vec3(1.0,1.0,1.0), 0.2);
				diffuse = diffuse + getOrenNayar(vLight, eye, intersection.normal, areaLights[index]->color, getDiffuseAlbedo(intersection), materials[intersection.material]->roughness);
				//specular = specular + getCookTorrance(vLight, eye, intersection.normal, areaLights[index]->color, materials[intersection.material]->getSpecular(intersection.uv), materials[intersection.material]->roughness);
				specular = specular + getCookTorrance(vLight, eye, intersection.normal, areaLights[index]->color, getSpecularAlbedo(intersection), materials[intersection.material]->roughness);
			}
		}

		diffuse = diffuse / (float)areaLights[index]->numPoints;
		specular = specular / (float)areaLights[index]->numPoints;

		return diffuse + specular;
		//return output * (1.0 / areaLights[index]->numPoints);
	}
	else
	{
		return vec3(0.0);
	}
}


vec3 DeviceScene::getAreaLightIlluminationAt(int index, Intersection &intersection, vec3 eye, vec2 offset, bool _diffuse, bool _specular)
{
	vec3 vLight;
	vec3 point;
	vec3 output = vec3(0.0,0.0,0.0);
	vec3 diffuse = vec3(0.0,0.0,0.0);
	vec3 specular = vec3(0.0,0.0,0.0);
	bool isIlluminated;
	float pointLightDistance;
	Intersection lightIntersection;

	if(areaLights[index]->type == AREA_LIGHT_PLANE)
	{
		for(int j=0; j<areaLights[index]->numPoints; j++)
		{
			point = areaLights[index]->points[j] + 
				areaLights[index]->tangents[j]*offset.x*areaLights[index]->spacing.x + 
				areaLights[index]->binormals[j]*offset.y*areaLights[index]->spacing.y;

			//return vec3(dot(eye,normal));
			isIlluminated = false;
			vLight = normalize(point - intersection.point);
			pointLightDistance = distance(point, intersection.point);
			lightIntersection = Intersection();

			// Check if there's an object between the point and the light
			if(getNearestIntersection(intersection.point, vLight, lightIntersection))
			{
				if(lightIntersection.distance < pointLightDistance)
				{
					isIlluminated = false;
				}
				else
				{
					isIlluminated = true;
				}
			}
			else
			{
				isIlluminated = true;
			}

			// If the point is being illuminated directly from the light, calculate shading at the point
			if(isIlluminated)
			{
				//output = output + vec3(1.0);
				//output = output + getOrenNayar(vLight, eye, normal, areaLights[index]->color, vec3(1.0,1.0,1.0), 0.2);
				if(_diffuse) diffuse = diffuse + getOrenNayar(vLight, eye, intersection.normal, areaLights[index]->color, getDiffuseAlbedo(intersection), materials[intersection.material]->roughness);
				//specular = specular + getCookTorrance(vLight, eye, intersection.normal, areaLights[index]->color, materials[intersection.material]->getSpecular(intersection.uv), materials[intersection.material]->roughness);
				if(_specular) specular = specular + getCookTorrance(vLight, eye, intersection.normal, areaLights[index]->color, getSpecularAlbedo(intersection), materials[intersection.material]->roughness);
			}
		}

		diffuse = diffuse / (float)areaLights[index]->numPoints;
		specular = specular / (float)areaLights[index]->numPoints;

		return diffuse + specular;
		//return output * (1.0 / areaLights[index]->numPoints);
	}
	else
	{
		return vec3(0.0);
	}
}


#endif // _DEVICE_SCENE_