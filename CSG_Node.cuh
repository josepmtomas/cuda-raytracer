#ifndef _CSG_NODE_
#define _CSG_NODE_

#include "Intersection.cuh"
#include "CSG_Sphere.cuh"
#include "CSG_Box.cuh"
#include "CSG_Cylinder.cuh"

class CSG_Node
{
protected:

	int  index;			// Index of this node in its array
	//int2 arrayIndex;	// First element(X) and last element(Y) of the array

	//__device__ __host__ void operationUnion(Intersection *intersectionsA, Intersection *intersectionsB, int2 *arrayIndex);
	//__device__ __host__ void operationDifference(Intersection *intersectionsA, Intersection *intersectionsB, int2 *arrayIndex);
	//__device__ __host__ void operationIntersection(Intersection *intersectionsA, Intersection *intersectionsB, int2 *arrayIndex);

public:

	int type;			// Type of the CSG node ( ROOT or NODE )
	int	operation;		// Operation that performs this node
	int type_a;			// Type of the left children
	int type_b;			// Type of the right children
	int index_a;
	int index_b;
	int n_primitives;	// Number of primitive nodes in the tree
	int material;

	CSG_Node		*node_a, *node_b;
	CSG_Sphere		*sphere_a, *sphere_b;
	CSG_Box			*box_a, *box_b;
	CSG_Cylinder	*cylinder_a, *cylinder_b;


	__device__ __host__ CSG_Node();
	__device__ __host__ CSG_Node(int _type, int _operation);

	__device__ __host__ void addChild(int _child, CSG_Node *_node);
	__device__ __host__ void addChild(int _child, CSG_Sphere *_sphere);
	__device__ __host__ void addChild(int _child, CSG_Box *_box);
	__device__ __host__ void addChild(int _child, CSG_Cylinder *_cylinder);

	__device__ __host__ bool rayIntersection(vec3 origin, vec3 ray, Intersection *intersections, int2 *arrayIndex);
	__device__ __host__ bool rayIntersection(vec3 origin, vec3 ray, Intersection &intersection);

};

#endif // _CSG_NODE_


//////////////////////////////////////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////////////////////////////////////


CSG_Node::CSG_Node()
{
	type = OBJECT_TYPE_CSG_NODE;
	index = 0;
	type_a = -1;
	type_b = -1;
	operation = CSG_OPERATION_NONE;
	n_primitives = 0;
}


CSG_Node::CSG_Node(int _type, int _operation)
{
	type = _type;
	operation = _operation;
	index = 0;
	type_a = -1;
	type_b = -1;
	n_primitives = 0;
}


void CSG_Node::addChild(int _child, CSG_Node *_node)
{
	switch(_child)
	{
	case CSG_CHILD_LEFT:
		type_a = OBJECT_TYPE_CSG_NODE;
		node_a = _node;
		break;

	case CSG_CHILD_RIGHT:
		type_b = OBJECT_TYPE_CSG_NODE;
		node_b = _node;
		break;

	default:
		break;
	}
}


void CSG_Node::addChild(int _child, CSG_Sphere *_sphere)
{
	switch(_child)
	{
	case CSG_CHILD_LEFT:
		type_a = OBJECT_TYPE_CSG_SPHERE;
		sphere_a = _sphere;
		break;

	case CSG_CHILD_RIGHT:
		type_b = OBJECT_TYPE_CSG_SPHERE;
		sphere_b = _sphere;
		break;

	default:
		break;
	}
}


void CSG_Node::addChild(int _child, CSG_Box *_box)
{
	switch(_child)
	{
	case CSG_CHILD_LEFT:
		type_a = OBJECT_TYPE_CSG_BOX;
		box_a = _box;
		break;

	case CSG_CHILD_RIGHT:
		type_b = OBJECT_TYPE_CSG_BOX;
		box_b = _box;
		break;

	default:
		break;
	}
}


void CSG_Node::addChild(int _child, CSG_Cylinder *_cylinder)
{
	switch(_child)
	{
	case CSG_CHILD_LEFT:
		type_a = OBJECT_TYPE_CSG_CYLINDER;
		cylinder_a = _cylinder;
		break;

	case CSG_CHILD_RIGHT:
		type_b = OBJECT_TYPE_CSG_CYLINDER;
		cylinder_b = _cylinder;
		break;

	default:
		break;
	}
}


bool CSG_Node::rayIntersection(vec3 origin, vec3 ray, Intersection *intersections, int2 *arrayIndex)
{
	Intersection x;
	int2 indexA;
	int2 indexB;
	bool hitA, hitB;
	int i,j,k;
	int current,previous,state;

	indexA.x = arrayIndex->y;
	indexA.y = arrayIndex->y;

	switch(type_a)
	{
	case OBJECT_TYPE_CSG_NODE:
		if(node_a->rayIntersection(origin,ray,intersections,&indexA))
		{
			//indexA.x += 1;
		}
		break;
	case OBJECT_TYPE_CSG_SPHERE:
		if(sphere_a->rayIntersection(origin, ray, intersections[indexA.y], intersections[indexA.y+1]))
		{
			//indexA.x += 1;
			indexA.y += 1;
		}
		break;
	case OBJECT_TYPE_CSG_BOX:
		if(box_a->rayIntersection(origin, ray, intersections[indexA.y], intersections[indexA.y+1]))
		{
			//indexA.x += 1;
			indexA.y += 1;
		}
		break;
	case OBJECT_TYPE_CSG_CYLINDER:
		if(cylinder_a->rayIntersection(origin, ray, intersections[indexA.y], intersections[indexA.y+1]))
		{
			//indexA.x += 1;
			indexA.y += 1;
		}
		break;
	default:
		break;
	}

	if(indexA.x >= indexA.y)
	{
		hitA = false;
		indexB = make_int2(arrayIndex->y, arrayIndex->y);
		//return false;

		if(operation == CSG_OPERATION_DIFFERENCE || operation == CSG_OPERATION_INTERSECTION)
		{
			return false;
		}
	}
	else
	{
		hitA = true;
		indexB = make_int2(indexA.y+1, indexA.y+1);
		/*arrayIndex->x = indexA.x;
		arrayIndex->y = indexA.y;
		return true;*/
	}

	switch(type_b)
	{
	case OBJECT_TYPE_CSG_NODE:
		if(node_b->rayIntersection(origin, ray, intersections, &indexB))
		{
			//indexB.x += 1;
		}
		break;
	case OBJECT_TYPE_CSG_SPHERE:
		if(sphere_b->rayIntersection(origin, ray, intersections[indexB.y], intersections[indexB.y+1]))
		{
			//indexB.x += 1;
			indexB.y += 1;
		}
		break;
	case OBJECT_TYPE_CSG_BOX:
		if(box_b->rayIntersection(origin, ray, intersections[indexB.y], intersections[indexB.y+1]))
		{
			//indexB.x += 1;
			indexB.y += 1;
		}
		break;
	case OBJECT_TYPE_CSG_CYLINDER:
		if(cylinder_b->rayIntersection(origin, ray, intersections[indexB.y], intersections[indexB.y+1]))
		{
			//indexB.x += 1;
			indexB.y += 1;
		}
		break;
	}
	

	// Process the sublists by node operation

	// DIFFERENCE -> invert intersections
	if(operation == CSG_OPERATION_DIFFERENCE)
	{
		if(indexB.x < indexB.y)					// If subarray B contains something
		{
			i = indexB.x;
			while(i <= indexB.y)
			{
				if(intersections[i].type == INTERSECTION_IN)
				{
					intersections[i].type = INTERSECTION_OUT;
				}
				else
				{
					intersections[i].type = INTERSECTION_IN;
				}
				intersections[i].normal = negate(intersections[i].normal);
				i++;
			}
		}
	}

	if(indexB.x >= indexB.y)
	{
		hitB = false;
	}
	else
	{
		hitB = true;
	}

	if(hitA && !hitB)
	{
		if(operation == CSG_OPERATION_UNION || operation == CSG_OPERATION_DIFFERENCE)
		{
			arrayIndex->y = indexA.y;
			return true;
		}
		else if(operation == CSG_OPERATION_INTERSECTION)
		{
			return false;
		}
	}
	else if(!hitA && hitB)
	{
		if(operation == CSG_OPERATION_UNION)
		{
			arrayIndex->y = indexB.y;
			return true;
		}
		else if(operation == CSG_OPERATION_DIFFERENCE || operation == CSG_OPERATION_INTERSECTION)
		{
			return false;
		}
	}
	else if(!hitA && !hitB)
	{
		return false;
	}

	// If hitA and hitB -> Sort the subarray [A,B]
	int incr = (indexB.y-indexA.x+1)/2;

	// Shell sort
	while(incr > 0)
	{
		for(i=incr+indexA.x; i<=indexB.y; i++)
		{
			x = intersections[i];
			j = i;
			while((j >= incr+indexA.x) && (x.distance < intersections[j-incr].distance))
			{
				intersections[j] = intersections[j-incr];
				j -= incr;
			}
			intersections[j] = x;
		}
		incr = incr/2;
	}

	// Delete negative values
	i=0;
	k=0;
	while(i <= indexB.y)
	{
		if(intersections[i].distance < 0)
		{
			k++;
		}
		else
		{
			intersections[i-k] = intersections[i];
		}
		i++;
	}
	indexB.y = indexB.y - k;

	if(indexB.y == 0 || (indexB.y + 1) % 2 != 0)
	{
		return false;
	}

	i = indexA.x+1;
	switch(operation)
	{
	case CSG_OPERATION_UNION:
		
		while(i <= indexB.y)
		{
			previous = intersections[i-1].type;
			current  = intersections[i].type;

			if(current == INTERSECTION_IN && previous == INTERSECTION_IN)
			{
				k = i+1;
				while(k <= indexB.y)
				{
					intersections[k-1] = intersections[k];
					k++;
				}
				indexB.y = indexB.y - 1;
			}
			else if(previous == INTERSECTION_OUT && current == INTERSECTION_OUT)
			{
				k = i;
				while(k <= indexB.y)
				{
					intersections[k-1] = intersections[k];
					k++;
				}
				indexB.y = indexB.y - 1;
			}
			else
			{
				i++;
			}
		}

		if(indexA.x < indexB.y)				// If there is at least an pair of intersections (IN/OUT)
		{
			//arrayIndex->x = indexA.x;
			arrayIndex->y = indexB.y;		// The index of the last element of the recieved array has increased
			return true;
		}
		else
		{
			return false;
		}
		//break;

	case CSG_OPERATION_DIFFERENCE:

		while(i <= indexB.y)
		{
			previous = intersections[i-1].type;
			current  = intersections[i].type;

			/*if(current == Intersection::IntersectionType::INTERIOR && previous == Intersection::IntersectionType::INTERIOR)
			{
				k = i+2;
				while(k <= indexB.y)
				{
					intersections[k-2] = intersections[k];
					k++;
				}
				indexB.y = indexB.y - 2;
			}
			else*/ if(previous == INTERSECTION_OUT && current == INTERSECTION_IN)
			{
				k = i+1;
				while(k <= indexB.y)
				{
					intersections[k-2] = intersections[k];
					k++;
				}
				indexB.y = indexB.y - 2;
			}
			else
			{
				i+=2;
			}
		}

		if(indexA.x < indexB.y)				// If there is at least an pair of intersections (IN/OUT)
		{
			arrayIndex->y = indexB.y;		// The index of the last element of the recieved array has increased
			return true;
		}
		else
		{
			return false;
		}
		//break;

	case CSG_OPERATION_INTERSECTION:

		// 0 - normal
		// 1 - waiting IN IN
		state = 1;

		indexA.y = i-1;

		while(i <= indexB.y)
		{
			previous = intersections[i-1].type;
			current  = intersections[i].type;
			
			if(previous == INTERSECTION_IN && current == INTERSECTION_IN)
			{
				j = i;
				k = i - indexA.y;
				while(j <= indexB.y)
				{
					intersections[j-k] = intersections[j];
					j++;
				}
				indexB.y = indexB.y - k;
				i = i - k + 1;
				state = 0;
			}
			else if(previous == INTERSECTION_OUT && current == INTERSECTION_OUT)
			{
				indexA.y = i;
				state = 1;
				i++;
			}
			else
			{
				i++;
			}
		}

		if(state == 1)
		{
			indexB.y = indexA.y-1;
		}

		if(indexA.x < indexB.y)				// If there is at least an pair of intersections (IN/OUT)
		{
			arrayIndex->y = indexB.y;		// The index of the last element of the recieved array has increased
			return true;
		}
		else
		{
			return false;
		}
		//break;

	default:
		break;
	}

	return false;
}


bool CSG_Node::rayIntersection(vec3 origin, vec3 ray, Intersection &intersection)
{
	//return false;

	Intersection in = Intersection();
	Intersection out = Intersection();

	Intersection *intersections = (Intersection*)malloc(sizeof(Intersection)*n_primitives*2);
	int2 arrayIndex = make_int2(0,0);

	//if(rayIntersection(origin,ray,operation,in,out))
	if(rayIntersection(origin,ray,intersections,&arrayIndex))
	{
		intersection = intersections[0];
		intersection.material = material;
		free(intersections);
		return true;
	}
	else
	{
		free(intersections);
		return false;
	}
}


