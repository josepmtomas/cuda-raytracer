#ifndef _DEVICE_BOUNDING_BOX_
#define _DEVICE_BOUNDING_BOX_

#include "Algebra.cuh"
#include "Intersection.cuh"

class DeviceBoundingBox
{
protected:

	// Mesh information
	int		*polygons;
	int		 numPolygons;
	vec3	*meshPositions;
	vec2	*meshUVs;
	vec3	*meshVNormals;
	vec3	*meshFNormals;
	vec4	*meshTangents;

	// Bounding Box tree information
	int		*children;
	int		numChildren;
	DeviceBoundingBox	*boundsArray;

	// Bounding box definition
	int		*elements;
	vec3	*positions;
	vec3	*fnormals;
	vec3	boundsMin;
	vec3	boundsMax;

public:

	__host__ __device__ DeviceBoundingBox();

	__host__ __device__ void setBoundingBoxDefinition(int *_elements, vec3 *_positions, vec3 *_fnormals, vec3 min, vec3 max);
	__host__ __device__ void setChildren(int *_children, int _numChildren);
	__host__ __device__ void setBoundingBoxArray(DeviceBoundingBox *_boundsArray);
	__host__ __device__ void setMeshInformation(int *_polygons, int _numPolygons, vec3 *_meshPositions, vec2 *_meshUVs, vec3 *_meshVNormals,
												vec3 *_meshFNormals, vec4 *_meshTangents);

	__host__ __device__ bool pointInside(vec3 point) const;
	__host__ __device__ bool boundIntersection(vec3 origin, vec3 ray, Intersection &intersection) const;
	__host__ __device__ bool rayIntersection(vec3 origin, vec3 ray, Intersection &intersection) const;

};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////


DeviceBoundingBox::DeviceBoundingBox()
{}


void DeviceBoundingBox::setBoundingBoxDefinition(int *_elements, vec3 *_positions, vec3 *_fnormals, vec3 min, vec3 max)
{
	elements = _elements;
	positions = _positions;
	fnormals = _fnormals;
	boundsMin = min;
	boundsMax = max;
}


void DeviceBoundingBox::setChildren(int *_children, int _numChildren)
{
	children = _children;
	numChildren = _numChildren;
}


void DeviceBoundingBox::setBoundingBoxArray(DeviceBoundingBox *_boundsArray)
{
	boundsArray = _boundsArray;
}


void DeviceBoundingBox::setMeshInformation(int *_polygons, int _numPolygons, vec3 *_meshPositions, vec2 *_meshUVs, vec3 *_meshVNormals,
											vec3 *_meshFNormals, vec4 *_meshTangents)
{
	polygons = _polygons;
	numPolygons = _numPolygons;
	meshPositions = _meshPositions;
	meshUVs = _meshUVs;
	meshVNormals = _meshVNormals;
	meshFNormals = _meshFNormals;
	meshTangents = _meshTangents;
}


bool DeviceBoundingBox::pointInside(vec3 point) const
{
	vec3 min = boundsMin;
	vec3 max = boundsMax;

	if(point.x <= max.x && point.x >= min.x &&
	   point.y <= max.y && point.y >= min.y &&
	   point.z <= max.z && point.z >= min.z)
	{
		return true;
	}
	else
	{
		return false;
	}
}


bool DeviceBoundingBox::boundIntersection(vec3 origin, vec3 ray, Intersection &intersection) const
{
	// Intitalize Te = 0, Ts = INF
	float te = 0.0f;
	float ts = INF;
	float t = 0.0f;

	float RdotN;
	vec3 normal,N;

	if(pointInside(origin))
	{
		return true;
	}

	for(int i=0; i<24; i+=4)
	{
		// Calculate R�N and t (distance)
		N = fnormals[i/4];
		RdotN = dot(ray,N);
		t = dot(N, positions[elements[i]] - origin) / RdotN;

		if(RdotN == 0)
		{
			if(t < 0.0f)
			{
				return false;
			}
		}
		else
		{
			if(RdotN < 0)	// IN
			{
				te = max(te,t);
				if(te == t)
				{
					normal = N;
				}
			}
			if(RdotN > 0)	// OUT
			{
				ts = min(ts,t);
			}
		}
	}

	if(te <= ts)
	{
		intersection.distance = te;
		intersection.normal = normal;
		return true;
	}
	else
	{
		return false;
	}
}


bool DeviceBoundingBox::rayIntersection(vec3 origin, vec3 ray, Intersection &intersection) const
{
	if(numChildren == 0)	// this node is a leaf
	{
		bool	bIntersection = false;
		int		returnPolygon=-1;
		float	t2, t=INF, areaA, areaB;
		float	NdotR;
		vec3	pos1, pos2 ,pos3;
		vec2	tc1, tc2, tc3;
		vec3	vn1, vn2, vn3;
		vec4	tan1, tan2, tan3;
		vec3	fnormal;
		vec3	point, returnPoint;
		vec4	returnTangent;

		// Obtain the distance and the polygon (if any)
		for(int e=0; e<numPolygons; e+=3)
		{
			//currentPolygon = polygons[e];
			pos1 = meshPositions[polygons[e]];
			pos2 = meshPositions[polygons[e+1]];
			pos3 = meshPositions[polygons[e+2]];
			fnormal = normalize(cross(pos2-pos1, pos3-pos2));
			NdotR = dot(fnormal,ray);

			if(NdotR <= 0.0)
			{
				t2 = dot(fnormal, (pos1-origin)) / NdotR;
				point = origin + ray*t2;

				areaA = dot(cross((pos3-pos1),(point-pos1)), cross((pos2-pos3),(point-pos3)));
				areaB = dot(cross((pos3-pos1),(point-pos1)), cross((pos1-pos2),(point-pos1)));

				if( areaA >= 0 && areaB >= 0 )
				{
					if(t > t2 && t2 >= 0.0)
					{
						returnPolygon = e;
						bIntersection = true;
						t = t2;
						returnPoint = point;
					}
				}
			}
		}

		// Intersection with bound but not with any of its polygons
		if(bIntersection == false)
		{
			return false;
		}

		// Intersection with bound and with a polygon
		else
		{
			pos1 = meshPositions[polygons[returnPolygon]];
			pos2 = meshPositions[polygons[returnPolygon+1]];
			pos3 = meshPositions[polygons[returnPolygon+2]];

			tc1 = meshUVs[polygons[returnPolygon]];
			tc2 = meshUVs[polygons[returnPolygon+1]];
			tc3 = meshUVs[polygons[returnPolygon+2]];

			vn1 = meshVNormals[polygons[returnPolygon]];
			vn2 = meshVNormals[polygons[returnPolygon+1]];
			vn3 = meshVNormals[polygons[returnPolygon+2]];

			tan1 = meshTangents[polygons[returnPolygon]];
			tan2 = meshTangents[polygons[returnPolygon+1]];
			tan3 = meshTangents[polygons[returnPolygon+2]];

			vec3 AP = returnPoint - pos1;
			vec3 BP = returnPoint - pos2;
			vec3 AC = pos3 - pos1;
			vec3 AB = pos2 - pos1;
			vec3 BC = pos3 - pos2;

			float triangleArea = module(cross(AB,AC));
			float u = module(cross(AP,AC)) / triangleArea;
			float v = module(cross(AP,AB)) / triangleArea;
			float w = module(cross(BP,BC)) / triangleArea;

			//Interpolation of UVs
			intersection.uv = (tc1*w) + (tc2*u) + (tc3*v);

			// Interpolation of normals
			intersection.normal = (vn1*w) + (vn2*u) + (vn3*v);

			// Interpolatio of tangents
			returnTangent = (tan1*w) + (tan2*u) + (tan3*v);
			intersection.tangent = vec3(returnTangent.x, returnTangent.y, returnTangent.z);

			// Fill the intersection
			intersection.distance = t;
			intersection.point = returnPoint;
			
			return true;
		}

	}
	else
	{

		Intersection current, minimum;
		bool bin = false;

		for(int i=0; i<numChildren; i++)
		{
			if(boundsArray[children[i]].boundIntersection(origin, ray, current))
			{
				if(boundsArray[children[i]].rayIntersection(origin, ray, current))
				{
					if(current < minimum)
					{
						minimum = current;
						bin = true;
					}
				}
			}
		}

		if(bin)
		{
			intersection = minimum;
			return true;
		}

		return false;
	}
}

#endif // _DEVICE_BOUNDING_BOX_