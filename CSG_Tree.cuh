#ifndef _CSG_TREE_
#define _CSG_TREE_

#include "CSG_Sphere.cuh"
#include "CSG_Box.cuh"
#include "CSG_Cylinder.cuh"

struct CSG_Node
{
	int type;			// Type of the current node (operation or primitive)
	int index;			// Index of the element in its array
	int child_a;		// Left children in the lower level (if any)
	int child_b;		// Right children in the lower level (if any)

	CSG_Node():type(-1),index(-1),child_a(-1),child_b(-1){};

	CSG_Node(int _type, int _index, int _child_a, int _child_b):type(_type), index(_index), child_a(_child_a), child_b(_child_b){};
};

class CSG_Tree
{
private:

	__device__ __host__ bool operationUnion(float3 origin, float3 ray,
											Intersection &Ain, Intersection &Aout,
											Intersection &Bin, Intersection &Bout,
											Intersection &in, Intersection &out);

	__device__ __host__ bool operationDifference(float3 origin, float3 ray,
											Intersection &Ain, Intersection &Aout,
											Intersection &Bin, Intersection &Bout,
											Intersection &in, Intersection &out);

	__device__ __host__ bool operationIntersection(float3 origin, float3 ray,
											Intersection &Ain, Intersection &Aout,
											Intersection &Bin, Intersection &Bout,
											Intersection &in, Intersection &out);

public:

	CSG_Node		**levels;		// The tree itself
	CSG_Sphere		*spheres;		// Array of spheres
	CSG_Box			*boxes;			// Array of boxes
	CSG_Cylinder	*cylinders;		// Array of cylinders

	int nLevels;					// Number of levels
	int *nNodesPerLevel;			// Number of nodes on each level
	
	__device__ __host__ CSG_Tree();

	__device__ __host__ bool rayIntersection(float3 origin, float3 ray, Intersection &intersection);
};


//////////////////////////////////////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////////////////////////////////////


CSG_Tree::CSG_Tree()
{

}


bool CSG_Tree::rayIntersection(float3 origin, float3 ray, Intersection &intersection)
{
	Intersection dummy = Intersection();
	return cylinders[0].rayIntersection(origin,ray,intersection,dummy);

	/*Intersection *previousIn;//	= (Intersection*)malloc(sizeof(Intersection));
	Intersection *previousOut;//	= (Intersection*)malloc(sizeof(Intersection));
	Intersection *currentIn;
	Intersection *currentOut;

	cudaMalloc((void**)&previousIn,sizeof(Intersection));
	cudaMalloc((void**)&previousOut,sizeof(Intersection));

	for(int i=nLevels-1; i>=0; i--)
	{
		// Allocate space for current level intersections
		//currentIn	= (Intersection*)malloc(sizeof(Intersection)*nNodesPerLevel[i]);
		//currentOut	= (Intersection*)malloc(sizeof(Intersection)*nNodesPerLevel[i]);

		cudaMalloc((void**)&currentIn, sizeof(Intersection)*nNodesPerLevel[i]);
		cudaMalloc((void**)&currentOut, sizeof(Intersection)*nNodesPerLevel[i]);

		// Calculate intersections
		for(int j=0; j<nNodesPerLevel[i]; j++)
		{
			currentIn[j]  = Intersection();
			currentOut[j] = Intersection();

			switch(levels[i][j].type)
			{
			case OBJECT_TYPE_CSG_UNION:
				operationUnion(origin, ray, previousIn [levels[i][j].child_a],
											previousOut[levels[i][j].child_a],
											previousIn [levels[i][j].child_b],
											previousOut[levels[i][j].child_b],
											currentIn[j], currentOut[j]); break;

			case OBJECT_TYPE_CSG_DIFFERENCE:
				operationDifference(origin, ray, previousIn [levels[i][j].child_a],
												 previousOut[levels[i][j].child_a],
												 previousIn [levels[i][j].child_b],
												 previousOut[levels[i][j].child_b],
												 currentIn[j], currentOut[j]); break;

			case OBJECT_TYPE_CSG_INTERSECTION:
				operationIntersection(origin, ray, previousIn [levels[i][j].child_a],
												   previousOut[levels[i][j].child_a],
												   previousIn [levels[i][j].child_b],
												   previousOut[levels[i][j].child_b],
												   currentIn[j], currentOut[j]); break;

			case OBJECT_TYPE_CSG_SPHERE:
				spheres[levels[i][j].index].rayIntersection(origin, ray, currentIn[j], currentOut[j]); break;

			case OBJECT_TYPE_CSG_BOX:
				boxes[levels[i][j].index].rayIntersection(origin, ray, currentIn[j], currentOut[j]);break;

			case OBJECT_TYPE_CSG_CYLINDER:
				cylinders[levels[i][j].index].rayIntersection(origin, ray, currentIn[j], currentOut[j]); break;
			}
		}

		// Free the previous array
		cudaFree(previousIn);
		cudaFree(previousOut);

		// Move current to previous
		previousIn	= currentIn;
		previousOut = currentOut;

	}

	intersection = currentIn[0];
	return true;

	if(currentIn[0].distance < INFINITE)
	{
		intersection = currentIn[0];
		//intersection.distance = nLevels*4;
		return true;
	}
	else
	{
		return false;
	}*/
}


bool CSG_Tree::operationUnion(float3 origin, float3 ray,
							Intersection &Ain, Intersection &Aout,
							Intersection &Bin, Intersection &Bout,
							Intersection &in, Intersection &out)
{
	return false;
}


bool CSG_Tree::operationDifference(float3 origin, float3 ray,
							Intersection &Ain, Intersection &Aout,
							Intersection &Bin, Intersection &Bout,
							Intersection &in, Intersection &out)
{
	Intersection inte = Intersection();
	inte.distance = 20;
	in = inte;
	return true;

	if(Ain.distance == INF)
	{
		return false;
	}
	else if(Ain.distance != INF && Bin.distance == INF)
	{
		in = Ain;
		out = Aout;
		return true;
	}
	else
	{
		if(Bin < Ain && Ain < Bout && Bout < Aout)
		{
			Bout.normal = negate(Bout.normal);
			in = Bout;
			out = Aout;
			return true;
		}
		else if(Ain < Bin)
		{
			in = Ain;
			if(Bout < Aout || Aout < Bin)
			{
				out = Aout;
			}
			else
			{
				Bin.normal = negate(Bin.normal);
				out = Bin;
			}
			return true;
		}
		else if(Bout < Ain)
		{
			in = Ain;
			out = Aout;
			return true;
		}
		else
		{
			return false;
		}
	}
}


bool CSG_Tree::operationIntersection(float3 origin, float3 ray,
							Intersection &Ain, Intersection &Aout,
							Intersection &Bin, Intersection &Bout,
							Intersection &in, Intersection &out)
{
	return false;
}


#endif // _CSG_TREE_