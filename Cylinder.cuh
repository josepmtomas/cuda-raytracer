#ifndef _CYLINDER_PRIMITIVE_
#define _CYLINDER_PRIMITIVE_

#include "Intersection.cuh"

class Cylinder
{
protected:

	int  index;
	vec4 bottom;			// Equation that defines the top end-cap plane
	vec4 top;				// Equation that defines the bottom end-cap plane

public:

	typedef enum CylinderSurface { SIDE, BOTTOM, TOP };

	vec3	base;			// The base location of the cylinder.
	vec3	axis;			// The axis of symetry for the cylinder. (unitary)
	float	radius;			// The radius of the cylinder.
	float	height;			// The height of the cylinder.
	int		material;		// Material of the cylinder.

	__device__ __host__ Cylinder();
	__device__ __host__ Cylinder(vec3 _base, vec3 _axis, float _radius, float _height);

	__device__ __host__ void buildPlanes();
	__device__ __host__ void setIndex(int _index);
	__device__ __host__ void setMaterial(int _material);
	__device__ __host__ bool rayIntersection(vec3 origin, vec3 ray, Intersection &intersection);
	__device__ __host__ bool rayIntersectionOut(vec3 origin, vec3 ray, Intersection &intersection);

};


//////////////////////////////////////////////////////////////////////////////////////////////////////
// Cylinder implementation
//////////////////////////////////////////////////////////////////////////////////////////////////////


inline Cylinder::Cylinder()
{
	base = vec3(0,0,0);
	axis = vec3(0,1,0);
	radius = 1.0f;
	height = 1.0f;

	buildPlanes();
}


inline Cylinder::Cylinder(vec3 _base, vec3 _axis, float _radius, float _height)
{
	base = _base;
	axis = normalize(_axis);
	radius = _radius;
	height = _height;

	buildPlanes();
}


inline void Cylinder::buildPlanes()
{
	// Bottom plane

	vec3 point = base;
	vec3 normal = negate(axis);
	float d;

	d = normal.x*point.x + normal.y*point.y + normal.z*point.z;
	bottom = vec4(normal.x, normal.y, normal.z, -d);

	// Top plane

	point = base + axis*height;
	normal = axis;

	d = normal.x*point.x + normal.y*point.y + normal.z*point.z;
	top = vec4(normal.x, normal.y, normal.z, -d);
}

inline void Cylinder::setIndex(int _index)
{
	index = _index;
}

inline void Cylinder::setMaterial(int _material)
{
	material = _material;
}


//  (Graphics Gems 4)
//	Intersecting a Ray with a Cylinder
//		- Joseph M. Cychosz
//		- Warren N. Waggenspack, Jr.

inline bool Cylinder::rayIntersection(vec3 origin, vec3 ray, Intersection &intersection)
{
	vec3	RC;				// Ray base to cylinder base
	float	d;				// Shortest distance between the ray and the cylinder
	float	t,s;			// Distances along the ray
	vec3	n,D,O;
	float	ln;
	float	in;				// Entering distance
	float	out;			// Leaving distance
	bool	hit = false;
	int		surfin;			// Entering surface identifier
	//int		surfout;		// Exiting surface identifier

	float	dc,dw;
	//float	in,out;			// Object intersection distances

	RC = origin - base;
	n = cross(ray,axis);
	ln = module(n);
	
	if(ln == 0.0f)								// ray parallel to cylinder
	{
		d	= dot(RC,axis);
		D	= RC - d*axis;
		d	= module(D);
		in	= -INF;
		out = INF;
		hit = (d <= radius);					// TRUE if ray is in cyl
	}
	else
	{
		n = normalize(n);
		d = fabs(dot(RC,n));					// Shortest distance
		hit = (d <= radius);

		if(hit)									// If ray hits cylinder
		{
			O	= cross(RC,axis);
			t	= - dot(O,n) / ln;
			O	= cross(n,axis);
			O	= normalize(O);
			s	= fabs(sqrt(radius*radius - d*d)) / dot(ray,O);
			in	= t + s;
			out = t - s;
		}
	}

	if(!hit)
	{
		return false;
	}
	else
	{
		//function getSpPoint(A,B,C){
		//	var x1=A.x, y1=A.y, x2=B.x, y2=B.y, x3=C.x, y3=C.y;
		//	var px = x2-x1, py = y2-y1, dAB = px*px + py*py;
		//	var u = ((x3 - x1) * px + (y3 - y1) * py) / dAB;
		//	var x = x1 + u * px, y = y1 + u * py;
		//	return {x:x, y:y}; //this is D
		//}

		/*float3 A = base;
		float3 B = base + axis*height;
		float3 C = origin + ray*t;
		 
		float dAB = powf(B.x-A.x,2) + powf(B.y-A.y,2) + powf(B.z-A.z,2);
		float u = (((C.x-A.x)*(B.x-A.x))+((C.y-A.y)*(B.y-A.y))+((C.z-A.z)*(B.z-A.z)))/dAB;
		float3 D = A + u*(B-A);

		intersection.distance	= in;
		intersection.point		= origin + ray*in;
		intersection.normal		= normalize(intersection.point-D);
		return true;*/

		//surfin = surfout = CylinderSurface::SIDE;
		surfin = CylinderSurface::SIDE;
		
		// Intersect the ray with the bottom end-cap plane
		
		dc = bottom.x*ray.x + bottom.y*ray.y + bottom.z*ray.z;
		dw = bottom.x*origin.x + bottom.y*origin.y + bottom.z*origin.z + bottom.w;

		if(dc == 0)			// If parallel to bottom plane
		{
			if(dw >= 0.0f) return false;
		}
		else
		{
			t = -dw/dc;
			if(dc >= 0.0f)	// If far plane
			{
				if( t > in && t < out )
				{
					out = t;
					//surfout = CylinderSurface::BOTTOM;
				}
				if(t < in) return false;
			}
			else			// If near plane
			{
				if( t > in && t < out )
				{
					in = t;
					surfin = CylinderSurface::BOTTOM;
				}
				if(t > out) return false;
			}
		}

		// Intersect the ray with the top end-cap plane

		dc = top.x*ray.x + top.y*ray.y + top.z*ray.z;
		dw = top.x*origin.x + top.y*origin.y + top.z*origin.z + top.w;

		if(dc == 0)			// If parallel to top plane
		{
			if(dw >= 0.0f) return false;
		}
		else
		{
			t = -dw/dc;
			if(dc >= 0.0f)	// If far plane
			{
				if( t > in && t < out )
				{
					out = t;
					//surfout = CylinderSurface::TOP;
				}
				if(t < in) return false;
			}
			else			// If near plane
			{
				if( t > in && t < out )
				{
					in = t;
					surfin = CylinderSurface::TOP;
				}
				if(t > out) return false;
			}
		}

		if(in < out && in > 0.0 && out > 0.0)
		{
			vec3 C,D;
			float u;

			intersection.distance	= in;
			intersection.point		= origin + ray*in;
			
			switch(surfin)
			{
			case CylinderSurface::BOTTOM:
				intersection.normal = negate(axis);
				intersection.tangent = cross(vec3(0,1,0), axis);
				break;
			case CylinderSurface::TOP:
				intersection.normal = axis;
				intersection.tangent = cross(vec3(0,1,0), axis);
				break;
			case CylinderSurface::SIDE:
				//A = base;
				//B = base + axis*height;
				C = origin + ray*t;
		 
				//dAB = powf(B.x-A.x,2) + powf(B.y-A.y,2) + powf(B.z-A.z,2);
				//u = (((C.x-A.x)*(B.x-A.x))+((C.y-A.y)*(B.y-A.y))+((C.z-A.z)*(B.z-A.z)))/dAB;
				//t=[(Cx-Ax)(Bx-Ax)+(Cy-Ay)(By-Ay)]/[(Bx-Ax)^2+(By-Ay)^2]
				//u = dot(C-A,B-A)/dot(B-A,B-A);
				//D = A + u*(B-A);

				/*public Real2 getNearestPointOnLine(Real2 point) {
    				unitVector = to.subtract(from).getUnitVector();
    				Vector2 lp = new Vector2(point.subtract(this.from));
    				double lambda = unitVector.dotProduct(lp);
    				Real2 vv = unitVector.multiplyBy(lambda);
    				return from.plus(vv);
				}*/

				C = intersection.point - base;
				u = dot(axis,C);
				D = base + axis*u;

				intersection.normal		= normalize(intersection.point-D);
				intersection.tangent	= cross(intersection.normal, axis);

				break;
			default:
				break;
			}

			intersection.material = material;
			intersection.objectType = OBJECT_TYPE_CYLINDER;

			return true;
		}
		else
			return false;
	}
}


inline bool Cylinder::rayIntersectionOut(vec3 origin, vec3 ray, Intersection &intersection)
{
	vec3	RC;				// Ray base to cylinder base
	float	d;				// Shortest distance between the ray and the cylinder
	float	t,s;			// Distances along the ray
	vec3	n,D,O;
	float	ln;
	float	in;				// Entering distance
	float	out;			// Leaving distance
	bool	hit = false;
	//int		surfin;			// Entering surface identifier
	int		surfout;		// Exiting surface identifier

	float	dc,dw;
	//float	in,out;			// Object intersection distances

	RC = origin - base;
	n = cross(ray,axis);
	ln = module(n);
	
	if(ln == 0.0f)								// ray parallel to cylinder
	{
		d	= dot(RC,axis);
		D	= RC - d*axis;
		d	= module(D);
		in	= -INF;
		out = INF;
		hit = (d <= radius);					// TRUE if ray is in cyl
	}
	else
	{
		n = normalize(n);
		d = fabs(dot(RC,n));					// Shortest distance
		hit = (d <= radius);

		if(hit)									// If ray hits cylinder
		{
			O	= cross(RC,axis);
			t	= - dot(O,n) / ln;
			O	= cross(n,axis);
			O	= normalize(O);
			s	= fabs(sqrt(radius*radius - d*d)) / dot(ray,O);
			in	= t + s;
			out = t - s;
		}
	}

	if(!hit)
	{
		return false;
	}
	else
	{
		//surfin = surfout = CylinderSurface::SIDE;
		//surfin = CylinderSurface::SIDE;
		surfout = CylinderSurface::SIDE;
		
		// Intersect the ray with the bottom end-cap plane
		
		dc = bottom.x*ray.x + bottom.y*ray.y + bottom.z*ray.z;
		dw = bottom.x*origin.x + bottom.y*origin.y + bottom.z*origin.z + bottom.w;

		if(dc == 0)			// If parallel to bottom plane
		{
			if(dw >= 0.0f) return false;
		}
		else
		{
			t = -dw/dc;
			if(dc >= 0.0f)	// If far plane
			{
				if( t > in && t < out )
				{
					out = t;
					surfout = CylinderSurface::BOTTOM;
				}
				if(t < in) return false;
			}
			else			// If near plane
			{
				if( t > in && t < out )
				{
					in = t;
					//surfin = CylinderSurface::BOTTOM;
				}
				if(t > out) return false;
			}
		}

		// Intersect the ray with the top end-cap plane

		dc = top.x*ray.x + top.y*ray.y + top.z*ray.z;
		dw = top.x*origin.x + top.y*origin.y + top.z*origin.z + top.w;

		if(dc == 0)			// If parallel to top plane
		{
			if(dw >= 0.0f) return false;
		}
		else
		{
			t = -dw/dc;
			if(dc >= 0.0f)	// If far plane
			{
				if( t > in && t < out )
				{
					out = t;
					surfout = CylinderSurface::TOP;
				}
				if(t < in) return false;
			}
			else			// If near plane
			{
				if( t > in && t < out )
				{
					in = t;
					//surfin = CylinderSurface::TOP;
				}
				if(t > out) return false;
			}
		}

		if(in < out)
		{
			vec3 C,D;
			float u;

			intersection.distance	= out;
			intersection.point		= origin + ray*out;
			
			switch(surfout)
			{
			case CylinderSurface::BOTTOM:
				intersection.normal = negate(axis);
				intersection.tangent = cross(vec3(0,1,0), axis);
				break;
			case CylinderSurface::TOP:
				intersection.normal = axis;
				intersection.tangent = cross(vec3(0,1,0), axis);
				break;
			case CylinderSurface::SIDE:
				//A = base;
				//B = base + axis*height;
				C = origin + ray*t;
		 
				//dAB = powf(B.x-A.x,2) + powf(B.y-A.y,2) + powf(B.z-A.z,2);
				//u = (((C.x-A.x)*(B.x-A.x))+((C.y-A.y)*(B.y-A.y))+((C.z-A.z)*(B.z-A.z)))/dAB;
				//t=[(Cx-Ax)(Bx-Ax)+(Cy-Ay)(By-Ay)]/[(Bx-Ax)^2+(By-Ay)^2]
				//u = dot(C-A,B-A)/dot(B-A,B-A);
				//D = A + u*(B-A);

				/*public Real2 getNearestPointOnLine(Real2 point) {
    				unitVector = to.subtract(from).getUnitVector();
    				Vector2 lp = new Vector2(point.subtract(this.from));
    				double lambda = unitVector.dotProduct(lp);
    				Real2 vv = unitVector.multiplyBy(lambda);
    				return from.plus(vv);
				}*/

				C = intersection.point - base;
				u = dot(axis,C);
				D = base + axis*u;

				intersection.normal		= normalize(intersection.point-D);
				intersection.tangent	= cross(intersection.normal, axis);

				break;
			default:
				break;
			}

			intersection.material = material;
			intersection.objectType = OBJECT_TYPE_CYLINDER;

			return true;
		}
		else
			return false;
	}

	
}


#endif	// _CYLINDER_PRIMITIVE_