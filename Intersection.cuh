#ifndef _INTERSECTION_
#define _INTERSECTION_

#include "Material.cuh"

class Intersection
{
public:

	int type;
	int objectType;
	int objectIndex;
	int material;
	float distance;
	vec3 point;
	vec3 normal;
	vec3 tangent;
	vec2 uv;
	
	__device__ __host__ Intersection():type(INTERSECTION_NONE),objectType(-1),objectIndex(-1),distance(INF){};

	__device__ __host__ bool operator <  (const Intersection& i) const {return distance <  i.distance;};
	__device__ __host__ bool operator <= (const Intersection& i) const {return distance <= i.distance;};
	__device__ __host__ bool operator >  (const Intersection& i) const {return distance >  i.distance;};
	__device__ __host__ bool operator >= (const Intersection& i) const {return distance >= i.distance;};

};

/*inline __device__ __host__ Intersection min(const Intersection &a, const Intersection &b)
{
	if(a<b)	return a;
	else	return b;
}

inline __device__ __host__ Intersection max(const Intersection &a, const Intersection &b)
{
	if(a>b)	return a;
	else	return b;
}*/


#endif // _INTERSECTION_