#ifndef _PERSPECTIVE_CAMERA_
#define _PERSPECTIVE_CAMERA_

#include "Algebra.cuh"

class PerspectiveCamera
{
//protected:

	//vec3 u,v,w;

public:
	
	vec3	u,v,w;
	vec3	pov;				// Point Of View
	vec3	poi;				// Point Of Intersest
	float	fov;				// Field Of View

	__device__ __host__ PerspectiveCamera();
	__device__ __host__ PerspectiveCamera(vec3 _pov, vec3 _poi, float _fov);

	__device__ __host__ vec3 visualPixel(float i, float j, int width, int height);
};

#endif // _PERSPECTIVE_CAMERA_


//////////////////////////////////////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////////////////////////////////////


inline PerspectiveCamera::PerspectiveCamera()
{
	pov = vec3(0,1,1);
	poi = vec3(0,0,0);
	fov = 60.0f;

	u = vec3(0,0,0);
	v = u;
	w = u;
}


inline PerspectiveCamera::PerspectiveCamera(vec3 _pov, vec3 _poi, float _fov)
{
	pov = _pov;
	poi = _poi;
	fov = _fov;

	w = normalize(negate(poi-pov));
	u = normalize(cross(vec3(0,1,0),w));
	v = cross(w,u);
}


inline vec3 PerspectiveCamera::visualPixel(float i, float j, int width, int height)
{
	float a,b,d,x,y;

	d = sqrtf(powf(pov.x-poi.x,2) + powf(pov.y-poi.y,2) + powf(pov.z-poi.z,2));

	b = 2*tan((fov/2)*PI/180)*d;
	a = ((float)width/(float)height)*b;

	x =  ((a/width)*(i+0.5))-(a/2);
	y = ((b/height)*(j+0.5))-(b/2);

	return normalize((poi-pov) + u*x + v*y);
}