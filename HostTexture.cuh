#ifndef _HOST_TEXTURE_
#define _HOST_TEXTURE_

#include "Algebra.cuh"
#include <vector>
#include <FreeImage.h>


class HostTexture
{
public:

	__host__ HostTexture();
	__host__ ~HostTexture();

	FIBITMAP	*dib;
	BYTE		*bits;
	int			width, height;
	int			imageSize;
	bool		valid;

	std::vector<vec3> data;

	__host__ bool loadTexture(const char *filename);
};

//

HostTexture::HostTexture()
{
	#ifdef FREEIMAGE_LIB
		FreeImage_Initialise();
	#endif
}

HostTexture::~HostTexture()
{
	#ifdef FREEIMAGE_LIB
		FreeImage_DeInitialise();
	#endif
}

bool HostTexture::loadTexture(const char *filename)
{
	FREE_IMAGE_FORMAT	fif = FIF_UNKNOWN;	// Image Format
	unsigned int		bpp, channels;		// Bits Per Pixel
	float				r,g,b;	// Channel components
	vec3				tempPixel;			

	// Check the file signature and deduce its format
	fif = FreeImage_GetFileType(filename,0);
	// If still unknown, try to guess the file format from the file extension
	if(fif == FIF_UNKNOWN)
		fif = FreeImage_GetFIFFromFilename(filename);
	// Is still UNKNOWN, return failure
	if(fif == FIF_UNKNOWN)
	{
		printf("Couldn't get the file format.\n");
		return false;
	}

	// Check that the plugin has reading capabilities and load the file
	if(FreeImage_FIFSupportsReading(fif))
	{
		dib = FreeImage_Load(fif, filename);
	}
	else
	{
		printf("No support for reading.\n");
	}

	// If the image failed to load, return failure
	if(!dib)
	{
		printf("Image failed to load");
		return false;
	}

	// Retrieve the image data
	bits = FreeImage_GetBits(dib);

	// Get the image width and height
	width = FreeImage_GetWidth(dib);
	height = FreeImage_GetHeight(dib);

	//if this somehow one of these failed (they shouldn't), return failure
	if((bits == 0) || (width == 0) || (height == 0))
		return false;

	// Get the data into our vector
	bpp = FreeImage_GetBPP(dib);
	channels = bpp/8;
	imageSize = width*height*channels;

	//printf("BPP: %d\n", bpp);

	for(int i=0; i<imageSize; i+=channels)
	{
		b = (float)bits[i]/255;
		g = (float)bits[i+1]/255;
		r = (float)bits[i+2]/255;

		if(channels == 3)
		{
			//a = 1.0;
			//tempPixel = vec4(r,g,b,a);
			tempPixel = vec3(r,g,b);
		}
		else
		{
			//a = (float)bits[i+3]/255;
			//tempPixel = vec4(r,g,b,a);
			tempPixel = vec3(r,g,b);
		}

		data.push_back(tempPixel);
	}

	FreeImage_Unload(dib);

	//this->tiling = vec2(1.0f);

	return true;
}



#endif //_HOST_TEXTURE_