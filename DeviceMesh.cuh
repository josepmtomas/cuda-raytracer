#ifndef _DEVICE_MESH_
#define _DEVICE_MESH_

#include "Algebra.cuh"
#include "DeviceBoundingBox.cuh"

class DeviceMesh
{
protected:

	int		*elements;
	vec3	*positions;
	vec2	*uvs;
	vec3	*vnormals;
	vec3	*fnormals;
	vec4	*tangents;

	int		numElements;
	int		numPositions;
	int		numUVs;
	int		numVNormals;
	int		numFNormals;
	int		numTangents;

	int		material;

	DeviceBoundingBox *boundingBoxes;

public:

	__host__ __device__ DeviceMesh();
	
	__host__ __device__ void setElements(int *_elements, int _numElements);
	__host__ __device__ void setPositions(vec3 *_positions, int _numPositions);
	__host__ __device__ void setUVs(vec2 *_uvs, int _numUVs);
	__host__ __device__ void setVertexNormals(vec3 *_vnormals, int _numVNormals);
	__host__ __device__ void setFaceNormals(vec3 *_fnormals, int _numFNormals);
	__host__ __device__ void setTangents(vec4 *_tangents, int _numTangents);

	__host__ __device__ void setMaterial(int _material);

	__host__ __device__ void setBoundingBoxes(DeviceBoundingBox *_boundingBoxes);

	__host__ __device__ bool rayIntersection(vec3 origin, vec3 ray, Intersection &intersection);
};

//////////////////////////////////////////////////////////////////////////////////////////////

DeviceMesh::DeviceMesh()
{};


void DeviceMesh::setElements(int *_elements, int _numElements)
{
	elements = _elements;
	numElements = _numElements;
}


void DeviceMesh::setPositions(vec3 *_positions, int _numPositions)
{
	positions = _positions;
	numPositions = _numPositions;
}


void DeviceMesh::setUVs(vec2 *_uvs, int _numUVs)
{
	uvs = _uvs;
	numUVs = _numUVs;
}


void DeviceMesh::setVertexNormals(vec3 *_vnormals, int _numVNormals)
{
	vnormals = _vnormals;
	numVNormals = _numVNormals;
}


void DeviceMesh::setFaceNormals(vec3 *_fnormals, int _numFNormals)
{
	fnormals = _fnormals;
	numFNormals = _numFNormals;
}


void DeviceMesh::setTangents(vec4 *_tangents, int _numTangents)
{
	tangents = _tangents;
	numTangents = _numTangents;
}


void DeviceMesh::setMaterial(int _material)
{
	material = _material;
}


void DeviceMesh::setBoundingBoxes(DeviceBoundingBox *_boundingBoxes)
{
	boundingBoxes = _boundingBoxes;
}


bool DeviceMesh::rayIntersection(vec3 origin, vec3 ray, Intersection &intersection)
{
	if(boundingBoxes[0].boundIntersection(origin, ray, intersection))
	{
		if(boundingBoxes[0].rayIntersection(origin, ray, intersection))
		{
			intersection.objectType = OBJECT_TYPE_MESH;
			intersection.material = material;
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}


#endif _DEVICE_MESH_