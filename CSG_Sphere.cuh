#ifndef _CSG_SPHERE_
#define _CSG_SPHERE_

#include "Intersection.cuh"

class CSG_Sphere
{
protected:

	int type;
	
	

public:

	int index;
	vec3	center;
	float	radius;
	
	__device__ __host__ CSG_Sphere();
	__device__ __host__ CSG_Sphere(vec3 _center, float _radius);

	__device__ __host__ void translation(vec3 distance);
	__device__ __host__ void rotation(vec3 axis, float angle);
	__device__ __host__ void scale(vec3 amount);

	__device__ __host__ bool rayIntersection(vec3 origin, vec3 ray, Intersection &inIntersection, Intersection &outIntersection);
};

#endif // _CSG_SPHERE_


//////////////////////////////////////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////////////////////////////////////


CSG_Sphere::CSG_Sphere()
{
	type = OBJECT_TYPE_CSG_SPHERE;
	index = 0;
	center = vec3(0,0,0);
	radius = 1.0f;
}


CSG_Sphere::CSG_Sphere(vec3 _center, float _radius)
{
	type = OBJECT_TYPE_CSG_SPHERE;
	index = 0;
	center = _center;
	radius = _radius;
}


void CSG_Sphere::translation(vec3 distance)
{
	center = center + distance;
}


void CSG_Sphere::rotation(vec3 axis, float angle)
{
	center = rotate(center, axis, angle);
}


void CSG_Sphere::scale(vec3 amount)
{
	center = center * amount;
	radius = radius * amount.x;
}


bool CSG_Sphere::rayIntersection(vec3 origin, vec3 ray, Intersection &inIntersection, Intersection &outIntersection)
{
	float alpha, beta, gamma;
	float root;
	float t1,t2;
	vec3 pcenter = origin - center;

	alpha = dot(ray,ray);
	beta = 2 * dot(pcenter,ray);
	gamma = dot(pcenter,pcenter) - (radius*radius);

	root = beta*beta - 4*alpha*gamma;

	if(root < 0.0f)
	{
		inIntersection.type = INTERSECTION_NONE;
		outIntersection.type = INTERSECTION_NONE;
		return false;
	}

	root = sqrtf(root);
	t2 = (-beta+root)/(2*alpha);
	t1 = (-beta-root)/(2*alpha);

	if(t1<=0)
	{
		inIntersection.type = INTERSECTION_NONE;
		outIntersection.type = INTERSECTION_NONE;
		return false;
	}

	inIntersection.type				= INTERSECTION_IN;
	inIntersection.objectType		= type;
	inIntersection.objectIndex		= index;
	inIntersection.distance			= t1;
	inIntersection.point			= origin + ray*t1;
	inIntersection.normal			= normalize(inIntersection.point - center);

	outIntersection.type			= INTERSECTION_OUT;
	outIntersection.objectType		= type;
	outIntersection.objectIndex		= index;
	outIntersection.distance		= t2;
	outIntersection.point			= origin + ray*t2;
	outIntersection.normal			= normalize(outIntersection.point - center);

	return true;
}