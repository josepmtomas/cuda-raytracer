#ifndef _DEVICE_TEXTURE_
#define _DEVICE_TEXTURE_

#include "Algebra.cuh"

class DeviceTexture
{
public:

	vec3	*data;
	int		width;
	int		height;

	__host__ __device__ DeviceTexture();

	__host__ __device__ void setTexture(vec3 *_data, int _width, int _height);
	__host__ __device__ vec3 at(vec2 uvs);
};

//

DeviceTexture::DeviceTexture()
{

}


void DeviceTexture::setTexture(vec3 *_data, int _width, int _height)
{
	data = _data;
	width = _width;
	height = _height;
}


vec3 DeviceTexture::at(vec2 uvs)
{
	float fx = fmod(uvs.x, 1.0f) * ((float)width-1);
	float fy = fmod(uvs.y, 1.0f) * ((float)height-1);
	int x = (int)fx;
	int y = (int)fy;
	int position = y*width+x;

	if(position >= width*height*3)
	{
		return vec3(0.0);
	}
	else
	{
		return data[position];
	}
}

#endif // _DEVICE_TEXTURE_