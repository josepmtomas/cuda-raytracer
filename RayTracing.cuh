#ifndef _RAY_TRACING_FUNCTIONS_
#define _RAY_TRACING_FUNCTIONS_

/*#include "Box.cuh"
#include "Sphere.cuh"
#include "Cylinder.cuh"*/

#include "Scene.cuh"

__device__ vec3 rayTrace(vec3 origin, vec3 ray, DeviceScene *scene, curandState *state, vec3 backgroundColor, int reflectionQuality, int indirectQuality, u_int2 steps, float colorBleeding)
{
	Intersection minimum;
	Intersection indirectIntersection;
	Intersection transmissionMinimum = Intersection();

	// Reflection stuff
	vec3 reflectionVector;
	vec3 direction1, direction2;
	float step_u, step_v;
	float rand;
	//int steps_u = 3;
	//int steps_v = 3;

	// Transmission
	vec3 transmissionIn;
	vec3 transmissionOut;
	int  result;

	// Common
	//float hitCount;

	// Colors
	vec3 tempColor = vec3(0.0);
	vec3 output = vec3(0.0);
	vec3 indirectColor = vec3(0.0);
	vec3 reflectionColor = vec3(0.0);
	vec3 transmissionColor = vec3(0.0);

	vec2 areaLightOffset;

	if(!scene->getNearestIntersectionAll(origin, ray, minimum))
	{
		return backgroundColor;
	}
	else
	{
		//return minimum.normal;
		//return abs(minimum.normal);

		if(minimum.objectType == OBJECT_TYPE_AREA_LIGHT)
		{
			return minimum.normal;
		}

		areaLightOffset.x = curand_uniform(state);
		areaLightOffset.y = curand_uniform(state);

		//minimum.point = minimum.point + minimum.normal * EPSILON;
		minimum.point = minimum.point + negate(ray) * EPSILON;
		minimum.normal = scene->getNormalAt(minimum);

		for(int i=0; i<scene->numAreaLights; i++)
		{
			output = output + scene->getAreaLightIlluminationAt(i, minimum, negate(ray), areaLightOffset, true , true);
			//output = output + scene->getAreaLightIlluminationAt(i, minimum, negate(ray));
		}

		for(int i=0; i<scene->numPointLights; i++)
		{
			output = output + scene->getPointLightIlluminationAt(i, minimum, negate(ray), true, true);
		}

		//output = scene->getDirectLighting(minimum, negate(ray), state);

		output = output + scene->materials[minimum.material]->emissiveColor;

		//return output;

		//////////////////////////////////////////////////////////////////////////////////////////////
		// Transmission
		//////////////////////////////////////////////////////////////////////////////////////////////

		if(scene->materials[minimum.material]->opacity < 1.0)
		{
			if(getTransmissionVector(1.0, scene->materials[minimum.material]->refractionIndex, minimum.normal, negate(ray), transmissionIn))
			{
				result = scene->getTransmission(transmissionIn, minimum, transmissionMinimum, transmissionOut);
				//return transmissionOut;

				switch(result)
				{
				case TRANSMISSION_FAIL:
					return vec3(0,0,0);
					//break;
				case TRANSMISSION_IN_OUT:
					transmissionColor = rayTrace(transmissionMinimum.point+transmissionMinimum.normal*EPSILON, transmissionOut, scene, state, backgroundColor, reflectionQuality, indirectQuality, steps, colorBleeding);
					break;
				case TRANSMISSION_IN_OBJECT:
					transmissionColor = rayTrace(transmissionMinimum.point+transmissionMinimum.normal*EPSILON, transmissionOut, scene, state, backgroundColor, reflectionQuality, indirectQuality, steps, colorBleeding);
					break;
				default:
					return vec3(0,1,0);
					//break;
				}
			}
			else
			{
				transmissionColor = rayTrace(minimum.point, transmissionIn, scene, state, backgroundColor, reflectionQuality, indirectQuality, steps, colorBleeding);
			}

			transmissionColor = transmissionColor * scene->getDiffuseAlbedo(minimum);
		}

		if(scene->materials[minimum.material]->opacity == 0.0f)
		{
			return transmissionColor * scene->getDiffuseAlbedo(minimum);
		}

		//return output;

		//////////////////////////////////////////////////////////////////////////////////////////////
		// Indirect Illumination
		//////////////////////////////////////////////////////////////////////////////////////////////

		bool indirect = scene->materials[minimum.material]->opacity > 0.0;
		//indirect = indirect && scene->materials[minimum.material]->reflectionAmount < 1.0;
		indirect = indirect && (indirectQuality >= 2);

		if(indirect)
		{
			step_u = (PI/2.0)/(float)indirectQuality;
			step_v = PI/(float)indirectQuality;
			rand = curand_uniform(state);
			//hitCount = 0.0;

			////////////////////////////////////////////////////

			for(int v=1; v <= indirectQuality; v++)
			{
				for(int u=1; u <= indirectQuality; u++)
				{
					rand = curand_uniform(state);
					direction1 = rotate(minimum.normal, minimum.tangent, (float)u*step_u+rand*step_u);
					direction1 = rotate(direction1, minimum.normal, (float)v*step_v+rand*step_v);
					direction2 = rotate(direction1, minimum.normal, PI);

					if(scene->getNearestIntersection(minimum.point, direction1, indirectIntersection))
					{
						//indirectIntersection.point = indirectIntersection.point + indirectIntersection.normal * EPSILON;
						indirectIntersection.point = indirectIntersection.point + negate(direction1)* EPSILON;
						indirectIntersection.normal = scene->getNormalAt(indirectIntersection);
						tempColor = vec3(0.0);
						for(int i=0; i<scene->numAreaLights; i++)
						{
							tempColor = scene->getAreaLightIlluminationAt(i, indirectIntersection, negate(direction1),areaLightOffset, true, false);
							tempColor += scene->materials[indirectIntersection.material]->emissiveColor;
							indirectColor = indirectColor + getOrenNayar(direction1, negate(ray), minimum.normal, tempColor,
								scene->getDiffuseAlbedo(minimum),scene->materials[minimum.material]->roughness);
						}

						for(int i=0; i<scene->numPointLights; i++)
						{
							tempColor = scene->getPointLightIlluminationAt(i, indirectIntersection, negate(direction1), true, false);
							tempColor += scene->materials[indirectIntersection.material]->emissiveColor;
							indirectColor = indirectColor + getOrenNayar(direction1, negate(ray), minimum.normal, tempColor,
								scene->getDiffuseAlbedo(minimum),scene->materials[minimum.material]->roughness);
						}	
					}
					/*else
					{
						indirectColor = indirectColor + getOrenNayar(direction1, negate(ray), minimum.normal, vec3(0.1,0.1,0.2),
							scene->materials[minimum.material]->getDiffuse(minimum.uv),scene->materials[minimum.material]->roughness);
					}*/

					if(scene->getNearestIntersection(minimum.point, direction2, indirectIntersection))
					{
						//indirectIntersection.point = indirectIntersection.point + indirectIntersection.normal * EPSILON;
						indirectIntersection.point = indirectIntersection.point + negate(direction2)* EPSILON;
						indirectIntersection.normal = scene->getNormalAt(indirectIntersection);
						tempColor = vec3(0.0);
						for(int i=0; i<scene->numAreaLights; i++)
						{
							tempColor = scene->getAreaLightIlluminationAt(i, indirectIntersection, negate(direction2), areaLightOffset, true, false);
							tempColor += scene->materials[indirectIntersection.material]->emissiveColor;
							indirectColor = indirectColor + getOrenNayar(direction2, negate(ray), minimum.normal, tempColor,
								scene->getDiffuseAlbedo(minimum),scene->materials[minimum.material]->roughness);
						}

						for(int i=0; i<scene->numPointLights; i++)
						{
							tempColor = scene->getPointLightIlluminationAt(i, indirectIntersection, negate(direction2), true, false);
							tempColor += scene->materials[indirectIntersection.material]->emissiveColor;
							indirectColor = indirectColor + getOrenNayar(direction2, negate(ray), minimum.normal, tempColor,
								scene->getDiffuseAlbedo(minimum),scene->materials[minimum.material]->roughness);
						}
						//indirectColor = indirectColor + tempColor;
						//hitCount += 1;
					}
					/*else
					{
						indirectColor = indirectColor + getOrenNayar(direction2, negate(ray), minimum.normal, vec3(0.1,0.1,0.2),
							scene->materials[minimum.material]->getDiffuse(minimum.uv),scene->materials[minimum.material]->roughness);
					}*/
				}
			}

			indirectColor = indirectColor / (float)(indirectQuality*indirectQuality*2);
			//indirectColor = indirectColor / hitCount;
			indirectColor = indirectColor * colorBleeding;
			//indirectColor = indirectColor * scene->materials[minimum.material]->getDiffuse(minimum.uv);
			indirectColor = indirectColor * scene->getDiffuseAlbedo(minimum);
		
			//return indirectColor;*/
		}

		//////////////////////////////////////////////////////////////////////////////////////////////
		// Reflections
		//////////////////////////////////////////////////////////////////////////////////////////////

		//bool isref = scene->getSpecularAlbedo(minimum) != vec3(0.0, 0.0, 0.0);
		bool isref = scene->materials[minimum.material]->specularColor != vec3(0.0,0.0,0.0);
		isref = isref && (scene->materials[minimum.material]->opacity > 0.0);
		isref = isref && (reflectionQuality > 0);
		isref = isref && (scene->materials[minimum.material]->reflectionAmount > 0.0);
		isref = isref && (steps.x > steps.y);

		//if(steps > 0 && scene->materials[minimum.material]->specularColor != vec3(0,0,0))
		if(isref)
		{
			reflectionVector = normalize(minimum.normal*2*dot(minimum.normal,negate(ray)) - negate(ray));

			if(scene->materials[minimum.material]->reflectionAngle == 0.0f)
			{
				reflectionColor = rayTrace(minimum.point, reflectionVector, scene, state, backgroundColor, reflectionQuality, indirectQuality, u_int2(steps.x,steps.y+1), colorBleeding);
			}
			else
			{
				//step_u = (PI/(180.0/scene->materials[minimum.material]->reflectionAngle))/(float)reflectionQuality;
				step_u = ((scene->materials[minimum.material]->reflectionAngle/2) * (PI/180)) / (float)reflectionQuality;
				step_v = PI/(float)reflectionQuality;

				float rand = curand_uniform(state)*PI;

				for(int v=1; v <= reflectionQuality; v++)
				{
					for(int u=1; u <= reflectionQuality; u++)
					{
						rand = curand_uniform(state);
						direction1 = rotate(reflectionVector, minimum.tangent, (float)u*step_u+rand*step_u);
						direction1 = rotate(direction1, reflectionVector, (float)v*step_v+rand*step_v);
						direction2 = rotate(direction1, reflectionVector, PI);

						reflectionColor = reflectionColor + rayTrace(minimum.point, direction1, scene, state, backgroundColor, reflectionQuality, indirectQuality, u_int2(steps.x,steps.y+1), colorBleeding);
						reflectionColor = reflectionColor + rayTrace(minimum.point, direction2, scene, state, backgroundColor, reflectionQuality, indirectQuality, u_int2(steps.x,steps.y+1), colorBleeding);
					}
				}
				reflectionColor = reflectionColor * (1.0/(float)(reflectionQuality*2*reflectionQuality));
			}

			
			//reflectionColor = shadowTrace(minimum.point, reflectionVector, scene, state, steps-1);
			
			//reflectionColor = reflectionColor * scene->materials[minimum.material]->specularColor;
			reflectionColor = reflectionColor * scene->getSpecularAlbedo(minimum);

		}
		else
		{
			reflectionColor = vec3(0,0,0);
		}
		
		//////////////////////////////////////////////////////////////////////////////////////////////

		output = lerp(output + indirectColor, reflectionColor, scene->materials[minimum.material]->reflectionAmount);
		output = lerp(transmissionColor, output, scene->materials[minimum.material]->opacity);
		return output;
		//return output + reflectionColor + indirectColor;
	}

}

#endif // _RAY_TRACING_FUNCTIONS_