#ifndef _HOST_CSG_TREE_
#define _HOST_CSG_TREE_

#include <vector>
#include "CSG_Node.cuh"

class HostCSGTree
{
public:
	std::vector<CSG_Node>		operations;
	std::vector<CSG_Box>		boxes;
	std::vector<CSG_Cylinder>	cylinders;
	std::vector<CSG_Sphere>		spheres;

	int material;

	__host__ HostCSGTree();

	__host__ void printContent(int color);
};

#endif //_HOST_CSG_TREE_

HostCSGTree::HostCSGTree()
{
	material = 0;
}


void HostCSGTree::printContent(int color)
{
	cprintf(color,"---------------------------\n");
	cprintf(color,"Showing content of CSG Tree\n");
	cprintf(color,"---------------------------\n");
	cprintf(color,"\tBoxes: %d\n", boxes.size());
	cprintf(color,"\tCylinders: %d\n", cylinders.size());
	cprintf(color,"\tSpheres: %d\n", spheres.size());
	cprintf(color,"\tOperations: %d\n", operations.size());
	cprintf(color,"---------------------------\n\n");
}