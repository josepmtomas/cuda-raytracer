
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "curand_kernel.h"

#include "RayTracing.cuh"
//#include "Scene.cuh"

//#include "CSG_Node.cuh"

#include <stdio.h>
#include <stdlib.h>

#include <GLFW/glfw3.h>

__global__ void rayTraceAA1(unsigned char *u_raster, DeviceScene *scene, curandState *states, SceneSettings settings)
{
	int j = blockIdx.x * blockDim.x + threadIdx.x;
	int i = blockIdx.y * blockDim.y + threadIdx.y;

	if(j < settings.renderWidth && i < settings.renderHeight)
	{
		int position = i*settings.renderWidth*3 + j*3;
		int id = i*settings.renderWidth+j;

		curand_init(1234, id, 0, &states[id]);

		vec3 ray = scene->camera->visualPixel((float)j,(float)i, settings.renderWidth, settings.renderHeight);
		vec3 color = rayTrace(scene->camera->pov, ray, scene, &states[id], settings.backgroundColor, settings.reflectionQuality, settings.indirectQuality,
			u_int2(settings.reflectionSteps,0), settings.colorBleeding);

		color.x > 1.0 ? color.x = 1.0 : 0;
		color.y > 1.0 ? color.y = 1.0 : 0;
		color.z > 1.0 ? color.z = 1.0 : 0;

		u_raster[position]	= color.x*255;
		u_raster[position+1]	= color.y*255;
		u_raster[position+2]	= color.z*255;
	}
}


__global__ void rayTraceAA4(float *f_raster, DeviceScene *scene, curandState *states, SceneSettings settings, vec2 *offsets)
{
	int j = blockIdx.x * blockDim.x + threadIdx.x;
	int i = blockIdx.y * blockDim.y + threadIdx.y;
	int k = blockIdx.z;

	if(j < settings.renderWidth && i < settings.renderHeight)
	{
		int position = i*settings.renderWidth*3 + j*3;
		//int id = i*settings.renderWidth*4+j*4+k;
		int id = i*settings.renderWidth+j;

		curand_init(1234+k, id, 0, &states[id]);

		vec3 ray = scene->camera->visualPixel((float)j+offsets[k].x,(float)i+offsets[k].y, settings.renderWidth, settings.renderHeight);
		vec3 color = rayTrace(scene->camera->pov, ray, scene, &states[id], settings.backgroundColor, settings.reflectionQuality, settings.indirectQuality,
			u_int2(settings.reflectionSteps,0), settings.colorBleeding);

		color.x > 1.0 ? color.x = 1.0 : 0;
		color.y > 1.0 ? color.y = 1.0 : 0;
		color.z > 1.0 ? color.z = 1.0 : 0;

		atomicAdd(&f_raster[position], color.x);
		atomicAdd(&f_raster[position+1], color.y);
		atomicAdd(&f_raster[position+2], color.z);
	}
}


__global__ void rayTraceAA16(float *f_raster, DeviceScene *scene, curandState *states, SceneSettings settings, vec2 *offsets)
{
	int j = blockIdx.x * blockDim.x + threadIdx.x;
	int i = blockIdx.y * blockDim.y + threadIdx.y;
	int k = blockIdx.z;

	if(j < settings.renderWidth && i < settings.renderHeight)
	{
		int position = i*settings.renderWidth*3 + j*3;
		//int id = i*settings.renderWidth*16 + j*16 + k;
		int id = i*settings.renderWidth + j;

		curand_init(1234+k, id, 0, &states[id]);

		vec3 ray = scene->camera->visualPixel((float)j+offsets[k].x,(float)i+offsets[k].y, settings.renderWidth, settings.renderHeight);
		vec3 color = rayTrace(scene->camera->pov, ray, scene, &states[id], settings.backgroundColor, settings.reflectionQuality, settings.indirectQuality,
			u_int2(settings.reflectionSteps,0), settings.colorBleeding);

		color.x > 1.0 ? color.x = 1.0 : 0;
		color.y > 1.0 ? color.y = 1.0 : 0;
		color.z > 1.0 ? color.z = 1.0 : 0;

		atomicAdd(&f_raster[position], color.x);
		atomicAdd(&f_raster[position+1], color.y);
		atomicAdd(&f_raster[position+2], color.z);
	}
}


__global__ void pixelMean(unsigned char *u_raster, float *f_raster, SceneSettings settings, int numSamples)
{
	int j = blockIdx.x * blockDim.x + threadIdx.x;
	int i = blockIdx.y * blockDim.y + threadIdx.y;
	int position = i*settings.renderWidth*3 + j*3;

	if(j < settings.renderWidth && i < settings.renderHeight)
	{
		u_raster[position]		= (f_raster[position]/(float)numSamples)*255;
		u_raster[position+1]	= (f_raster[position+1]/(float)numSamples)*255;
		u_raster[position+2]	= (f_raster[position+2]/(float)numSamples)*255;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////


void render(SceneSettings *settings, unsigned char *raster)
{
	glClear(GL_COLOR_BUFFER_BIT);

	glViewport(-(settings->renderWidth)/2, -(settings->renderHeight)/2, settings->renderWidth, settings->renderHeight);
	glClearColor(0.5,0.5,0.5,1.0);

	glRasterPos2i(0,0);
	glDrawPixels(settings->renderWidth, settings->renderHeight, GL_RGB, GL_UNSIGNED_BYTE, raster);
	glFlush();
}

int main(int argc, char **argv)
{
	///////////////////////////////////// GLFW variables

	int running = GL_TRUE;
	GLFWwindow	*window;
	float startTime;
	float endTime;
	vec4 times;

	///////////////////////////////////// Scene variables

	char *outFilename;

	HostScene		h_scene;
	DeviceScene		dh_scene;
	DeviceScene	   *dd_scene;
	SceneSettings	settings;

	curandState 	*d_states;

	unsigned char	*h_u_raster;	// Host Unsigned char Raster
	unsigned char	*d_u_raster;	// Device Unsigned char raster
	float			*h_f_raster;	// Host Float Raster
	float			*d_f_raster;	// Device Float Raster

	dim3	threadsPerBlock;
	dim3	numBlocks;

	///////////////////////////////////// Antialiasing samples offset

	vec2 *d_AA4Samples;
	vec2 AA4Samples[4];
		AA4Samples[0] = vec2(-0.25, 0.25);
		AA4Samples[1] = vec2( 0.25, 0.25);
		AA4Samples[2] = vec2( 0.25,-0.25);
		AA4Samples[3] = vec2(-0.25,-0.25);

	cudaMalloc((void**)&d_AA4Samples, sizeof(vec2)*4);
	cudaMemcpy(d_AA4Samples, &AA4Samples, sizeof(vec2)*4, cudaMemcpyHostToDevice);

	vec2 *d_AA16Samples;
	vec2 AA16Samples[16];
		AA16Samples[0] = vec2(-0.375, 0.375);	AA16Samples[1] = vec2(-0.125, 0.375);	AA16Samples[2] = vec2( 0.125, 0.375);	AA16Samples[3] = vec2( 0.375, 0.375);
		AA16Samples[4] = vec2(-0.375, 0.125);	AA16Samples[5] = vec2(-0.125, 0.125);	AA16Samples[6] = vec2( 0.125, 0.125);	AA16Samples[7] = vec2( 0.375, 0.125);
		AA16Samples[8] = vec2(-0.375,-0.125);	AA16Samples[9] = vec2(-0.125,-0.125);	AA16Samples[10]= vec2( 0.125,-0.125);	AA16Samples[11]= vec2( 0.375,-0.125);
		AA16Samples[12]= vec2(-0.375,-0.375);	AA16Samples[13]= vec2(-0.125,-0.375);	AA16Samples[14]= vec2( 0.125,-0.375);	AA16Samples[15]= vec2( 0.375,-0.375);

	cudaMalloc((void**)&d_AA16Samples, sizeof(vec2)*16);
	cudaMemcpy(d_AA16Samples, &AA16Samples, sizeof(vec2)*16, cudaMemcpyHostToDevice);

	/////////////////////////////////////

	cprintf(CONSOLE_COLOR_BLUE, "[GLFW] Initializing ... ");
	if(!glfwInit())
	{
		cprintf(CONSOLE_COLOR_RED, "[GLFW] Failed to initialize.\n");
		exit(EXIT_FAILURE);
	}
	cprintf(CONSOLE_COLOR_CYAN, "OK\n");

	/////////////////////////////////////

	if(argc == 1)
	{
		printf("Usage: CudaRayTracer.exe \"SceneFile\"\n");
		printf("       CudaRayTracer.exe \"SceneFile\"  \"ResultImage.format\"\n");
		return 3;
	}

	if(argc == 3)
	{
		outFilename = argv[2];
	}
	else
	{
		outFilename = "out.bmp";
	}

	startTime = glfwGetTime();
	if(!parseSceneFile(argv[1], &h_scene, &settings))
	{
		cprintf(CONSOLE_COLOR_RED, "Scene failed to load.\n");
		getchar();
		return 1;
	}
	endTime = glfwGetTime();
	times.x = (endTime - startTime);


	startTime = glfwGetTime();
	cprintf(CONSOLE_COLOR_GREEN, "\nScene loaded successfully.\n\n");

	cprintf(CONSOLE_COLOR_WHITE, "Transfering Scene to GPU\n");
	copySceneHostToDevice(&h_scene, &dh_scene);

	cudaMalloc((void**)&dd_scene, sizeof(DeviceScene));
	cudaMemcpy(dd_scene, &dh_scene, sizeof(DeviceScene), cudaMemcpyHostToDevice);

	cprintf(CONSOLE_COLOR_GREEN, "DONE\n\n");
	endTime = glfwGetTime();
	times.y = endTime - startTime;

	///////////////////////////////////// Cuda random malloc

	/*if(settings.antialiasing == 1) cudaMalloc((void **)&d_states, sizeof(curandState)*settings.renderWidth*settings.renderHeight);
	if(settings.antialiasing == 4) cudaMalloc((void **)&d_states, sizeof(curandState)*settings.renderWidth*settings.renderHeight*4);
	if(settings.antialiasing == 16) cudaMalloc((void **)&d_states, sizeof(curandState)*settings.renderWidth*settings.renderHeight*16);*/
	cudaMalloc((void **)&d_states, sizeof(curandState)*settings.renderWidth*settings.renderHeight);
	printf("CUDA Random malloc ... %s\n\n", cudaGetErrorString(cudaGetLastError()));

	///////////////////////////////////// GLFW Initialize

	/*cprintf(CONSOLE_COLOR_BLUE, "[GLFW] Initializing ... ");
	if(!glfwInit())
	{
		cprintf(CONSOLE_COLOR_RED, "[GLFW] Failed to initialize.\n");
		exit(EXIT_FAILURE);
	}
	cprintf(CONSOLE_COLOR_CYAN, "OK\n");*/

	cprintf(CONSOLE_COLOR_BLUE, "[GLFW] Creating window ... ");
	window = glfwCreateWindow(settings.renderWidth, settings.renderHeight, "CUDA RayTracer - Josep M. Tomas", NULL, NULL);

	if (!window)
	{
		cprintf(CONSOLE_COLOR_RED, "[GLFW] Error creating the window.\n");
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window);
	glfwSetWindowPos(window, 200, 150);
	glfwSwapInterval(1);

	glClear(GL_COLOR_BUFFER_BIT);
	glfwSwapBuffers(window);
	cprintf(CONSOLE_COLOR_CYAN, "OK\n\n");

	///////////////////////////////////// Allocate raster

	startTime = glfwGetTime();

	switch(settings.antialiasing)
	{
	case 1:
		h_u_raster = (unsigned char *)malloc(sizeof(unsigned char)*settings.renderWidth*settings.renderHeight*3);
		h_f_raster = (float *)malloc(sizeof(float)*settings.renderWidth*settings.renderHeight*3);
		threadsPerBlock = dim3(8,8);
		numBlocks = dim3(settings.renderWidth/threadsPerBlock.x, settings.renderHeight/threadsPerBlock.y);
		settings.renderWidth%threadsPerBlock.x > 0 ? numBlocks.x = numBlocks.x + 1 : 0;
		settings.renderHeight%threadsPerBlock.y > 0 ? numBlocks.y = numBlocks.y + 1 : 0;
		break;

	case 4:
		h_u_raster = (unsigned char *)malloc(sizeof(unsigned char)*settings.renderWidth*settings.renderHeight*3);
		h_f_raster = (float *)malloc(sizeof(float)*settings.renderWidth*settings.renderHeight*3);
		
		threadsPerBlock = dim3(8,8);
		numBlocks = dim3(settings.renderWidth/threadsPerBlock.x, settings.renderHeight/threadsPerBlock.y,4);
		settings.renderWidth%threadsPerBlock.x > 0 ? numBlocks.x = numBlocks.x + 1 : 0;
		settings.renderHeight%threadsPerBlock.y > 0 ? numBlocks.y = numBlocks.y + 1 : 0;
		break;
	case 16:
		h_u_raster = (unsigned char *)malloc(sizeof(unsigned char)*settings.renderWidth*settings.renderHeight*3);
		h_f_raster = (float *)malloc(sizeof(float)*settings.renderWidth*settings.renderHeight*3);
		
		threadsPerBlock = dim3(8,8);
		numBlocks = dim3(settings.renderWidth/threadsPerBlock.x, settings.renderHeight/threadsPerBlock.y,16);
		settings.renderWidth%threadsPerBlock.x > 0 ? numBlocks.x = numBlocks.x + 1 : 0;
		settings.renderHeight%threadsPerBlock.y > 0 ? numBlocks.y = numBlocks.y + 1 : 0;
		break;
	default:
		cprintf(CONSOLE_COLOR_RED, "[ERROR][Allocate] Wrong antialiasing value.\n");
		return 2;
		//break;
	}

	for(unsigned int i=0; i<settings.renderHeight; i++)
	{
		for(unsigned int j=0; j<settings.renderWidth*3; j+=3)
		{
			h_u_raster[i*settings.renderWidth*3 + j]	= 20;	//((j/3)/(float)window_width)*255;
			h_u_raster[i*settings.renderWidth*3 + j+1]	= 20;	//(i/(float)window_height)*255;
			h_u_raster[i*settings.renderWidth*3 + j+2]	= 20;	
			h_f_raster[i*settings.renderWidth*3 + j]	= 0.0;
			h_f_raster[i*settings.renderWidth*3 + j+1]	= 0.0;
			h_f_raster[i*settings.renderWidth*3 + j+2]	= 0.0;
		}
	}

	cudaMalloc((void**)&d_u_raster,sizeof(unsigned char)*settings.renderWidth*settings.renderHeight*3);
	cudaMemcpy(d_u_raster, h_u_raster, sizeof(unsigned char)*settings.renderWidth*settings.renderHeight*3, cudaMemcpyHostToDevice);
	printf("Unsigned char Raster init ... %s\n", cudaGetErrorString(cudaGetLastError()));

	cudaMalloc((void**)&d_f_raster,sizeof(float)*settings.renderWidth*settings.renderHeight*3);
	cudaMemcpy(d_f_raster, h_f_raster, sizeof(float)*settings.renderWidth*settings.renderHeight*3, cudaMemcpyHostToDevice);
	printf("Float Raster init ... %s\n\n", cudaGetErrorString(cudaGetLastError()));

	endTime = glfwGetTime();
	times.z = endTime - startTime;

	///////////////////////////////////// Launch kernels

	cudaDeviceSetLimit(cudaLimitStackSize, 20000);
	printf("Setting stack size ... %s\n", cudaGetErrorString(cudaGetLastError()));


	switch(settings.antialiasing)
	{
	case 1:
		startTime = glfwGetTime();
		rayTraceAA1<<<numBlocks,threadsPerBlock>>>(d_u_raster, dd_scene, d_states, settings);
		printf("RayTracing kernel: %s\n", cudaGetErrorString(cudaGetLastError()));

		cudaDeviceSynchronize();
		printf("Synchronize: %s\n", cudaGetErrorString(cudaGetLastError()));

		cudaMemcpy(h_u_raster, d_u_raster, sizeof(unsigned char)*settings.renderWidth*settings.renderHeight*3, cudaMemcpyDeviceToHost);
		printf("Raster copy: %s\n", cudaGetErrorString(cudaGetLastError()));
		
		endTime = glfwGetTime();
		times.w = endTime - startTime;
		break;

	case 4:
		startTime = glfwGetTime();
		rayTraceAA4<<<numBlocks,threadsPerBlock>>>(d_f_raster, dd_scene, d_states, settings, d_AA4Samples);
		printf("Raytracing kernel: %s\n", cudaGetErrorString(cudaGetLastError()));

		cudaDeviceSynchronize();
		printf("Synchronize: %s\n", cudaGetErrorString(cudaGetLastError()));

		threadsPerBlock.z = 1;
		pixelMean<<<numBlocks,threadsPerBlock>>>(d_u_raster, d_f_raster, settings, 4);
		printf("Mean kernel: %s\n", cudaGetErrorString(cudaGetLastError()));

		cudaDeviceSynchronize();
		printf("Synchronize: %s\n", cudaGetErrorString(cudaGetLastError()));

		cudaMemcpy(h_u_raster, d_u_raster, sizeof(unsigned char)*settings.renderWidth*settings.renderHeight*3, cudaMemcpyDeviceToHost);
		printf("Raster copy: %s\n", cudaGetErrorString(cudaGetLastError()));

		endTime = glfwGetTime();
		times.w = endTime - startTime;
		break;

	case 16:
		startTime = glfwGetTime();
		rayTraceAA16<<<numBlocks,threadsPerBlock>>>(d_f_raster, dd_scene, d_states, settings, d_AA16Samples);
		printf("Raytracing kernel: %s\n", cudaGetErrorString(cudaGetLastError()));

		cudaDeviceSynchronize();
		printf("Synchronize: %s\n", cudaGetErrorString(cudaGetLastError()));

		threadsPerBlock.z = 1;
		pixelMean<<<numBlocks,threadsPerBlock>>>(d_u_raster, d_f_raster, settings, 16);
		printf("Mean kernel: %s\n", cudaGetErrorString(cudaGetLastError()));

		cudaDeviceSynchronize();
		printf("Synchronize: %s\n", cudaGetErrorString(cudaGetLastError()));

		cudaMemcpy(h_u_raster, d_u_raster, sizeof(unsigned char)*settings.renderWidth*settings.renderHeight*3, cudaMemcpyDeviceToHost);
		printf("Raster copy: %s\n", cudaGetErrorString(cudaGetLastError()));

		endTime = glfwGetTime();
		times.w = endTime - startTime;
		break;

	default:
		cprintf(CONSOLE_COLOR_RED, "[ERROR][Execution] Wrong antialiasing value.\n");
		return 2;
		//break;
	}

	cprintf(CONSOLE_COLOR_WHITE, "\nResult times:\n");
	cprintf(CONSOLE_COLOR_WHITE, "\tScene parse: %.4f s\n", times.x);
	cprintf(CONSOLE_COLOR_WHITE, "\tScene copy: %.4f s\n", times.y);
	cprintf(CONSOLE_COLOR_WHITE, "\tRaster & misc.: %.4f s\n", times.z);
	cprintf(CONSOLE_COLOR_WHITE, "\tExecution: %.4f s\n", times.w);


	#ifdef FREEIMAGE_LIB
		FreeImage_Initialise();
	#endif

	unsigned char temp;
	for(int i=0; i<settings.renderWidth*settings.renderHeight*3; i=i+3)
	{
		temp = h_u_raster[i];
		h_u_raster[i] = h_u_raster[i+2];
		h_u_raster[i+2] = temp;
	}

	FIBITMAP *outBitmap = FreeImage_ConvertFromRawBits(h_u_raster, settings.renderWidth, settings.renderHeight, settings.renderWidth*3, 24, 0, 1, 2, false);
	
	FreeImage_Save(FreeImage_GetFIFFromFilename(outFilename), outBitmap, outFilename);

	#ifdef FREEIMAGE_LIB
		FreeImage_DeInitialise();
	#endif

	for(int i=0; i<settings.renderWidth*settings.renderHeight*3; i=i+3)
	{
		temp = h_u_raster[i];
		h_u_raster[i] = h_u_raster[i+2];
		h_u_raster[i+2] = temp;
	}

	//cprintf(CONSOLE_COLOR_BLUE, "Running.\n");
	while(running && !glfwWindowShouldClose(window))
	{
		glClear(GL_COLOR_BUFFER_BIT);

		render(&settings, h_u_raster);

		glfwSwapBuffers(window);
		glfwPollEvents();
		running = !glfwGetKey(window, GLFW_KEY_ESCAPE);
	}

	///////////////////////////////////// Free rasters

	cprintf(CONSOLE_COLOR_BLUE, "Ending.\n");

	free(h_u_raster);

	h_scene.clearScene();

	glfwDestroyWindow(window);
	glfwTerminate();

    exit(EXIT_SUCCESS);
}
