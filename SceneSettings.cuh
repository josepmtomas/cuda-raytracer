#ifndef _SCENE_SETTINGS_
#define _SCENE_SETTINGS_

#include "Algebra.cuh"

struct SceneSettings
{
public:
	vec3	backgroundColor;
	int		renderWidth;
	int		renderHeight;
	int		reflectionQuality;
	int		reflectionSteps;
	int		indirectQuality;
	float	colorBleeding;
	int		antialiasing;

	__host__ __device__ SceneSettings();
};

// Default values
SceneSettings::SceneSettings()
{
	backgroundColor = vec3(0,0,0);
	renderWidth = 512;
	renderHeight = 512;
	reflectionQuality = 3;
	reflectionSteps = 1;
	indirectQuality = 3;
	colorBleeding = 1;
	antialiasing = 1;
}

#endif // _SCENE_SETTINGS_