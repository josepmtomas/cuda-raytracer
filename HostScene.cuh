#ifndef _HOST_SCENE_
#define _HOST_SCENE_

#include <fstream>
#include <string>
#include <sstream>
#include <vector>

#include "SceneSettings.cuh"

#include "Material.cuh"
#include "Sphere.cuh"
#include "Box.cuh"
#include "Cone.cuh"
#include "Cylinder.cuh"
#include "HostMesh.cuh"
#include "HostCSGTree.cuh"

#include "PerspectiveCamera.cuh"


#include "PointLight.cuh"
#include "HostAreaLight.cuh"

using std::vector;

class HostScene
{
public:

	vector<Box>				boxes;
	vector<Cone>			cones;
	vector<Cylinder>		cylinders;
	vector<Sphere>			spheres;
	vector<HostMesh>		meshes;
	vector<HostCSGTree>		csgTrees;

	vector<HostTexture>		textures;
	vector<Material>		materials;

	vector<PointLight>		pointLights;
	vector<HostAreaLight>	areaLights;

	PerspectiveCamera	camera;

	__host__ HostScene();

	__host__ void addTexture(HostTexture _texture);
	__host__ void addMaterial(Material _material);

	__host__ void addBox(Box _box);
	__host__ void addCone(Cone _cone);
	__host__ void addCylinder(Cylinder _cylinder);
	__host__ void addSphere(Sphere _sphere);
	__host__ void addMesh(HostMesh _mesh);
	__host__ void addCSGTree(HostCSGTree _csgTree);

	__host__ void setCamera(PerspectiveCamera _camera);
	
	__host__ void addPointLight(PointLight _pointLight);
	__host__ void addAreaLight(HostAreaLight _areaLight);

	__host__ void printInfo();
	__host__ void clearScene();
};

#endif // _HOST_SCENE_


HostScene::HostScene()
{

}


void HostScene::addTexture(HostTexture _texture)
{
	textures.push_back(_texture);
}


void HostScene::addMaterial(Material _material)
{
	materials.push_back(_material);
}


void HostScene::addBox(Box _box)
{
	boxes.push_back(_box);
}


void HostScene::addCone(Cone _cone)
{
	cones.push_back(_cone);
}


void HostScene::addCylinder(Cylinder _cylinder)
{
	cylinders.push_back(_cylinder);
}


void HostScene::addSphere(Sphere _sphere)
{
	spheres.push_back(_sphere);
}


void HostScene::addMesh(HostMesh _mesh)
{
	meshes.push_back(_mesh);
}


void HostScene::addCSGTree(HostCSGTree _csgTree)
{
	csgTrees.push_back(_csgTree);
}


void HostScene::addPointLight(PointLight _pointLight)
{
	pointLights.push_back(_pointLight);
}


void HostScene::addAreaLight(HostAreaLight _areaLight)
{
	if(_areaLight.type != AREA_LIGHT_NONE)
		areaLights.push_back(_areaLight);
}


void HostScene::setCamera(PerspectiveCamera _camera)
{
	camera = _camera;
}


void HostScene::printInfo()
{
	printf("---------- HostScene info ----------\n");
	printf("\t%d Texture(s)\n", textures.size());
	printf("\t%d Material(s)\n", textures.size());
	printf("\t%d Sphere(s)\n", spheres.size());
	printf("\t%d Box(es)\n", boxes.size());
	printf("\t%d Cylinder(s)\n", cylinders.size());
	printf("\t%d Cone(s)\n", cones.size());
	printf("\t%d Mesh(es)\n", meshes.size());
	printf("\t%d PointLight(s)\n", pointLights.size());
	printf("\t%d AreaLights(s)\n", areaLights.size());
	printf("------------------------------------\n");
}


void HostScene::clearScene()
{
	boxes.clear();
	cones.clear();
	cylinders.clear();
	spheres.clear();
	materials.clear();
	meshes.clear();
	csgTrees.clear();
}