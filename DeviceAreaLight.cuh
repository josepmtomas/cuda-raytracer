#ifndef _DEVICE_AREA_LIGHT_
#define _DEVICE_AREA_LIGHT_

#include "Plane.cuh"

class DeviceAreaLight
{
public:

	vec3	*points;
	vec3	*normals;
	vec3	*tangents;
	vec3	*binormals;
	vec3	color;
	vec2	spacing;
	int		numPoints;
	int		type;

	Plane	*plane;

	__device__ __host__ DeviceAreaLight();
	__device__ __host__ DeviceAreaLight(vec3 _color);

	__device__ __host__ void setPoints(vec3 *_points, int _numPoints, int _type);
	__device__ __host__ void setPoints(vec3 *_points, vec3 *_normals, int _numPoints, int _type);
	__device__ __host__ void setPoints(vec3 *_points, vec3 *_tangents, vec3 *_binormals, int _numPoints, int _type);
	__device__ __host__ void setPlane(Plane *_plane);
	__device__ __host__ void setSpacing(vec2 _spacing);
	
	__device__ __host__ bool isReachable(int index, vec3 lightVector);

	__device__ __host__ bool rayIntersect(vec3 origin, vec3 ray, Intersection &intersection);
};

DeviceAreaLight::DeviceAreaLight()
{
	color = vec3(1.0,1.0,1.0);
}

DeviceAreaLight::DeviceAreaLight(vec3 _color)
{
	color = _color;
}

void DeviceAreaLight::setPoints(vec3 *_points, int _numPoints, int _type)
{
	points = _points;
	numPoints = _numPoints;
	type = _type;
}


void DeviceAreaLight::setPoints(vec3 *_points, vec3 *_normals, int _numPoints, int _type)
{
	points = _points;
	normals = _normals;
	numPoints = _numPoints;
	type = _type;
}


void DeviceAreaLight::setPoints(vec3 *_points, vec3 *_tangents, vec3 *_binormals, int _numPoints, int _type)
{
	points = _points;
	tangents = _tangents;
	binormals = _binormals;
	numPoints = _numPoints;
	type = _type;
}


void DeviceAreaLight::setPlane(Plane *_plane)
{
	plane = _plane;
}


void DeviceAreaLight::setSpacing(vec2 _spacing)
{
	spacing = _spacing;
}


bool DeviceAreaLight::isReachable(int index, vec3 lightVector)
{
	if(type == AREA_LIGHT_PLANE)
	{
		return true;
	}
	else if(type == AREA_LIGHT_SPHERE)
	{
		if(dot(normals[index], negate(lightVector)) >= 0.0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

bool DeviceAreaLight::rayIntersect(vec3 origin, vec3 ray, Intersection &intersection)
{
	if(type == AREA_LIGHT_PLANE)
	{
		if(plane->rayIntersection(origin, ray, intersection))
		{
			intersection.objectType = OBJECT_TYPE_AREA_LIGHT;
			intersection.normal = color;
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

#endif // _DEVICE_AREA_LIGHT_