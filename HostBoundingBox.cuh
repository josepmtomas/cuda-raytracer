#ifndef _HOST_BOUNDING_BOX_
#define _HOST_BOUNDING_BOX_

#include <vector>

#include "Algebra.cuh"

class HostBoundingBox
{
public:

	__host__ HostBoundingBox();
	__host__ HostBoundingBox(vec3 min, vec3 max, u_int3 subdivision, std::vector<int> *_meshPolygons,
						 std::vector<vec3> *_meshPositions, std::vector<vec2> *_meshUVs, std::vector<vec3> *_meshVNormals,
						 std::vector<vec3> *_meshFNormals, std::vector<vec4> *_meshTangents);
	__host__ ~HostBoundingBox();


	// Associated polygons
	std::vector<int>	meshPolygons;
	std::vector<vec3>	*meshPositions;
	std::vector<vec2>	*meshUVs;
	std::vector<vec3>	*meshVNormals;
	std::vector<vec3>	*meshFNormals;
	std::vector<vec4>	*meshTangents;

	// Bounding Box definition
	std::vector<int>	elements;
	std::vector<vec3>	position;
	std::vector<vec3>	fnormals;

	int  id;
	vec3 boundsMin;
	vec3 boundsMax;

	bool isLeaf;
	bool isEmpty;

	std::vector<HostBoundingBox> children;
	std::vector<int> childrenIndices;

	__host__ void buildBound(vec3 min, vec3 max);	// Builds the current bound (not its children(s))
	__host__ bool pointInside(vec3 point) const;
	__host__ void recursivePrint(int *index, int level);
	__host__ void recursiveMark(int *index);
	__host__ void toArray(std::vector<HostBoundingBox> *boundingBoxes);

};

//////////////////////////////////////////////////////////

void HostBoundingBox::buildBound(vec3 min, vec3 max)
{

	vec3 v[8];
	vec3 a,b,c;

	boundsMin = min;
	boundsMax = max;

	v[0] = vec3(min.x, min.y, max.z);
	v[1] = vec3(max.x, min.y, max.z);
	v[2] = vec3(max.x, min.y, min.z);
	v[3] = vec3(min.x, min.y, min.z);
	v[4] = vec3(min.x, max.y, max.z);
	v[5] = vec3(max.x, max.y, max.z);
	v[6] = vec3(max.x, max.y, min.z);
	v[7] = vec3(min.x, max.y, min.z);

	position.push_back(v[0]);
	position.push_back(v[1]);
	position.push_back(v[2]);
	position.push_back(v[3]);
	position.push_back(v[4]);
	position.push_back(v[5]);
	position.push_back(v[6]);
	position.push_back(v[7]);

	elements.push_back(4);	elements.push_back(0);	elements.push_back(1);	elements.push_back(5);
	elements.push_back(5);	elements.push_back(1);	elements.push_back(2);	elements.push_back(6);
	elements.push_back(6);	elements.push_back(2);	elements.push_back(3);	elements.push_back(7);
	elements.push_back(7);	elements.push_back(3);	elements.push_back(0);	elements.push_back(4);
	elements.push_back(7);	elements.push_back(4);	elements.push_back(5);	elements.push_back(6);
	elements.push_back(0);	elements.push_back(3);	elements.push_back(2);	elements.push_back(1);

	// Calculate normals
	for(int i=0; i<elements.size(); i+=4)
	{
		a = position[elements[i]];
		b = position[elements[i+1]];
		c = position[elements[i+2]];

		fnormals.push_back(normalize(cross((b-a),(c-b))));
	}
}


HostBoundingBox::HostBoundingBox()
{}


HostBoundingBox::HostBoundingBox(vec3 min, vec3 max, u_int3 subdivision, std::vector<int> *_meshPolygons,
						 std::vector<vec3> *_meshPositions, std::vector<vec2> *_meshUVs, std::vector<vec3> *_meshVNormals,
						 std::vector<vec3> *_meshFNormals, std::vector<vec4> *_meshTangents)
{

	//////////////////////////////

	int a,b,c;
	vec3 submin = vec3(INF);
	vec3 submax = vec3(-INF);
	vec3 step, start, end;
	HostBoundingBox tempBoundingBox;

	//////////////////////////////

	// Create the bounding box
	buildBound(min,max);

	meshPositions = _meshPositions;
	meshUVs = _meshUVs;
	meshVNormals = _meshVNormals;
	meshFNormals = _meshFNormals;
	meshTangents = _meshTangents;

	//printf("received %d polygons to check\n", _meshPolygons->size()/3);

	// Add and count the number of polygons inside this bound
	for(int e=0; e < _meshPolygons->size(); e+=3)
	{
		a = _meshPolygons->at(e);
		b = _meshPolygons->at(e+1);
		c = _meshPolygons->at(e+2);

		if( pointInside(_meshPositions->at(a)) ||
			pointInside(_meshPositions->at(b)) ||
			pointInside(_meshPositions->at(c)))
		{
			meshPolygons.push_back(a);
			meshPolygons.push_back(b);
			meshPolygons.push_back(c);
		}
	}

	//printf("I have %d polygons inside\n", meshPolygons.size());

	// If there aren't any polygons inside thsi bounding box
	if(meshPolygons.size()/3 == 0)
	{
		isEmpty = true;
		//printf("EMPTY ");
	}

	// If not, check the number of polygons inside
	else
	{
		// If the number of polygons is bigger than the threshold, then subdivide this bounding box
		// into a number of children determined by the subdivision value.
		if(meshPolygons.size()/3 > 300)
		{
			isLeaf = false;
			isEmpty = false;

			step.x = (max.x - min.x)/(float)subdivision.x;
			step.y = (max.y - min.y)/(float)subdivision.y;
			step.z = (max.z - min.z)/(float)subdivision.z;
			
			start = min;

			end = start + step;

			for(int z=0; z < subdivision.z; z++)
			{
				for(int y=0; y < subdivision.y; y++)
				{
					for(int x=0; x < subdivision.x; x++)
					{
						tempBoundingBox = HostBoundingBox(start, end, subdivision, &meshPolygons,
							meshPositions, meshUVs, meshVNormals, meshFNormals, meshTangents);

						if(!tempBoundingBox.isEmpty)
						{
							children.push_back(tempBoundingBox);
						}
						
						start.x = end.x;
						end.x += step.x;
					}
					start.x = min.x;
					end.x = start.x + step.x;
					start.y = end.y;
					end.y += step.y;
				}
				start.y = min.y;
				end.y = start.y + step.y;
				start.z = end.z;
				end.z += step.z;
			}
			// This node is not a leaf so it doesn't have associated polygons
			meshPolygons.clear();
		}
		// If the number of polygons is smaller than the threshold this bound is a leaf
		else
		{
			//printf("LEAF ");
			isLeaf = true;
			isEmpty = false;

			// Adjust the size of the bound
		}
	}
}


HostBoundingBox::~HostBoundingBox()
{}


bool HostBoundingBox::pointInside(vec3 point) const
{
	//float t=0.025;		// Threshold
	float t=0.1;		// Threshold
	vec3 min = boundsMin - t;
	vec3 max = boundsMax + t;

	if(point.x <= max.x && point.x >= min.x &&
	   point.y <= max.y && point.y >= min.y &&
	   point.z <= max.z && point.z >= min.z)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void HostBoundingBox::recursivePrint(int *index, int level)
{
	printf("Level: %d\tIndex: %d\tchildren: %d\tpolygons: %d\n", level, *index, children.size(), meshPolygons.size()/3);

	if(!isLeaf)
	{
		for(int i=0; i<children.size(); i++)
		{
			*index = *index + 1;
			children[i].recursivePrint(index, level+1);
		}
	}
}


void HostBoundingBox::recursiveMark(int *index)
{
	id = *index;
	childrenIndices.clear();

	if(!isLeaf)
	{
		for(int i=0; i<children.size(); i++)
		{
			*index = *index + 1;
			childrenIndices.push_back(*index);
			children[i].recursiveMark(index);
		}
	}
}

void HostBoundingBox::toArray(std::vector<HostBoundingBox> *boundingBoxes)
{
	/*std::vector<HostBoundingBox>::iterator it;

	for(int i=0; i<children.size(); i++)
	{
		it = boundingBoxes->begin() + childrenIndices[i];
		it = boundingBoxes->erase(it);
		boundingBoxes->insert(it,children[i]);
	}*/

	if(!isLeaf)
	{
		for(int i=0; i<children.size(); i++)
		{
			boundingBoxes->push_back(children[i]);
			children[i].toArray(boundingBoxes);
		}
	}
}

#endif //_BOUNDING_BOX_