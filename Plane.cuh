#ifndef _PLANE_
#define _PLANE_

#include "Intersection.cuh"

class Plane
{

public:

	vec3	points[4];
	vec3	normal;

	Plane();
	Plane(vec3 a, vec3 b, vec3 c, vec3 d, vec3 _normal);

	__device__ __host__ bool rayIntersection(vec3 origin, vec3 direction, Intersection &intersection);
};


Plane::Plane()
{
	points[0] = vec3(-1,0,-1);
	points[1] = vec3(-1,0, 1);
	points[2] = vec3( 1,0, 1);
	points[3] = vec3( 1,0,-1);
	normal = vec3(0,1,0);
}

Plane::Plane(vec3 a, vec3 b, vec3 c, vec3 d, vec3 _normal)
{
	points[0] = a;
	points[1] = b;
	points[2] = c;
	points[3] = d;
	normal = _normal;
	//normal = normalize(cross(b-a,c-b));
}

bool Plane::rayIntersection(vec3 origin, vec3 ray, Intersection &intersection)
{
	float t2, t = INFINITE;
	float area_a, area_b;
	float nDotR;
	vec3 point;

	// 0 - 1 - 2
	nDotR = dot(normal,ray);
	if(nDotR <= 0.0)
	{
		t2 = dot(normal, (points[0] - origin)) / nDotR;
		point = origin + ray * t2;
		area_a = dot(cross((points[2]-points[0]),(point-points[0])),cross((points[1]-points[2]),(point-points[2])));
		area_b = dot(cross((points[2]-points[0]),(point-points[0])),cross((points[0]-points[1]),(point-points[0])));

		if(area_a >= 0 && area_b >= 0)
		{
			if(t > t2 && t2 >= 0.0)
			{
				t = t2;
			}
		}
	}

	// 2 - 3 - 0
	nDotR = dot(normal,ray);
	if(nDotR <= 0.0)
	{
		t2 = dot(normal, (points[2] - origin)) / nDotR;
		point = origin + ray * t2;
		area_a = dot(cross((points[0]-points[2]),(point-points[2])),cross((points[3]-points[0]),(point-points[0])));
		area_b = dot(cross((points[0]-points[2]),(point-points[2])),cross((points[2]-points[3]),(point-points[2])));

		if(area_a >= 0 && area_b >= 0)
		{
			if(t > t2 && t2 > 0.0)
			{
				t = t2;
			}
		}
	}

	if(t == INFINITE)
	{
		return false;
	}
	else
	{
		intersection.distance = t;
		intersection.point = origin + ray * t;
		intersection.normal = normal;
		return true;
	}
}


#endif // _PLANE_