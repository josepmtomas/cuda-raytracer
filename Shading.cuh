#ifndef _SHADING_FUNCTIONS_
#define _SHADING_FUNCTIONS_

#include "Algebra.cuh"

__device__ __host__ vec3 getOrenNayar(vec3 vLight, vec3 vEye, vec3 vNormal, vec3 lightIntensity, vec3 albedo, float roughness)
{
	float roughness2 = roughness * roughness;
	//float refindex = 1 - roughness;
	float A, B, alpha, beta, gamma, I;
	float normalDotLight, lightDotNormal, normalDotEye, eyeDotNormal;
	vec3  vLightProjected, vEyeProjected;


	/////////////////////////////////////////////////////////////////////////////////
	// OREN - NAYAR
	/////////////////////////////////////////////////////////////////////////////////
	A = 1.0f - 0.5f * roughness2 / (roughness2 + 0.57f);
	B = 0.45f * roughness2 / (roughness2 + 0.09f);

	normalDotLight = dot(vNormal,vLight);
	lightDotNormal = dot(vLight,vNormal);
	normalDotEye = dot(vNormal,vEye);
	eyeDotNormal = dot(vEye,vNormal);
	vLightProjected = vLight - vNormal * normalDotLight;
	vEyeProjected = vEye - vNormal * normalDotEye;

	alpha = max(1-cosf(eyeDotNormal), 1-cosf(lightDotNormal));
	beta  = min(1-cosf(eyeDotNormal), 1-cosf(lightDotNormal));
	gamma = dot(l1Norm(vEyeProjected), l1Norm(vLightProjected));

	I = normalDotLight * (A+B*max(0.0f,gamma) * sin(alpha) * sin(beta));

	return albedo * I * lightIntensity;
}


__device__ __host__ vec3 getCookTorrance(vec3 vLight, vec3 vEye, vec3 vNormal, vec3 lightIntensity, vec3 albedo, float roughness)
{
	float roughness2 = roughness * roughness;
	float refindex = 1 - roughness;
	float exponent, D, F, X, G;
	float normalDotLight, normalDotEye, normalDotHalf, normalDotHalf2;
	vec3  vHalf;

	/////////////////////////////////////////////////////////////////////////////////
	// COOK - TORRANCE
	/////////////////////////////////////////////////////////////////////////////////
	vHalf = normalize(vLight + vEye);
	normalDotEye = dot(vNormal,vEye);
	normalDotLight = dot(vNormal,vLight);

	// Beckman's distribution function D
	normalDotHalf = dot(vNormal, vHalf);
	normalDotHalf2 = normalDotHalf * normalDotHalf;
	exponent = -(1-normalDotHalf2) / (normalDotHalf2*roughness2);
	D = pow(EULER,exponent);

	// Compute Fresnel term F
	F = lerp(powf(1 - normalDotEye, 5), 1.0f, refindex);

	// Compute self shadowing term G
	X = 2.0f * normalDotHalf / dot(vEye,vHalf);
	G = min(1.0f, min(X * normalDotLight, X * normalDotEye));

	// Compute final Cook-Torrance specular term
	X = (D*F*G) / (normalDotEye * PI);

	return X * albedo * lightIntensity;
}


__device__ void getOrenNayarCookTorrance(vec3 vLight, vec3 vEye, vec3 vNormal, vec3 lightIntensity, vec3 albedo, vec3 spec, float roughness, vec3 &diffuse, vec3 &specular)
{
	float roughness2 = roughness * roughness;
	float refindex = 1 - roughness;
	float A, B, alpha, beta, gamma, I, exponent, D, F, X, G;
	float normalDotLight, lightDotNormal, normalDotEye, eyeDotNormal, normalDotHalf, normalDotHalf2;
	vec3  vLightProjected, vEyeProjected, vHalf;


	/////////////////////////////////////////////////////////////////////////////////
	// OREN - NAYAR
	/////////////////////////////////////////////////////////////////////////////////
	A = 1.0f - 0.5f * roughness2 / (roughness2 + 0.57f);
	B = 0.45f * roughness2 / (roughness2 + 0.09f);

	normalDotLight = dot(vNormal,vLight);
	lightDotNormal = dot(vLight,vNormal);
	normalDotEye = dot(vNormal,vEye);
	eyeDotNormal = dot(vEye,vNormal);
	vLightProjected = vLight - vNormal * normalDotLight;
	vEyeProjected = vEye - vNormal * normalDotEye;

	alpha = max(1-cosf(eyeDotNormal), 1-cosf(lightDotNormal));
	beta  = min(1-cosf(eyeDotNormal), 1-cosf(lightDotNormal));
	gamma = dot(l1Norm(vEyeProjected), l1Norm(vLightProjected));

	I = normalDotLight * (A+B*max(0.0f,gamma) * sin(alpha) * sin(beta));

	diffuse = albedo * I * lightIntensity;

	/////////////////////////////////////////////////////////////////////////////////
	// COOK - TORRANCE
	/////////////////////////////////////////////////////////////////////////////////
	vHalf = normalize(vLight + vEye);

	// Beckman's distribution function D
	normalDotHalf = dot(vNormal, vHalf);
	normalDotHalf2 = normalDotHalf * normalDotHalf;
	exponent = -(1-normalDotHalf2) / (normalDotHalf2*roughness2);
	D = pow(EULER,exponent);

	// Compute Fresnel term F
	F = lerp(powf(1 - normalDotEye, 5), 1.0f, refindex);

	// Compute self shadowing term G
	X = 2.0f * normalDotHalf / dot(vEye,vHalf);
	G = min(1.0f, min(X * normalDotLight, X * normalDotEye));

	// Compute final Cook-Torrance specular term
	X = (D*F*G) / (normalDotEye * PI);

	specular = vec3(X) * spec * lightIntensity;
}


/*	float nDotL = dot(minimum.normal,lightVector);
	vec3 H = normalize(negate(ray) + lightVector);
	float nDotH = dot(minimum.normal,H);

	vec3 diffuse = scene->materials[minimum.material]->diffuseColor * nDotL;
	vec3 specular = scene->materials[minimum.material]->specularColor * powf(nDotH,15);*/

__device__ vec3 getLambertDiffuse(vec3 vLight, vec3 vNormal, vec3 albedo, vec3 lightIntensity)
{
	return albedo * dot(vNormal, vLight) * lightIntensity;
}

__device__ vec3 getBlinnSpecular(vec3 vLight, vec3 vNormal, vec3 vEye, vec3 albedo, float power, vec3 lightIntensity)
{
	vec3 H = normalize(vEye +  vLight);
	return albedo * powf(dot(vNormal, H),power) * lightIntensity;
}

#endif;
