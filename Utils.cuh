#ifndef _UTILITIES_
#define _UTILITIES_

#include <iostream>
#include <windows.h>

#define CONSOLE_COLOR_BLACK		 0
#define CONSOLE_COLOR_DARKBLUE	 1
#define CONSOLE_COLOR_DARKGREEN  2
#define CONSOLE_COLOR_DARKCYAN   3
#define CONSOLE_COLOR_DARKRED	 4
#define CONSOLE_COLOR_DARKPURPLE 5
#define CONSOLE_COLOR_DARKYELLOW 6
#define CONSOLE_COLOR_GRAY		 7
#define CONSOLE_COLOR_DARKGRAY	 8
#define CONSOLE_COLOR_BLUE		 9
#define CONSOLE_COLOR_GREEN		10
#define CONSOLE_COLOR_CYAN		11
#define CONSOLE_COLOR_RED		12
#define CONSOLE_COLOR_PURPLE	13
#define CONSOLE_COLOR_YELLOW	14
#define CONSOLE_COLOR_WHITE		15

int cprintf (int textColor, const char *format, ...);

int cprintf (int textColor, const char *format, ...)
{
	HANDLE hConsole;
	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	//SetConsoleTextAttribute(hConsole,(backColor*16)+textColor);
	SetConsoleTextAttribute(hConsole, textColor);

	va_list arg;
	int done;
	
	va_start (arg, format);
	done = vfprintf (stdout, format, arg);
	va_end (arg);

	SetConsoleTextAttribute(hConsole, CONSOLE_COLOR_GRAY);

	return done;
}

#endif // _UTILITIES_