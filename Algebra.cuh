#ifndef _CUDARAYTRACER_ALGEBRA_
#define _CUDARAYTRACER_ALGEBRA_

//#include <vector_types.h>
//#include <vector_functions.h>
#include <glm/glm.hpp>
#include "math.h"
#include "Utils.cuh"

#define PI			3.14159265f
#define INF			3.4E38f
#define EPSILON		0.001f
#define EULER       2.718281828459045f

#define INTERSECTION_NONE				0
#define INTERSECTION_IN					1
#define INTERSECTION_OUT				2
#define INTERSECTION_INSIDE				3

#define OBJECT_TYPE_SPHERE				10
#define OBJECT_TYPE_BOX					11
#define OBJECT_TYPE_CONE				12
#define OBJECT_TYPE_CYLINDER			13
#define OBJECT_TYPE_MESH				14
#define OBJECT_TYPE_AREA_LIGHT			15

#define OBJECT_TYPE_CSG_ROOT			20
#define OBJECT_TYPE_CSG_UNION			21
#define OBJECT_TYPE_CSG_DIFFERENCE		22
#define OBJECT_TYPE_CSG_INTERSECTION	23
#define OBJECT_TYPE_CSG_SPHERE			24
#define OBJECT_TYPE_CSG_BOX				25
#define OBJECT_TYPE_CSG_CONE			26
#define OBJECT_TYPE_CSG_CYLINDER		27
#define OBJECT_TYPE_CSG_NODE			28

#define CSG_CHILD_LEFT					30
#define CSG_CHILD_RIGHT					31

#define CSG_OPERATION_NONE				100
#define CSG_OPERATION_UNION				101
#define CSG_OPERATION_DIFFERENCE		102
#define CSG_OPERATION_INTERSECTION		103

#define AREA_LIGHT_NONE					0		// Area Light not built
#define AREA_LIGHT_PLANE				1		
#define AREA_LIGHT_SPHERE				2

#define TRANSMISSION_FAIL				0
#define TRANSMISSION_IN_OUT				1		// Ray enters a transparent object and exits without hittings anything else
#define TRANSMISSION_IN_OBJECT			2		// Ray enters a transparent object and hits another object before exiting


////////////////////////////////////////////////////////////////////////////////////////
// FLOAT
////////////////////////////////////////////////////////////////////////////////////////

inline __device__ __host__ float lerp(const float &x, const float &y, const float &a)
{
	return (y*a + x*(1.0f-a));
}

////////////////////////////////////////////////////////////////////////////////////////
// TYPES
////////////////////////////////////////////////////////////////////////////////////////

struct face3
{
	unsigned int position[3];
	unsigned int vnormal[3];
	unsigned int uv[3];
};

struct u_int2
{
	unsigned int x,y;

	__device__ __host__ u_int2();
	__device__ __host__ u_int2(unsigned int xy);
	__device__ __host__ u_int2(unsigned int _x, unsigned int _y);
};

struct u_int3
{
	unsigned int x,y,z;

	__device__ __host__ u_int3();
	__device__ __host__ u_int3(unsigned int xyz);
	__device__ __host__ u_int3(u_int2 xy, unsigned int _z);
	__device__ __host__ u_int3(unsigned int _x, unsigned int _y, unsigned int _z);
};

struct vec2
{
	float x,y;

	__device__ __host__ vec2();
	__device__ __host__ vec2(float xy);
	__device__ __host__ vec2(float _x, float _y);
};

struct vec3
{
	float x,y,z;

	__device__ __host__ vec3();
	__device__ __host__ vec3(float xyz);
	__device__ __host__ vec3(vec2 v, float _z);
	__device__ __host__ vec3(float _x, float _y, float _z);
};

struct vec4
{
	float x,y,z,w;

	__device__ __host__ vec4();
	__device__ __host__ vec4(float xyzw);
	__device__ __host__ vec4(vec3 v);
	__device__ __host__ vec4(vec3 v, float _w);
	__device__ __host__ vec4(float _x, float _y, float _z, float _w);
};

struct mat3
{
	vec3 m[3];

	__device__ __host__ mat3();
	__device__ __host__ mat3(vec3 v1, vec3 v2, vec3 v3);
};

struct mat4
{
	vec4 m[4];

	__device__ __host__ mat4();
	__device__ __host__ mat4(float x);
	__device__ __host__ mat4(vec4 v1, vec4 v2, vec4 v3, vec4 v4);
};


////////////////////////////////////////////////////////////////////////////////////////
// UINT2
////////////////////////////////////////////////////////////////////////////////////////

inline u_int2::u_int2()
{
	x=y=0;
}

inline u_int2::u_int2(unsigned int xy)
{
	x = xy;
	y = xy;
}

inline u_int2::u_int2(unsigned int _x, unsigned int _y)
{
	x = _x;
	y = _y;
}

////////////////////////////////////////////////////////////////////////////////////////
// UINT3
////////////////////////////////////////////////////////////////////////////////////////

inline u_int3::u_int3()
{
	x=y=z=0;
}

inline u_int3::u_int3(unsigned int xyz)
{
	x=y=z = xyz;
}

inline u_int3::u_int3(u_int2 u, unsigned int _z)
{
	x = u.x;
	y = u.y;
	z = _z;
}

inline u_int3::u_int3(unsigned int _x, unsigned int _y, unsigned int _z)
{
	x = _x;
	y = _y;
	z = _z;
}

////////////////////////////////////////////////////////////////////////////////////////
// VEC2
////////////////////////////////////////////////////////////////////////////////////////

inline vec2::vec2()
{
	x=y=0.0;
}

inline vec2::vec2(float xy)
{
	x = xy;
	y = xy;
}

inline vec2::vec2(float _x, float _y)
{
	x = _x;
	y = _y;
}

inline __device__ __host__ bool operator == (const vec2 &a, const vec2 &b)
{
	if(a.x == b.x && a.y == b.y) return true;
	else return false;
}

inline __device__ __host__ vec2 operator + (const vec2 &a, const vec2 &b)
{
	return vec2(a.x+b.x, a.y+b.y);
}

inline __device__ __host__ vec2 operator - (const vec2 &a, const vec2 &b)
{
	return vec2(a.x-b.x, a.y-b.y);
}

inline __device__ __host__ vec2 operator * (const vec2 &a, const float &x)
{
	return vec2(a.x*x, a.y*x);
}

////////////////////////////////////////////////////////////////////////////////////////
// VEC3
////////////////////////////////////////////////////////////////////////////////////////

inline vec3::vec3()
{
	x=y=z=0.0;
}

inline vec3::vec3(float xyz)
{
	x = xyz;
	y = xyz;
	z = xyz;
}

inline vec3::vec3(vec2 v, float _z)
{
	x = v.x;
	y = v.y;
	z = _z;
}

inline vec3::vec3(float _x, float _y, float _z)
{
	x = _x;
	y = _y;
	z = _z;
}


inline __device__ __host__ vec3 vec4toVec3(vec4 v)
{
	return vec3(v.x, v.y, v.z);
}

inline __device__ __host__ bool operator == (const vec3 &a, const vec3 &b)
{
	if( a.x == b.x && a.y == b.y && a.z == b.z ) return true;
	else return false;
}

inline __device__ __host__ bool operator != (const vec3 &a, const vec3 &b)
{
	if( a.x != b.x || a.y != b.y || a.z != b.z ) return true;
	else return false;
}

inline __device__ __host__ bool operator < (const vec3 &a, const vec3 &b)
{
	if((a.x < b.x) && (a.y < b.y) && (a.z < b.z)) return true;
	else return false;
}

inline __device__ __host__ bool operator > (const vec3 &a, const vec3 &b)
{
	if((a.x > b.x) && (a.y > b.y) && (a.z > b.z)) return true;
	else return false;
}

inline __device__ __host__ bool operator <= (const vec3 &a, const vec3 &b)
{
	if((a.x <= b.x) && (a.y <= b.y) && (a.z <= b.z)) return true;
	else return false;
}

inline __device__ __host__ bool operator >= (const vec3 &a, const vec3 &b)
{
	if((a.x >= b.x) && (a.y >= b.y) && (a.z >= b.z)) return true;
	else return false;
}

inline __device__ __host__ vec3 operator + (const vec3 a, const vec3 b)
{
	return vec3(a.x+b.x, a.y+b.y, a.z+b.z);
}

inline __device__ __host__ vec3 operator - (const vec3 a, const vec3 b)
{
	return vec3(a.x-b.x, a.y-b.y, a.z-b.z);
}

inline __device__ __host__ void operator += (vec3 &a, const vec3 b)
{
	a.x = a.x + b.x;
	a.y = a.y + b.y;
	a.z = a.z + b.z;
}

inline __device__ __host__ void operator -= (vec3 &a, const vec3 b)
{
	a.x = a.x - b.x;
	a.y = a.y - b.y;
	a.z = a.z - b.z;
}

inline __device__ __host__ vec3 operator*(const vec3 &a, const float &x)
{
	return vec3(a.x*x, a.y*x, a.z*x);
}

inline __device__ __host__ vec3 operator/(const vec3 &a, const float &x)
{
	return vec3(a.x/x, a.y/x, a.z/x);
}


inline __device__ __host__ vec3 operator*(const float &x, const vec3 &a)
{
	return vec3(a.x*x, a.y*x, a.z*x);
}

inline __device__ __host__ vec3 operator*(const vec3 &a, const vec3 &b)
{
	return vec3(a.x*b.x, a.y*b.y, a.z*b.z);
}

inline __device__ __host__ vec3 abs(vec3 a)
{
	return vec3(abs(a.x),abs(a.y),abs(a.z));
}

inline __device__ __host__ vec3 cross(const vec3 &a, const vec3 &b)
{
	return vec3(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x); 
}

inline __device__ __host__ float distance(const vec3 &a, const vec3 &b)
{
	return sqrtf(powf(a.x-b.x,2)+powf(a.y-b.y,2)+powf(a.z-b.z,2));
}

inline __device__ __host__ float dot(const vec3 &a, const vec3 &b)
{
	return a.x*b.x + a.y*b.y + a.z*b.z;
}

inline __device__ __host__ vec3 l1Norm(const vec3 &v)
{
	return v.x + v.y + v.z;
}

inline __device__ __host__ vec3 lerp(const vec3 &v1, const vec3 &v2, const float &a)
{
	return (v2*a + v1*(1.0f-a));
}

inline __device__ __host__ float module(const vec3 &a)
{
	return sqrtf((a.x*a.x)+(a.y*a.y)+(a.z*a.z));
}


inline __device__ __host__ vec3 negate(const vec3 &a)
{
	return vec3(-a.x, -a.y, -a.z);
}


inline __device__ __host__ vec3 normalize(const vec3 &a)
{
	return a*(1/module(a));
}

inline __device__ __host__ float rcproduct(vec3 row, vec3 col)		// Row x Column product
{
	return (row.x*col.x)+(row.y*col.y)+(row.z*col.z);
}


////////////////////////////////////////////////////////////////////////////////////////
// VEC4
////////////////////////////////////////////////////////////////////////////////////////

inline vec4::vec4()
{
	x=y=z=w=0.0;
}

inline vec4::vec4(float xyzw)
{
	x = xyzw;
	y = xyzw;
	z = xyzw;
	w = xyzw;
}

inline vec4::vec4(vec3 v)
{
	x = v.x;
	y = v.y;
	z = v.z;
	w = 1.0;
}

inline vec4::vec4(vec3 v, float _w)
{
	x = v.x;
	y = v.y;
	z = v.z;
	w = _w;
}

inline vec4::vec4(float _x, float _y, float _z, float _w)
{
	x = _x;
	y = _y;
	z = _z;
	w = _w;
}

inline __device__ __host__ float dot(const vec4 &v1, const vec4 &v2)
{
	return v1.x*v2.x + v1.y*v2.y + v1.z*v2.z + v1.w*v2.w;
}

inline __device__ __host__ vec4 operator + (const vec4 &v1, const vec4 &v2)
{
	return vec4(v1.x+v2.x, v1.y+v2.y, v1.z+v2.z, v1.w+v2.w);
}

inline __device__ __host__ vec4 operator - (const vec4 &v1, const vec4 &v2)
{
	return vec4(v1.x-v2.x, v1.y-v2.y, v1.z-v2.z, v1.w-v2.w);
}

inline __device__ __host__ float operator * (const vec4 &v1, const vec4 &v2)
{
	return (v1.x*v2.x)+(v2.y*v2.y)+(v1.z*v2.z)+(v1.w*v2.w);
}

inline __device__ __host__ vec4 operator * (const vec4 v, const float x)
{
	return vec4(v.x*x, v.y*x, v.z*x, v.w*x);
}

inline __device__ __host__ vec4 operator * (const float &x, const vec4 &v)
{
	return vec4(v.x*x, v.y*x, v.z*x, v.w*x);
}

////////////////////////////////////////////////////////////////////////////////////////
// MAT3
////////////////////////////////////////////////////////////////////////////////////////

inline mat3::mat3()
{
	m[0] = vec3(1,0,0);
	m[1] = vec3(0,1,0);
	m[2] = vec3(0,0,1);
}

inline mat3::mat3(vec3 v1, vec3 v2, vec3 v3)
{
	m[0] = v1;
	m[1] = v2;
	m[2] = v3;
}

inline __device__ __host__ mat3 operator * (const mat3 &m1, const mat3 &m2)
{
	mat3 m;
	vec3 col;

	for(int i=0; i<3; i++)
	{
		col = vec3(m2.m[0].x, m2.m[1].x, m2.m[2].x);
		m.m[i].x = rcproduct(m1.m[i],col);
		col = vec3(m2.m[0].y, m2.m[1].y, m2.m[2].y);
		m.m[i].y = rcproduct(m1.m[i],col);
		col = vec3(m2.m[0].z, m2.m[1].z, m2.m[2].z);
		m.m[i].z = rcproduct(m1.m[i],col);
	}

	return m;
}

inline __device__ __host__ vec3 operator * (const vec3 &v, const mat3 &m)
{
	vec3 result;
	vec3 col;

	col = vec3(m.m[0].x, m.m[1].x, m.m[2].x);
	result.x = rcproduct(v,col);
	col = vec3(m.m[0].y, m.m[1].y, m.m[2].y);
	result.y = rcproduct(v,col);
	col = vec3(m.m[0].z, m.m[1].z, m.m[2].z);
	result.z = rcproduct(v,col);

	return result;
}

inline __device__ __host__ vec3 rotate(vec3 v, vec3 axis, float angle)
{
	mat3 rotation = mat3();

	/*rotation.m[0].x = cosf(angle) + ((axis.x*axis.x)*(1 - cosf(angle)));
	rotation.m[0].y = (axis.x*axis.y*(1 - cosf(angle))) - axis.z*sinf(angle);
	rotation.m[0].z = (axis.x*axis.z*(1 - cosf(angle))) + axis.y*sinf(angle);

	rotation.m[1].x = (axis.y*axis.x*(1 - cosf(angle))) + axis.z*sinf(angle);
	rotation.m[1].y = cosf(angle) + ((axis.y*axis.y)*(1 - cosf(angle)));
	rotation.m[1].z = (axis.y*axis.z*(1 - cosf(angle))) - axis.x*sinf(angle);

	rotation.m[2].x = (axis.z*axis.x*(1 - cosf(angle))) - axis.y*sinf(angle);
	rotation.m[2].y = (axis.z*axis.y*(1 - cosf(angle))) + axis.x*sinf(angle);
	rotation.m[2].z = cosf(angle) + ((axis.z*axis.z)*(1 - cosf(angle)));*/

	float x2 = axis.x*axis.x;
	float y2 = axis.y*axis.y;
	float z2 = axis.z*axis.z;

	rotation.m[0].x = x2 + ((1-x2)*cos(angle));
	rotation.m[0].y = axis.x*axis.y*(1-cos(angle)) - axis.z*sin(angle);
	rotation.m[0].z = axis.x*axis.z*(1-cos(angle)) + axis.y*sin(angle);

	rotation.m[1].x = axis.x*axis.y*(1-cos(angle)) + axis.z*sin(angle);
	rotation.m[1].y = y2 + (1-y2)*cos(angle);
	rotation.m[1].z = axis.y*axis.z*(1-cos(angle)) - axis.x*sin(angle);

	rotation.m[2].x = axis.x*axis.z*(1-cos(angle)) - axis.y*sin(angle);
	rotation.m[2].y = axis.y*axis.z*(1-cos(angle)) + axis.x*sin(angle);
	rotation.m[2].z = z2 + (1-z2)*cos(angle);

	return v*rotation;
}

////////////////////////////////////////////////////////////////////////////////////////
// MAT4
////////////////////////////////////////////////////////////////////////////////////////

inline mat4::mat4()
{
	m[0] = vec4(1,0,0,0);
	m[1] = vec4(0,1,0,0);
	m[2] = vec4(0,0,1,0);
	m[3] = vec4(0,0,0,1);
}

inline mat4::mat4(float x)
{
	m[0] = vec4(x);
	m[1] = vec4(x);
	m[2] = vec4(x);
	m[3] = vec4(x);
}

inline mat4::mat4(vec4 v1, vec4 v2, vec4 v3, vec4 v4)
{
	m[0] = v1;
	m[1] = v2;
	m[2] = v3;
	m[3] = v4;
}

inline __device__ __host__ mat4 transpose(mat4 &m1)
{
	mat4 m;

	m.m[0] = vec4(m1.m[0].x, m1.m[1].x, m1.m[2].x, m1.m[3].x);
	m.m[1] = vec4(m1.m[0].y, m1.m[1].y, m1.m[2].y, m1.m[3].y);
	m.m[2] = vec4(m1.m[0].z, m1.m[1].z, m1.m[2].z, m1.m[3].z);
	m.m[3] = vec4(m1.m[0].w, m1.m[1].w, m1.m[2].w, m1.m[3].w);

	return m;
}

inline __device__ __host__ mat4 operator - (const mat4 &m1, const mat4 &m2)
{
	return mat4(m1.m[0] - m2.m[0],
				m1.m[1] - m2.m[1],
				m1.m[2] - m2.m[2],
				m1.m[3] - m2.m[3]);
}

inline __device__ __host__ mat4 operator * (const mat4 &m1, const mat4 &m2)
{
	mat4 m;
	vec4 col;

	for(int i=0; i<4; i++)
	{
		col = vec4(m2.m[0].x, m2.m[1].x, m2.m[2].x, m2.m[3].x);
		m.m[i].x = m1.m[i] * col;
		col = vec4(m2.m[0].y, m2.m[1].y, m2.m[2].y, m2.m[3].y);
		m.m[i].y = m1.m[i] * col;
		col = vec4(m2.m[0].z, m2.m[1].z, m2.m[2].z, m2.m[3].z);
		m.m[i].z = m1.m[i] * col;
		col = vec4(m2.m[0].w, m2.m[1].w, m2.m[2].w, m2.m[3].w);
		m.m[i].w = m1.m[i] * col;
	}

	return m;
}

inline __device__ __host__ vec4 operator * (const mat4 &m, const vec4 &v)
{
	return vec4(dot(m.m[0],v),
				dot(m.m[1],v),
				dot(m.m[2],v),
				dot(m.m[3],v));
}

inline __device__ __host__ mat4 operator * (const mat4 &m, const float &x)
{
	mat4 matrix;

	matrix.m[0] = m.m[0]*x;
	matrix.m[1] = m.m[1]*x;
	matrix.m[2] = m.m[2]*x;
	matrix.m[3] = m.m[3]*x;

	return matrix;
}

////////////////////////////////////////////////////////////////////////////////////////

inline __device__ __host__ vec3 getReflectionVector(vec3 normal, vec3 direction)
{
	return normalize(normal*2*dot(normal,direction) - direction);
}


inline __device__ __host__ bool getTransmissionVector(float indexi, float indext, vec3 normal, vec3 directionIn, vec3 &directionOut)
{
	float q = indexi/indext;
	float nDotL = dot(normal,directionIn);
	float sin1 = sin(acos(nDotL));
	float cs2 = 1 - q*q * (1 - nDotL*nDotL);

	if(cs2 >= 0.0)
	{
		
		directionOut = normalize((q*nDotL - sqrtf(1 - q*q * (1 - nDotL*nDotL)))*normal - (q*directionIn));
		return true;
	}
	else
	{
		//return vec3(1,0,1);
		//normalize((q*nDotL - sqrtf(-(1 - q*q * (1 - nDotL*nDotL))))*normal - (q*direction));
		directionOut = getReflectionVector(negate(normal),directionIn);
		return false;
	}

	//return normalize(q*direction - (q*nDotL - sqrtf(1-q*q*sin1*sin1))*normal);
}


#endif // __CUDARAYTRACER_ALGEBRA_