#ifndef _POINT_LIGHT_
#define _POINT_LIGHT_

#include "Intersection.cuh"

class PointLight
{
public:

	vec3	position;
	vec3	color;

	PointLight();
	PointLight(vec3 _position, vec3 _color);

	__device__ __host__ vec3 getLightVector(vec3 point);
};

#endif _POINT_LIGHT_


PointLight::PointLight()
{
	position = vec3(0,0,0);
	color = vec3(1,1,1);
}


PointLight::PointLight(vec3 _position, vec3 _color)
{
	position = _position;
	color = _color;
}


vec3 PointLight::getLightVector(vec3 point)
{
	return normalize(position-point);
}