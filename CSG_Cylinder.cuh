#ifndef _CSG_CYLINDER_
#define _CSG_CYLINDER_

#include "Intersection.cuh"

class CSG_Cylinder
{
protected:

	vec4	bottom;			// Equation that defines the top end-cap plane
	vec4	top;			// Equation that defines the bottom end-cap plane

public:

	typedef enum CylinderSurface { SIDE, BOTTOM, TOP };

	vec3	base;			// The base location of the cylinder.
	vec3	axis;			// The axis of symetry for the cylinder. (unitary)
	float	radius;			// The radius of the cylinder.
	float	height;			// The height of the cylinder.

	__device__ __host__ CSG_Cylinder();
	__device__ __host__ CSG_Cylinder(vec3 _base, vec3 _axis, float _radius, float _height);

	__device__ __host__ void buildPlanes();

	__device__ __host__ void translation(vec3 distance);
	__device__ __host__ void rotation(vec3 axis, float angle);
	__device__ __host__ void scale(vec3 amount);

	__device__ __host__ bool rayIntersection(vec3 origin, vec3 ray, Intersection &inIntersection, Intersection &outIntersection);

};


//////////////////////////////////////////////////////////////////////////////////////////////////////
// CSG Cylinder implementation
//////////////////////////////////////////////////////////////////////////////////////////////////////


CSG_Cylinder::CSG_Cylinder()
{
	base = vec3(0,0,0);
	axis = vec3(0,1,0);
	radius = 1.0f;
	height = 1.0f;

	buildPlanes();
}


CSG_Cylinder::CSG_Cylinder(vec3 _base, vec3 _axis, float _radius, float _height)
{
	base = _base;
	axis = normalize(_axis);
	radius = _radius;
	height = _height;

	buildPlanes();
}


void CSG_Cylinder::buildPlanes()
{
	// Bottom plane

	vec3 point = base;
	vec3 normal = negate(axis);
	float d;

	d = normal.x*point.x + normal.y*point.y + normal.z*point.z;
	bottom = vec4(normal.x, normal.y, normal.z, -d);

	// Top plane

	point = base + axis*height;
	normal = axis;

	d = normal.x*point.x + normal.y*point.y + normal.z*point.z;
	top = vec4(normal.x, normal.y, normal.z, -d);
}

void CSG_Cylinder::translation(vec3 distance)
{
	base = base + distance;
}

void CSG_Cylinder::rotation(vec3 _axis, float _angle)
{
	base = rotate(base, axis, _angle);
	axis = rotate(axis, _axis, _angle);
}

void CSG_Cylinder::scale(vec3 amount)
{
	base = base * amount;
	radius = radius * amount.y;
	height = height * amount.x;
}

//  Adapted from:
//
//  (Graphics Gems 4)
//	Intersecting a Ray with a Cylinder
//		- Joseph M. Cychosz
//		- Warren N. Waggenspack, Jr.

bool CSG_Cylinder::rayIntersection(vec3 origin, vec3 ray, Intersection &inIntersection, Intersection &outIntersection)
{
	vec3	RC;				// Ray base to cylinder base
	float	d;				// Shortest distance between the ray and the cylinder
	float	t,s;			// Distances along the ray
	vec3	n,D,O;
	float	ln;
	float	in;				// Entering distance
	float	out;			// Leaving distance
	bool	hit = false;
	int		surfin;			// Entering surface identifier
	int		surfout;		// Exiting surface identifier

	float	dc,dw;
	//float	in,out;			// Object intersection distances

	RC = origin - base;
	n = cross(ray,axis);
	ln = module(n);
	
	if(ln == 0.0f)								// ray parallel to cylinder
	{
		d	= dot(RC,axis);
		D	= RC - d*axis;
		d	= module(D);
		in	= -INF;
		out = INF;
		hit = (d <= radius);					// TRUE if ray is in cyl
	}
	else
	{
		n = normalize(n);
		d = fabs(dot(RC,n));					// Shortest distance
		hit = (d <= radius);

		if(hit)									// If ray hits cylinder
		{
			O	= cross(RC,axis);
			t	= - dot(O,n) / ln;
			O	= cross(n,axis);
			O	= normalize(O);
			s	= fabs(sqrt(radius*radius - d*d)) / dot(ray,O);
			in	= t + s;
			out = t - s;
		}
	}

	if(!hit)
	{
		return false;
	}
	else
	{
		surfin = surfout = CylinderSurface::SIDE;
		
		// Intersect the ray with the bottom end-cap plane
		
		dc = bottom.x*ray.x + bottom.y*ray.y + bottom.z*ray.z;
		dw = bottom.x*origin.x + bottom.y*origin.y + bottom.z*origin.z + bottom.w;

		if(dc == 0)			// If parallel to bottom plane
		{
			if(dw >= 0.0f) return false;
		}
		else
		{
			t = -dw/dc;
			if(dc >= 0.0f)	// If far plane
			{
				if( t > in && t < out )
				{
					out = t;
					surfout = CylinderSurface::BOTTOM;
				}
				if(t < in) return false;
			}
			else			// If near plane
			{
				if( t > in && t < out )
				{
					in = t;
					surfin = CylinderSurface::BOTTOM;
				}
				if(t > out) return false;
			}
		}

		// Intersect the ray with the top end-cap plane

		dc = top.x*ray.x + top.y*ray.y + top.z*ray.z;
		dw = top.x*origin.x + top.y*origin.y + top.z*origin.z + top.w;

		if(dc == 0)			// If parallel to top plane
		{
			if(dw >= 0.0f) return false;
		}
		else
		{
			t = -dw/dc;
			if(dc >= 0.0f)	// If far plane
			{
				if( t > in && t < out )
				{
					out = t;
					surfout = CylinderSurface::TOP;
				}
				if(t < in) return false;
			}
			else			// If near plane
			{
				if( t > in && t < out )
				{
					in = t;
					surfin = CylinderSurface::TOP;
				}
				if(t > out) return false;
			}
		}

		if(in < out)
		{
			vec3 C,D;
			float u;

			inIntersection.type = INTERSECTION_IN;
			inIntersection.distance	= in;
			inIntersection.point	= origin + ray*in;
			outIntersection.type = INTERSECTION_OUT;
			outIntersection.distance= out;
			outIntersection.point	= origin + ray*out;

			// In surface
			switch(surfin)
			{
			case CylinderSurface::BOTTOM:
				inIntersection.normal = negate(axis); break;

			case CylinderSurface::TOP:
				inIntersection.normal = axis; break;

			case CylinderSurface::SIDE:
				C = inIntersection.point - base;
				u = dot(axis,C);
				D = base + axis*u;

				inIntersection.normal = normalize(inIntersection.point-D);
				break;
			default:
				break;
			}

			// Out surface
			switch(surfout)
			{
			case CylinderSurface::BOTTOM:
				outIntersection.normal = negate(axis); break;

			case CylinderSurface::TOP:
				outIntersection.normal = axis; break;

			case CylinderSurface::SIDE:
				C = outIntersection.point - base;
				u = dot(axis,C);
				D = base + axis*u;

				outIntersection.normal = normalize(outIntersection.point-D);
				break;
			default:
				break;
			}

			return true;
		}
		else
			return false;
	}
}


#endif // _CSG_CYLINDER_